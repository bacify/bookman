<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Bookman_model extends CI_Model {

	public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
        public function get_user_admin($data=NULL)
        {
                if($data!=NULL)
                    $this->db->where($data);
                $query = $this->db->get('admin');
                return $query->result();                 
        }
        public function insert_user_admin($data){

                $query = $this->db->insert('admin',$data);
                return $this->db->insert_id();
        }
        public function update_user_admin($data,$con){

                $this->db->where($con);
                $query = $this->db->update('admin',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_user_admin($con){

                 
                $query = $this->db->delete('admin',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function get_village($data=NULL)
        {
                if($data!=NULL)
                    $this->db->where($data);
                $query = $this->db->get('villages');
                return $query->result();                 
        }
        public function get_district($data=NULL)
        {
                if($data!=NULL)
                    $this->db->where($data);
                $query = $this->db->get('districts');
                return $query->result();                 
        }
        public function get_regency($data=NULL)
        {
                if($data!=NULL)
                    $this->db->where($data);
                $query = $this->db->get('regencies');
                return $query->result();                 
        }
        public function get_province($data=NULL)
        {
                if($data!=NULL)
                    $this->db->where($data);
                $query = $this->db->get('provinces');
                return $query->result();                 
        }
         
        public function get_katalog($data=null)
        {
                if($data!=null)
                $this->db->where($data);
                $query = $this->db->get('katalog');
                return $query->result();
        }
        public function get_stok_inventory($data=null)
        {
                if($data!=null)
                $this->db->where($data);
                $query = $this->db->get('stok_inventory');
                return $query->result();
        }
        public function get_katalog_like($data,$limit=null)
        {
                if($limit!=null){
                        $this->db->limit(10);
                }
                $this->db->like($data);
                $query = $this->db->get('katalog');
                return $query->result();
        }
        public function get_katalog_like_or($data,$limit=null)
        {
                if($limit!=null){
                        $this->db->limit(10);
                }
                $this->db->or_like($data);
                $query = $this->db->get('katalog');
                return $query->result();
        }
        public function get_katalog_instock_like_or($data,$limit=null)
        {
                if($limit!=null){
                        $this->db->limit(10);
                }
                $this->db->or_like($data);
                $this->db->join('stok_inventory','katalog.id_katalog=stok_inventory.katalog_id');
                $this->db->where('stok >','0');
                $query = $this->db->get('katalog');
                return $query->result();
        }
        public function insert_katalog($data){

                $query = $this->db->insert('katalog',$data);
                return $this->db->insert_id();
        }
        public function update_katalog($data,$con){

                $this->db->where($con);
                $query = $this->db->update('katalog',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_katalog($con){

                 
                $query = $this->db->delete('katalog',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function insert_master_transaksi($data){

                $query = $this->db->insert('transaksi_master',$data);
                return $this->db->insert_id();
        }
        public function get_master_transaksi($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('transaksi_master');
                return $query->result();
        }
        public function update_master_transaksi($data,$con){

                $this->db->where($con);
                $query = $this->db->update('transaksi_master',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_master_transaksi($con){

                 
                $query = $this->db->delete('transaksi_master',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function insert_master_transaksi_detail($data){

                $query = $this->db->insert('transaksi_master_detail',$data);
                return $this->db->insert_id();
        }
        public function get_master_transaksi_detail($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('transaksi_master_detail');
                return $query->result();
        }
        public function update_master_transaksi_detail($data,$con){

                $this->db->where($con);
                $query = $this->db->update('transaksi_master_detail',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_master_transaksi_detail($con){

                 
                $query = $this->db->delete('transaksi_master_detail',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function insert_inventory($data){

                $query = $this->db->insert('inventory',$data);
                return $this->db->insert_id();
        }
        public function get_inventory($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('inventory');
                return $query->result();
        }
        public function update_inventory($data,$con){

                $this->db->where($con);
                $query = $this->db->update('inventory',$data);
                return ($this->db->affected_rows() > 0);
        }
        
        public function delete_inventory($con){

                 
                $query = $this->db->delete('inventory',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function insert_member($data){

                $query = $this->db->insert('member',$data);
                return $this->db->insert_id();
        }
        public function get_member($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('member');
                return $query->result();
        }
        public function update_member($data,$con){

                $this->db->where($con);
                $query = $this->db->update('member',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_member($con){

                 
                $query = $this->db->delete('member',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function insert_pinjaman($data){

                $query = $this->db->insert('peminjaman',$data);
                return $this->db->insert_id();
        }
        public function get_pinjaman($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('peminjaman');
                return $query->result();
        }
        public function get_pinjaman_with_detail($data=null){

                if($data!=null)
                        $this->db->where($data);
                        $this->db->join('peminjaman_detail','id_pinjam=pinjam_id');
                $query = $this->db->get('peminjaman');
                return $query->result();
        }
        public function update_pinjaman($data,$con){

                $this->db->where($con);
                $query = $this->db->update('peminjaman',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_pinjaman($con){

                 
                $query = $this->db->delete('peminjaman',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function insert_pinjaman_detail($data){

                $query = $this->db->insert('peminjaman_detail',$data);
                return $this->db->insert_id();
        }
        public function get_pinjaman_detail($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('peminjaman_detail');
                return $query->result();
        }
        public function update_pinjaman_detail($data,$con){

                $this->db->where($con);
                $query = $this->db->update('peminjaman_detail',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_pinjaman_detail($con){

                 
                $query = $this->db->delete('peminjaman_detail',$con);
                return ($this->db->affected_rows() > 0);
        }

        public function get_pinjaman_aktif($data=null)
        {
                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('peminjaman_aktif');
                return $query->result();
        }
        public function get_pinjaman_aktif_detail($data=null)
        {
                if($data!=null)
                        $this->db->where($data);
                $this->db->join('katalog','katalog_id=id_katalog');
                $query = $this->db->get('peminjaman_aktif');
                return $query->result();
        }
        public function get_kategory($data=null)
        {
                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('katalog_kategori');
                return $query->result();
        }
        public function get_inventory_status($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('inventory_status');
                return $query->result();
        }
        public function update_inventory_status($data){
               
                $query = $this->db->update('inventory_status',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function get_kunjungan($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('kunjungan');
                return $query->result();
        }
        public function insert_kunjungan($data){

                $query = $this->db->insert('kunjungan',$data);
                return $this->db->insert_id();
        }
        public function get_jumlah_kunjungan($data=null){

                if($data!=null)
                        $this->db->where($data);

                $this->db->select('count(*) as jumlah');
                $query = $this->db->get('kunjungan');
                return $query->result();
        }
        public function get_booking_online($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('booking');
                return $query->result();
        }
        public function get_katalog_baru($limit=null){

                if($limit!=null)
                        $this->db->limit($limit);
                        $this->db->order_by('id_katalog','desc');
                $query = $this->db->get('katalog');
                return $query->result();
        }

        public function insert_booking($data){

                $query = $this->db->insert('booking',$data);
                return $this->db->insert_id();
        }
        public function get_booking($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('booking');
                return $query->result();
        }
        public function update_booking($data,$con){

                $this->db->where($con);
                $query = $this->db->update('booking',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_booking($con){

                 
                $query = $this->db->delete('booking',$con);
                return ($this->db->affected_rows() > 0);
        }
        public function insert_booking_detail($data){

                $query = $this->db->insert('booking_detail',$data);
                return $this->db->insert_id();
        }
        public function get_booking_detail($data=null){

                if($data!=null)
                        $this->db->where($data);
                $query = $this->db->get('booking_detail');
                return $query->result();
        }
        public function update_booking_detail($data,$con){

                $this->db->where($con);
                $query = $this->db->update('booking_detail',$data);
                return ($this->db->affected_rows() > 0);
        }
        public function delete_booking_detail($con){

                 
                $query = $this->db->delete('booking_detail',$con);
                return ($this->db->affected_rows() > 0);
        }
         
}
