<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('template');        
        $this->load->model('bookman_model');            
         /*disable Cache*/
            $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        /*EO disable cache*/    
    }

	public function index()
	{
		$this->template->load('c_home');
    }
    public function home()
	{
        if($this->input->post('kata_kunci')){
            $katakunci=$this->input->post('kata_kunci');
            redirect('main/cari/'.urlencode($katakunci));
        }        
        
        $this->template->load('c_home');
    }
    
    public function cari(){
        $katakunci=$this->uri->segment(3);
        
        $data['katakunci']=$katakunci;
        $like=array();
		$like['no_register'] = $katakunci;
		$like['no_panggil'] = $katakunci;
		$like['judul'] = $katakunci;
		$like['pengarang'] = $katakunci;
		$like['subyek'] = $katakunci;

        $result=$this->bookman_model->get_katalog_like_or($like);
        foreach($result as $r){

            /* get all katakunci */
            $r->no_register = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r->no_register));
            $r->no_panggil = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r->no_panggil));
            $r->judul = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r->judul));
            $r->pengarang = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r->pengarang));
            $r->subyek = str_replace( strtolower($katakunci) , '<b>'.strtolower($katakunci).'</b>' , strtolower($r->subyek));


            /* GET STATUS */
            $x=array();
            $con=array();
            $con['katalog_id']=$r->id_katalog;
            $x=$this->bookman_model->get_stok_inventory($con);
            if(count($x)>0){
                if($x[0]->stok > 0){
                    $r->status=1;
                }else{
                    $r->status=0;
                }
            }else{
                $r->status=0;
            }
        }
        
        $data['hasil']=$result;
        $this->template->load('c_hasil_cari',$data);
    }

    public function cek(){
        if($this->input->post('nomor_anggota')){
            $katakunci=$this->input->post('nomor_anggota');
            redirect('main/cek_pinjaman/'.urlencode($katakunci));
        }    
         
        $this->template->load('c_cek_pinjam');
    }
    public function cek_pinjaman(){
        $katakunci=$this->uri->segment(3);
        
        $data['katakunci']=$katakunci;
        $like=array();
		$like['member_id'] = $katakunci;	

        $result=$this->bookman_model->get_pinjaman_aktif_detail($like);
        
        
        $data['hasil']=$result;
        $this->template->load('c_hasil_cek_pinjaman',$data);
    }

    public function koleksi_baru(){
         
        $result=$this->bookman_model->get_katalog_baru(50);
        $data['hasil']=$result;
        $this->template->load('c_koleksi_page',$data);
    }
    public function akun(){
         
        redirect('main/login');
        $this->template->load('c_cek_pinjam');
    }   

    public function lupapassword(){
         
        $this->template->load('c_lupapassword');
    }
    public function about(){
         
        $this->template->load('c_cek_pinjam');
    }
    public function login(){
        $this->checklogin();
		if( $this->input->post('submit') !== NULL){
			/* CHECK LOGIN */
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			$data=array('email'=>$email,
						'password'=>md5($password),
						'status'=>1);
						 
			$r=$this->bookman_model->get_member($data);
			if(count($r)>0){
				/* LOGIN SUKSES */
				$userdata=array('UID'=>$r[0]->id_member,
								'nama'=>$r[0]->nama,								
								'email'=>$r[0]->email,
								'type'=>$r[0]->type,
								'type'=>'member'
								);
				$this->session->set_userdata($userdata);
				redirect('/member/','refresh');
			}else{
				 
				/* LOGIN GAGAL */
				$this->session->set_flashdata('pesan', 'Login gagal, user/pass salah');
				 
			}
		}	

        $this->template->load('c_login_page');
    }
    public function  checklogin(){
		if(!$this->session->userdata('UID')){			 
			 
		}else{
			if($this->session->userdata('type')!='admin')
			 redirect('member','refresh');
		}
	}
}
