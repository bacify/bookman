<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kunjungan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('template');        
        $this->load->model('bookman_model');            
         /*disable Cache*/
            $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        /*EO disable cache*/    
    }

	public function index()
	{
		$this->template->load('g_home');
    }

    public function insertkunjungan(){
		 
		$id=$this->uri->segment(3);
		if($id==null)
            die();
            
        /* GET USER */        
		$user=$this->bookman_model->get_member(array('id_member'=>$id));
		$result=array();
		if(count($user)>0){
            /* INSERT KUNJUNGAN */

            $val=array();
            $val['member_id']=$id;
            $r=$this->bookman_model->insert_kunjungan($val);
            if($r){
                $result['status']=1;
			    $result['result']=$user[0];
            }else{
                $result['status']=0;
			    $result['result']=array();;
            }
			 
		}else{
			$result['status']=0;
			$result['result']=array();;
		}
		header('Content-Type: application/json');
		
		echo  json_encode($result);
	}
}