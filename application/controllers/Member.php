<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('Admintemplate');        
        $this->load->model('bookman_model');            
        $this->load->library('Datatables');           
         /*disable Cache*/
            $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        /*EO disable cache*/    
    }

	public function index()
	{
        $this->checklogin();
		
		// GET TANGGUNGAN
		$con=array();
		$con['status_pinjam']=1;
		$con['member_id']=$this->session->userdata('UID');
		$con['peminjaman_detail.status']=1;
		$tanggungan=$this->bookman_model->get_pinjaman_with_detail($con);
		$data['tanggungan']=count($tanggungan);
		
		/* GET KUNJUNGAN */
		$con=array();
		$con['member_id']=$this->session->userdata('UID');
		$jumlahkunjungan=$this->bookman_model->get_jumlah_kunjungan($con);
		$data['kunjungan']=$jumlahkunjungan[0]->jumlah;

		/* GET PINJAMAN ONLINE */
		$con=array();
		$con['member_id']=$this->session->userdata('UID');
		$pinjaman_online=$this->bookman_model->get_booking($con);
		$data['booking_online']=count($pinjaman_online);
        $this->admintemplate->load('m_home',$data);
    }
    public function home()
	{          
       
		$this->checklogin();
		
		// GET TANGGUNGAN
		$con=array();
		$con['status_pinjam']=1;
		$con['member_id']=$this->session->userdata('UID');
		$con['peminjaman_detail.status']=1;
		$tanggungan=$this->bookman_model->get_pinjaman_with_detail($con);
		$data['tanggungan']=count($tanggungan);
		/* GET KUNJUNGAN */
		$con=array();
		$con['member_id']=$this->session->userdata('UID');
		$jumlahkunjungan=$this->bookman_model->get_jumlah_kunjungan($con);
		$data['kunjungan']=$jumlahkunjungan[0]->jumlah;

		/* GET PINJAMAN ONLINE */
		$con=array();
		$con['member_id']=$this->session->userdata('UID');
		$pinjaman_online=$this->bookman_model->get_booking($con);
		$data['booking_online']=count($pinjaman_online);
        $this->admintemplate->load('m_home',$data);
    }

    public function logout(){
		$this->session->sess_destroy();
		$this->session->set_flashdata('pesan', 'Anda telah Keluar');
			redirect('/main/login', 'refresh');
	}
    public function  checklogin(){
		if(!$this->session->userdata('UID')){
			$this->session->set_flashdata('pesan', 'Anda Belum Login, silahkan login');
			redirect('/main/login', 'refresh');
				
		}
	}

    public function member_daftar_pinjaman()
    {
		$this->checklogin();
		$con=array();
		$con['member_id']=$this->session->userdata('UID');
		$con['status']=1;
		$pinjaman=$this->bookman_model->get_pinjaman_with_detail($con);
		foreach($pinjaman as $p){
			$c=array();
			$c['id_katalog']=$p->katalog_id;
			$buku=$this->bookman_model->get_katalog($c);
			if(count($buku)>0)
				$p->katalog=$buku[0];
			else
				$p->katalog=null;
		}
		 $data['pinjaman']=$pinjaman;
        $this->admintemplate->load('member_daftar_pinjaman',$data);
    }


    public function member_riwayat()
    {
        $this->checklogin();

        $this->admintemplate->load('member_riwayat');
    }

    public function member_booking()
    {
        $this->checklogin();

        $this->admintemplate->load('m_booking_list');
    }
    public function get_pinjaman(){
        if(!$this->session->userdata('UID')){
            echo array();
            die();
				
        }
        $id=$this->session->userdata('UID');
		$this->datatables->select('id_pinjam,LPAD(member_id,6,0) as member_id,tanggal,status_pinjam');
		$this->datatables->from('peminjaman');	
        $this->datatables->where(array('member_id'=>$id));	
        $this->datatables->add_column('view',  '<a href="'.base_url().'member/pinjam_detail/$1" class="btn btn-transparent  "  title="cek list pinjaman"><i class="far fa-hand-point-right"></i></a> ',
											   'id_pinjam');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
    }
    public function get_pinjaman_online(){
        if(!$this->session->userdata('UID')){
            echo array();
            die();
				
        }
        $id=$this->session->userdata('UID');
		$this->datatables->select('id_booking,LPAD(member_id,6,0) as member_id,tanggal_booking,tanggal_pinjam,status');
		$this->datatables->from('booking');	
        $this->datatables->where(array('member_id'=>$id));	
        $this->datatables->add_column('view',  '<a href="'.base_url().'member/booking_detail/$1" class="btn btn-transparent  "  title="cek list pinjaman"><i class="far fa-hand-point-right"></i></a> '.											   
											   '<a href="javascript:void(0);" class="delete_record btn btn-transparent "   data-id="$1" title="Batalkan pinjaman"><i class="far fa-calendar-times"></i></a>',
											   'id_booking');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
    }
    public function booking_baru(){
		
		if( $this->input->post('submit') !== NULL){
            
            $tanggal=explode('-',$this->input->post('tanggal'));
            
            

			 
			$value=array();
            $value['tanggal_pinjam']=$tanggal[2].'-'.$tanggal[1].'-'.$tanggal[0];
            $value['status']=0;				 
			$value['member_id']=$this->session->userdata('UID');				 
					
				$r=$this->bookman_model->insert_booking($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');
			
			redirect('member/booking_detail/'.$r,'refresh');
		}
	

		redirect('member/member_booking/','refresh');
		 
    }
    public function booking_detail(){
		$this->checklogin();		
		$id='';
		$master=array();
		$nama='';
		if($this->uri->segment(3)!=''){
			$id=$this->uri->segment(3);
                $master=$this->bookman_model->get_booking(array('id_booking'=>$id));
                if(count($master)>0)
                $master=$master[0];			
		}else{
			redirect('dashboard/member_booking','refresh');
		}
        
        $data['status_edit']=(date('Y-m-d')>=date('Y-m-d',strtotime($master->tanggal_pinjam)));
		$data['nama']=$this->session->userdata('nama');	
		$data['id_booking']=$id;
        $data['booking_master']=$master;        
		$this->admintemplate->load('m_booking_detail',$data);
    }
    public function get_pinjaman_online_detail(){         
        $this->checklogin();
		$this->datatables->select('id_booking_detail,no_panggil,judul,pengarang,tahun,isbn');
		if($this->uri->segment(3)!=''){
			$wh=$this->uri->segment(3);			
			$this->datatables->where('booking_id',$wh);
		}	
		$this->datatables->from('booking_detail');			
		$this->datatables->join('katalog', 'katalog_id=id_katalog');
		$this->datatables->add_column('view',  '<a href="'.base_url().'dashboard/pinjaman_detail/$1" class="btn btn-primary btn-sm"  title="Proses Data"><i class="fas fa-dot-circle"></i></a> '.											   
											   '<a href="javascript:void(0);" class="delete_record btn btn-danger btn-sm" data-id="$1" title="Hapus Data"><i class="fas fa-times"></i></a>',
											   'id_pinjam');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
    }
    public function booking_detail_add(){
		 
		
		$master=$this->input->post('id_booking');
		if( $this->input->post('id_katalog') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['booking_id']=$this->input->post('id_booking');
			$con['katalog_id']=$this->input->post('id_katalog');
			$v=$this->bookman_model->get_booking_detail($con);
			if(count($v)>0){
				$this->session->set_flashdata('pesan', 'data Sudah ada');
			}else{
				 
				$value=array();
				/* CHECK LOGIN */
				$value['booking_id']=$this->input->post('id_booking');
				$value['katalog_id']=$this->input->post('id_katalog');					
						
				$r=$this->bookman_model->insert_booking_detail($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');			
			}
			 

			redirect('member/booking_detail/'.$master,'refresh');
		}
	

		redirect('member/booking_detail/'.$master,'refresh');
    }
    public function booking_detail_delete(){
         
		$idpinjam=$this->input->post('id_booking');
		if( $this->input->post('id_detail') !== NULL){			 
			
			$con=array();			 
			$con['id_booking_detail']=$this->input->post('id_detail');
			$r=$this->bookman_model->delete_booking_detail($con);			
			 	
			if($r){
				$this->session->set_flashdata('pesan', 'berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'GAGAL dihapus');
			}
		}

		
		redirect('member/booking_detail/'.$idpinjam,'refresh');
    }
    

    public function searchkatalogall(){
		 
		$con=$this->input->get('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		$like['no_register'] = $con;
		$like['no_panggil'] = $con;
		$like['judul'] = $con;

		$r=$this->bookman_model->get_katalog_like_or($like,10);

		$result=array();
		foreach($r as $rr){
			$temp=array();
			if($rr->no_register==null||$rr->no_register==''){
				$rr->no_register='N/A';
			}
			if($rr->no_panggil==null||$rr->no_panggil==''){
				$rr->no_panggil='N/A';
			}
			if($rr->pengarang==null||$rr->pengarang==''){
				$rr->pengarang='N/A';
			}
			$temp=array('value'=>$rr->id_katalog, 'label'=>$rr->no_register.'| '.$rr->no_panggil.'| '.$rr->judul.'| '.$rr->pengarang.'| '.$rr->tahun);
			$result[]=$temp;
		};
		
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}
    
} 