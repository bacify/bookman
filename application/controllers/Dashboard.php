<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
        parent::__construct();
        $this->load->library('Admintemplate');        
        $this->load->model('bookman_model');            
        $this->load->library('Datatables');            
         /*disable Cache*/
            $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
            $this->output->set_header('Pragma: no-cache');
            $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        /*EO disable cache*/    
    }

	public function index()
	{
		 
		$this->checklogin();

		/* GET KUNJUNGAN */
		$c=array();
		$c['MONTH(waktu)']=date('m');
		$c['YEAR(waktu)']=date('Y');
		$kunjungan=$this->bookman_model->get_kunjungan($c);
		$data['jumlah_kunjungan']=count($kunjungan);
		$c=array();
		$c['MONTH(tanggal)']=date('m');
		$c['YEAR(tanggal)']=date('Y');
		$pinjaman=$this->bookman_model->get_pinjaman($c);
		$data['jumlah_pinjaman']=count($pinjaman);
		$c=array();
		$c['MONTH(tanggal_booking)']=date('m');
		$c['YEAR(tanggal_booking)']=date('Y');
		$booking=$this->bookman_model->get_booking_online($c);
		$data['jumlah_booking']=count($booking);
		$user=$this->bookman_model->get_member();
		$data['jumlah_member']=count($user);
		$katalog=$this->bookman_model->get_katalog();
		$data['jumlah_jenis_katalog']=count($katalog);
		$inventory=$this->bookman_model->get_stok_inventory();
		$total=0;
		foreach($inventory as $iv){
			$total+=$iv->stok;
		}
		$data['jumlah_katalog']=$total;
		$this->admintemplate->load('admin_home',$data);
		// $this->load->view('dashboard');
	}
	public function login(){

		if( $this->input->post('submit') !== NULL){
			/* CHECK LOGIN */
			$username=$this->input->post('userid');
			$password=$this->input->post('password');
			$data=array('username'=>$username,
						'password'=>md5($password),
						'status'=>1);
						 
			$r=$this->bookman_model->get_user_admin($data);
			if(count($r)>0){
				/* LOGIN SUKSES */
				$userdata=array('UID'=>$r[0]->id_admin,
								'nama'=>$r[0]->nama,
								'username'=>$r[0]->username,
								'email'=>$r[0]->email,
								'akses'=>$r[0]->akses,
								'type'=>'admin'
								);
				$this->session->set_userdata($userdata);
				redirect('/dashboard/','refresh');
			}else{
				 
				/* LOGIN GAGAL */
				$this->session->set_flashdata('pesan', 'Login gagal, pastikan memasukkan username dan password dengan benar');
				redirect('/dashboard/login', 'refresh');
			}
		}	

		$this->admintemplate->load('admin_login');
	}
	public function utama()
	{
		$this->checklogin();

		/* GET KUNJUNGAN */
		$c=array();
		$c['MONTH(waktu)']=date('m');
		$c['YEAR(waktu)']=date('Y');
		$kunjungan=$this->bookman_model->get_kunjungan($c);
		$data['jumlah_kunjungan']=count($kunjungan);
		$c=array();
		$c['MONTH(tanggal)']=date('m');
		$c['YEAR(tanggal)']=date('Y');
		$pinjaman=$this->bookman_model->get_pinjaman($c);
		$data['jumlah_pinjaman']=count($pinjaman);
		$c=array();
		$c['MONTH(tanggal_booking)']=date('m');
		$c['YEAR(tanggal_booking)']=date('Y');
		$booking=$this->bookman_model->get_booking_online($c);
		$data['jumlah_booking']=count($booking);
		$user=$this->bookman_model->get_member();
		$data['jumlah_member']=count($user);
		$katalog=$this->bookman_model->get_katalog();
		$data['jumlah_jenis_katalog']=count($katalog);
		$inventory=$this->bookman_model->get_stok_inventory();
		$total=0;
		foreach($inventory as $iv){
			$total+=$iv->stok;
		}
		$data['jumlah_katalog']=$total;
		$this->admintemplate->load('admin_home',$data);
		// $this->load->view('dashboard');
	}


	public function inventory(){
		$this->checklogin();

		$this->admintemplate->load('admin_riwayat_transaksi');
		// $this->load->view('dashboard');
	}
	public function get_book_history(){
		$this->datatables->select('katalog_id,tanggal,judul,pengarang,penerbit,nama_kategori,masuk,keluar,keterangan');

		  
		$this->datatables->from('inventory');
		$this->datatables->join('katalog', 'katalog_id=id_katalog');
		$this->datatables->join('katalog_kategori', 'kategori_id=id_kategori');
		
		
		
		$book= $this->datatables->generate();
		// $temp=json_decode($book);
		
		// foreach($temp->data as $d){
		// 	$d->id_master=sprintf('%06d', $d->id_master);
		// }
		// $book=json_encode($temp);		
		header('Content-Type: application/json');
		echo $book;
	}
	public function master_katalog(){
		$this->checklogin();

		$kategori=$this->bookman_model->get_kategory();
		$data['kategori']=$kategori;
		$this->admintemplate->load('admin_master_katalog',$data);
	}
	public function stok_katalog(){
		$this->checklogin();		
		$data=array();
		$this->admintemplate->load('admin_stok_master_katalog',$data);
	}
	public function barcode(){
		$this->checklogin();	
		$field=$this->input->get('field');	
		$value='';
		$x=(int)$this->input->get('id');
		$type=(int)$this->input->get('type');

		/* get value */
		$katalog=$this->bookman_model->get_katalog(array('id_katalog'=>$x));
		 

		if($field=='id'){
			/* GET ID */
			
			$value=sprintf('%06d', $x);
		}else if($field=='isbn'){
			$value=$katalog[0]->isbn;
		}else if($field=='register'){
			$value=$katalog[0]->no_register;

		}else{
			echo 'error';
			die();
		}
		$data=array();
		if($value==''){
			$value='000000';
		}
		$data['field']=strtoupper($field);
		$data['value']=strtoupper($value);

		if(count($katalog)>0){
			$data['judul']=$katalog[0]->judul;
			$data['pengarang']=$katalog[0]->pengarang;
		}
		if($type==1)
			$this->admintemplate->load('admin_print_qrcode',$data);
		else
			$this->admintemplate->load('admin_print_barcode',$data);
		
	}
	public function getqrcode(){

		$id=$this->uri->segment(3);
		$this->checklogin();		
		/* get library CIQRCODE */
		$this->load->library('ciqrcode');  
		$config['cacheable']	= true; //boolean, the default is true
		$config['cachedir']		= ''; //string, the default is application/cache/
		$config['errorlog']		= ''; //string, the default is application/logs/
		$config['quality']		= true; //boolean, the default is true
		$config['size']			= ''; //interger, the default is 1024
		$config['black']		= array(224,255,255); // array, default is array(255,255,255)
		$config['white']		= array(70,130,180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($config);        
 
        $params['data'] = $id; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
		//$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		header("Content-Type: image/png");
		$data['img']=$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
		// $data=array();
		
	}
	public function getbarcode(){

		$id=$this->uri->segment(3);
		$this->checklogin();		
		/* get library CIQRCODE */
		$this->load->library('zend');
		// Load in folder Zend
		$this->zend->load('Zend/Barcode');
		// Generate barcode
		// header("Content-Type: image/png");
		Zend_Barcode::render('code128', 'image', array('text'=>$id), array());		
		 
		// $data=array();
		
	}
	public function master_buku_save(){

		if( $this->input->post('judul') !== NULL){
			$value=array();
			/* CHECK LOGIN */
			$value['no_register']=$this->input->post('no_register');
			$value['judul']=$this->input->post('judul');
			$value['no_panggil']=$this->input->post('no_panggil');
			$value['pengarang']=$this->input->post('pengarang');
			$value['penerbit']=$this->input->post('penerbit');
			$value['kategori_id']=$this->input->post('kategori');
			$value['bahasa']=$this->input->post('bahasa');
			$value['tahun']=$this->input->post('tahun');
			$value['edisi']=$this->input->post('edisi');
			$value['subyek']=$this->input->post('subyek');
			$value['klasifikasi']=$this->input->post('klasifikasi');
			$value['deskripsi']=$this->input->post('deskripsi');
			$value['isbn']=$this->input->post('isbn');			
			
			$r=$this->bookman_model->insert_katalog($value);			
			$this->session->set_flashdata('pesan', 'Katalog dengan id '.$r.' berhasil disimpan');
		}

			redirect('dashboard/master_katalog','refresh');
	}
	public function master_buku_update(){
		if( $this->input->post('id') !== NULL){
			$value=array();
			$con=array();
			$con['id_katalog']=$this->input->post('id');
			/* CHECK LOGIN */
			$value['no_register']=$this->input->post('no_register');
			$value['judul']=$this->input->post('judul');
			$value['no_panggil']=$this->input->post('no_panggil');
			$value['pengarang']=$this->input->post('pengarang');
			$value['penerbit']=$this->input->post('penerbit');
			$value['kategori_id']=$this->input->post('kategori');
			$value['bahasa']=$this->input->post('bahasa');
			$value['tahun']=$this->input->post('tahun');
			$value['edisi']=$this->input->post('edisi');
			$value['subyek']=$this->input->post('subyek');
			$value['klasifikasi']=$this->input->post('klasifikasi');
			$value['deskripsi']=$this->input->post('deskripsi');
			$value['isbn']=$this->input->post('isbn');			
						 
			$r=$this->bookman_model->update_katalog($value,$con);			
			if($r){
				$this->session->set_flashdata('pesan', 'Perubahan data katalog berhasil dilakukan');
			}	else{
				$this->session->set_flashdata('pesan', 'Perubahan data katalog GAGAL dilakukan');
			}
		}

			redirect('dashboard/master_katalog','refresh');
	}
	public function master_buku_delete(){
		if( $this->input->post('id') !== NULL){
			 
			$con=array();
			$con['id_katalog']=$this->input->post('id');
			 
			$r=$this->bookman_model->delete_katalog($con);			
			if($r){
				$this->session->set_flashdata('pesan', 'katalog berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'katalog GAGAL dihapus');
			}
		}

			redirect('dashboard/master_katalog','refresh');
	}

	public function inventory_masuk(){
		$this->checklogin();		
		$data=array();
		$this->admintemplate->load('admin_inventory_in',$data);
	}
	public function get_transaksi_master(){

				 
		$this->datatables->select('id_master,tanggal_transaksi as tanggal,asal,keterangan,admin_id,nama,status_transaksi');

		if($this->uri->segment(3)!=''){
			$wh=$this->uri->segment(3);
			if($wh==1 || $wh==2)
			$this->datatables->where('jenis_transaksi',$wh);
		}		 
		$this->datatables->from('transaksi_master');
		$this->datatables->join('admin', 'admin_id=id_admin');
		
		$book= $this->datatables->generate();
		// $temp=json_decode($book);
		
		// foreach($temp->data as $d){
		// 	$d->id_master=sprintf('%06d', $d->id_master);
		// }
		// $book=json_encode($temp);		
		header('Content-Type: application/json');
		echo $book;
	}

	public function get_transaksi_master_json(){
		 
		$id=$this->uri->segment(3);

		if($id==null)
			die();
		$katalog=$this->bookman_model->get_master_transaksi(array('id_master'=>$id));
		 
		header('Content-Type: application/json');
		echo  json_encode($katalog[0]);
	}

	public function master_transaksi_save(){
		if( $this->input->post('asal') !== NULL){
			$value=array();
			/* CHECK LOGIN */
			$value['jenis_transaksi']=$this->input->post('jenis');
			$value['asal']=$this->input->post('asal');
			$value['keterangan']=$this->input->post('keterangan');
			$value['status_transaksi']=0;
			$value['admin_id']=$this->session->userdata('UID');			 			
					 
			$r=$this->bookman_model->insert_master_transaksi($value);			
			$this->session->set_flashdata('pesan', 'Transaksi Baru '.sprintf('%06d', $r).' berhasil disimpan');
			redirect('dashboard/inventory_detail_in/'.$r,'refresh');
		}
	

			redirect('dashboard/inventory_masuk','refresh');
	}
	public function master_transaksi_update(){
		if( $this->input->post('id_master') !== NULL){
			$value=array();
			$con=array();
			$con['id_master']=$this->input->post('id_master');
			/* CHECK LOGIN */
			$value['asal']=$this->input->post('asal');
			$value['keterangan']=$this->input->post('keterangan');			 
			$value['admin_id']=$this->session->userdata('UID');				
						 
			$r=$this->bookman_model->update_master_transaksi($value,$con);			
			if($r){
				$this->session->set_flashdata('pesan', 'Perubahan data transaksi berhasil dilakukan');
			}	else{
				$this->session->set_flashdata('pesan', 'Perubahan data transaksi GAGAL dilakukan');
			}
		}

			redirect('dashboard/inventory_masuk','refresh');
	}
	public function master_transaksi_delete(){
		if( $this->input->post('id') !== NULL){
			 
			$con=array();
			$con['id_master']=$this->input->post('id');
			$con2['master_id']=$this->input->post('id');
			$this->bookman_model->delete_master_transaksi_detail($con2);			
			$r=$this->bookman_model->delete_master_transaksi($con);			
			if($r){
				$this->session->set_flashdata('pesan', 'Transaksi berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'Transaksi GAGAL dihapus');
			}
		}

			redirect('dashboard/inventory_masuk','refresh');
	}
	
	public function inventory_detail_in(){
		$this->checklogin();		
		$id='';
		$master=array();
		if($this->uri->segment(3)!=''){
			$id=$this->uri->segment(3);
				$master=$this->bookman_model->get_master_transaksi(array('id_master'=>$id));
			if(count($master)>0)
				$master=$master[0];
		}else{
			redirect('dashboard/inventory_masuk','refresh');
		}
		$kategori=$this->bookman_model->get_kategory();
		$data['kategori']=$kategori;
		$data['master']=$master;		
		$data['id_master']=$id;
		$this->admintemplate->load('admin_inventory_in_detail',$data);
	}
	public function get_transaksi_master_detail(){

				 
		$this->datatables->select('id_detail,katalog_id,nama_kategori,no_register,no_panggil,judul,pengarang,penerbit,bahasa,tahun,edisi,subyek,klasifikasi,deskripsi,isbn,keluar,masuk');

		if($this->uri->segment(3)!=''){
			$wh=$this->uri->segment(3);			
			$this->datatables->where('master_id',$wh);
		}		 
		$this->datatables->from('transaksi_master_detail');
		$this->datatables->join('katalog', 'katalog_id=id_katalog');
		$this->datatables->join('katalog_kategori', 'kategori_id=id_kategori');
		$this->datatables->add_column('view', '<a href="javascript:void(0);" class="view_record btn btn-success btn-sm" data-id="$1" title="Lihat Detail Transaksi"><i class="fas fa-eye"></i></a>
											   <a href="javascript:void(0);" class="edit_record btn btn-primary btn-sm" data-id="$1"  title="edit Katalog"><i class="far fa-edit"></i></a> 
											   ',
											   'id_detail');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
	}

	public function master_detail_add(){
		$master=$this->input->post('id_master');
		if( $this->input->post('id_katalog') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['master_id']=$this->input->post('id_master');
			$con['katalog_id']=$this->input->post('id_katalog');
			$v=$this->bookman_model->get_master_transaksi_detail($con);
			if(count($v)>0){
				$this->session->set_flashdata('pesan', 'data Sudah ada');
			}else{

				$value=array();
				/* CHECK LOGIN */
				$value['master_id']=$this->input->post('id_master');
				$value['katalog_id']=$this->input->post('id_katalog');
				$value['masuk']=$this->input->post('jumlah');			
						
				$r=$this->bookman_model->insert_master_transaksi_detail($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');
			}
			redirect('dashboard/inventory_detail_in/'.$master,'refresh');
		}
	

		redirect('dashboard/inventory_detail_in/'.$master,'refresh');
	}
	public function master_detail_new_add(){
		$master=$this->input->post('id_master');
		if( $this->input->post('judul') !== NULL){

			/* insert katalog */
			$value=array();
			/* CHECK LOGIN */
			$value['no_register']=$this->input->post('no_register');
			$value['judul']=$this->input->post('judul');
			$value['no_panggil']=$this->input->post('no_panggil');
			$value['pengarang']=$this->input->post('pengarang');
			$value['penerbit']=$this->input->post('penerbit');
			$value['kategori_id']=$this->input->post('kategori');
			$value['bahasa']=$this->input->post('bahasa');
			$value['tahun']=$this->input->post('tahun');
			$value['edisi']=$this->input->post('edisi');
			$value['subyek']=$this->input->post('subyek');
			$value['klasifikasi']=$this->input->post('klasifikasi');
			$value['deskripsi']=$this->input->post('deskripsi');
			$value['isbn']=$this->input->post('isbn');			
						 
			$r=$this->bookman_model->insert_katalog($value);		
			

			/* INSERT DETAIL */
			$value=array();
			/* CHECK LOGIN */
			$value['master_id']=$this->input->post('id_master');
			$value['katalog_id']=$r;
			$value['masuk']=$this->input->post('jumlah');			
					 
			$rr=$this->bookman_model->insert_master_transaksi_detail($value);			
			$this->session->set_flashdata('pesan', 'data ditambahkan');
			redirect('dashboard/inventory_detail_in/'.$master,'refresh');		
		 
		}

		redirect('dashboard/inventory_detail_in/'.$master,'refresh');
	}
	public function master_detail_delete(){

		$master=$this->input->post('id_master'); 	
		if( $this->input->post('id_detail') !== NULL){
			 
			$con=array();
			$con['id_detail']=$this->input->post('id_detail');			 
			$r=$this->bookman_model->delete_master_transaksi_detail($con);			
			if($r){
				$this->session->set_flashdata('pesan', 'data berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'data GAGAL dihapus');
			}
		}

		redirect('dashboard/inventory_detail_in/'.$master,'refresh');
	}
	public function master_detail_save(){


		$master=$this->input->post('id_master'); 			
		 
		if( $this->input->post('id_master') !== NULL && $this->check_inventory_status()){
			/* start update and prevent another update */
			$this->inventory_start();
			 
			/* GET all detail */
			$con=array();
			$con['master_id']=$master;			 
			$v=$this->bookman_model->get_master_transaksi_detail($con);
			if(count($v)>0){
				$keterangan='TRANS IN '.sprintf('%06d',$master);
				// CHECK TO PREVENT DUPLICATE
				$iv=$this->bookman_model->get_inventory(array('keterangan'=>$keterangan));
				if(count($iv)>0){
					$this->session->set_flashdata('pesan', 'Ada Yang Salah');
					$this->inventory_end();
					redirect('dashboard/inventory_detail_in/'.$master,'refresh');
				}
				/* INSERT INTO INVENTORY */
				foreach($v as $vdata){
					$val=array();
					$val['katalog_id']=$vdata->katalog_id;
					$val['masuk']=$vdata->masuk;
					$val['keterangan']='TRANS IN '.sprintf('%06d',$vdata->master_id);
					 $this->bookman_model->insert_inventory($val);
				}

				/* UPDATE MASTER STATUS */
				$con=array();
				$con['id_master']=$master;
				$val=array();
				$val['status_transaksi']=1;
				$retu=$this->bookman_model->update_master_transaksi($val,$con);
				if($retu){
					$this->session->set_flashdata('pesan', 'Data Berhasil Disimpan');
				}	else{
					$this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
				}

				/* END update and prevent another update */
				$this->inventory_end();				 
				redirect('dashboard/inventory_detail_in/'.$master,'refresh');
			}else{
				$this->session->set_flashdata('pesan', 'Tak bisa disimpan, BELUM ADA DATA');
				redirect('dashboard/inventory_detail_in/'.$master,'refresh');
			}
			 
		}else{
			$this->session->set_flashdata('pesan', 'Tak bisa disimpan, Ada yang salah');
			redirect('dashboard/inventory_detail_in/'.$master,'refresh');
		}


		
	 
	}

		/* ========================= */


		public function inventory_keluar(){
			$this->checklogin();		
			$data=array();
			$this->admintemplate->load('admin_inventory_out',$data);
		}
		// public function get_transaksi_master(){
	
					 
		// 	$this->datatables->select('id_master,tanggal_transaksi as tanggal,asal,keterangan,admin_id,nama,status_transaksi');
	
		// 	if($this->uri->segment(3)!=''){
		// 		$wh=$this->uri->segment(3);
		// 		if($wh==1 || $wh==2)
		// 		$this->datatables->where('jenis_transaksi',$wh);
		// 	}		 
		// 	$this->datatables->from('transaksi_master');
		// 	$this->datatables->join('admin', 'admin_id=id_admin');
			
		// 	$book= $this->datatables->generate();
		// 	// $temp=json_decode($book);
			
		// 	// foreach($temp->data as $d){
		// 	// 	$d->id_master=sprintf('%06d', $d->id_master);
		// 	// }
		// 	// $book=json_encode($temp);		
		// 	header('Content-Type: application/json');
		// 	echo $book;
		// }
	 
	public function master_transaksi_out_save(){
		if( $this->input->post('asal') !== NULL){
			$value=array();
			/* CHECK LOGIN */
			$value['jenis_transaksi']=$this->input->post('jenis');
			$value['asal']=$this->input->post('asal');
			$value['keterangan']=$this->input->post('keterangan');
			$value['status_transaksi']=0;
			$value['admin_id']=$this->session->userdata('UID');			 			
					 
			$r=$this->bookman_model->insert_master_transaksi($value);			
			$this->session->set_flashdata('pesan', 'Transaksi Baru '.sprintf('%06d', $r).' berhasil disimpan');
			redirect('dashboard/inventory_detail_out/'.$r,'refresh');
		}
	

			redirect('dashboard/inventory_keluar','refresh');
	}
	public function master_transaksi_out_update(){
		if( $this->input->post('id_master') !== NULL){
			$value=array();
			$con=array();
			$con['id_master']=$this->input->post('id_master');
			/* CHECK LOGIN */
			$value['asal']=$this->input->post('asal');
			$value['keterangan']=$this->input->post('keterangan');			 
			$value['admin_id']=$this->session->userdata('UID');				
						 
			$r=$this->bookman_model->update_master_transaksi($value,$con);			
			if($r){
				$this->session->set_flashdata('pesan', 'Perubahan data transaksi berhasil dilakukan');
			}	else{
				$this->session->set_flashdata('pesan', 'Perubahan data transaksi GAGAL dilakukan');
			}
		}

			redirect('dashboard/inventory_keluar','refresh');
	}
	public function master_transaksi_out_delete(){
		if( $this->input->post('id') !== NULL){
			 
			$con=array();
			$con['id_master']=$this->input->post('id');
			$con2['master_id']=$this->input->post('id');
			$this->bookman_model->delete_master_transaksi_detail($con2);			
			$r=$this->bookman_model->delete_master_transaksi($con);			
			if($r){
				$this->session->set_flashdata('pesan', 'Transaksi berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'Transaksi GAGAL dihapus');
			}
		}

		redirect('dashboard/inventory_keluar','refresh');
	}
	 
	
	public function inventory_detail_out(){
		$this->checklogin();		
		$id='';
		$master=array();
		if($this->uri->segment(3)!=''){
			$id=$this->uri->segment(3);
				$master=$this->bookman_model->get_master_transaksi(array('id_master'=>$id));
			if(count($master)>0)
				$master=$master[0];
		}else{
			redirect('dashboard/inventory_masuk','refresh');
		}
		$kategori=$this->bookman_model->get_kategory();
		$data['kategori']=$kategori;
		$data['master']=$master;		
		$data['id_master']=$id;
		$this->admintemplate->load('admin_inventory_out_detail',$data);
	}
	public function get_transaksi_master_detail_out(){

				 
		$this->datatables->select('id_detail,katalog_id,no_register,no_panggil,judul,pengarang,penerbit,bahasa,tahun,edisi,subyek,klasifikasi,deskripsi,isbn,keluar,masuk');

		if($this->uri->segment(3)!=''){
			$wh=$this->uri->segment(3);			
			$this->datatables->where('master_id',$wh);
		}		 
		$this->datatables->from('transaksi_master_detail ');
		$this->datatables->join('katalog', 'katalog_id=id_katalog');					
		$this->datatables->add_column('view', '<a href="javascript:void(0);" class="view_record btn btn-success btn-sm" data-id="$1" title="Lihat Detail Transaksi"><i class="fas fa-eye"></i></a>
											   <a href="javascript:void(0);" class="edit_record btn btn-primary btn-sm" data-id="$1"  title="edit Katalog"><i class="far fa-edit"></i></a> 
											   ',
											   'id_detail');
		 
		$book= $this->datatables->generate();
		$temp=json_decode($book);
		
		foreach($temp->data as $d){
			$d->stok=$this->get_stok_inventory($d->katalog_id);
		}
		$book=json_encode($temp);	
		header('Content-Type: application/json');
		echo $book;
	}
	public function ajax_check_stok(){

		if($this->input->post('id_katalog')){
			$id=$this->input->post('id_katalog');
			if($this->get_stok_inventory($id)>0){
				echo '1';
			}
			else{
				echo '0';
			}
		}else{
			echo '0';
		}
		
	}
	public function get_stok_inventory($x){
		 
		$result=$this->bookman_model->get_stok_inventory(array('katalog_id'=>$x));
		if($result>0){
			return $result[0]->stok;
		}else{
			return 0;
		}

	}
	public function master_detail_out_add(){
		$master=$this->input->post('id_master');
		if( $this->input->post('id_katalog') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['master_id']=$this->input->post('id_master');
			$con['katalog_id']=$this->input->post('id_katalog');
			$v=$this->bookman_model->get_master_transaksi_detail($con);
			if(count($v)>0){
				$this->session->set_flashdata('pesan', 'data Sudah ada');
			}else{

				$value=array();
				/* CHECK LOGIN */
				$value['master_id']=$this->input->post('id_master');
				$value['katalog_id']=$this->input->post('id_katalog');
				$value['keluar']=$this->input->post('jumlah');			
						
				$r=$this->bookman_model->insert_master_transaksi_detail($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');
			}
			redirect('dashboard/inventory_detail_out/'.$master,'refresh');
		}
	

		redirect('dashboard/inventory_detail_out/'.$master,'refresh');
	}
	public function master_detail_out_update(){		
		if( $this->input->post('id_detail') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['id_detail']=$this->input->post('id_detail');
			$val=array();
			$val['keluar']=$this->input->post('jumlah');
			$v=$this->bookman_model->update_master_transaksi_detail($val,$con);
			if($v){
				echo '1';
			}else{
				echo '0';
			}
			
		}		
	}
	public function master_detail_out_delete(){

		$master=$this->input->post('id_master'); 	
		if( $this->input->post('id_detail') !== NULL){
			 
			$con=array();
			$con['id_detail']=$this->input->post('id_detail');			 
			$r=$this->bookman_model->delete_master_transaksi_detail($con);			
			if($r){
				$this->session->set_flashdata('pesan', 'data berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'data GAGAL dihapus');
			}
		}

		redirect('dashboard/inventory_detail_out/'.$master,'refresh');
	}
	public function master_detail_out_save(){

		$master=$this->input->post('id_master'); 			
		 
		if( $this->input->post('id_master') !== NULL && $this->check_inventory_status()){
			 
			/* start update and prevent another update */
			$this->inventory_start();
			/* GET all detail */
			$con=array();
			$con['master_id']=$master;			 
			$v=$this->bookman_model->get_master_transaksi_detail($con);
			if(count($v)>0){
				$keterangan='TRANS IN '.sprintf('%06d',$master);
				// CHECK TO PREVENT DUPLICATE
				$iv=$this->bookman_model->get_inventory(array('keterangan'=>$keterangan));
				if(count($iv)>0){
					$this->session->set_flashdata('pesan', 'Ada Yang Salah');
					$this->inventory_end();
					redirect('dashboard/inventory_detail_in/'.$master,'refresh');
				}
				/* INSERT INTO INVENTORY */
				foreach($v as $vdata){
					$val=array();
					$val['katalog_id']=$vdata->katalog_id;
					$val['keluar']=$vdata->keluar;
					$val['keterangan']='TRANS OUT '.sprintf('%06d',$vdata->master_id);
					$this->bookman_model->insert_inventory($val);
				}

				/* UPDATE MASTER STATUS */
				$con=array();
				$con['id_master']=$master;
				$val=array();
				$val['status_transaksi']=1;
				$retu=$this->bookman_model->update_master_transaksi($val,$con);

				/* END update and prevent another update */
				$this->inventory_end();
				if($retu){
					$this->session->set_flashdata('pesan', 'Data Berhasil Disimpan');
				}	else{
					$this->session->set_flashdata('pesan', 'Data Gagal Disimpan');
				}
				
				redirect('dashboard/inventory_detail_out/'.$master,'refresh');
			}else{
				$this->session->set_flashdata('pesan', 'Tak bisa disimpan, BELUM ADA DATA');
				redirect('dashboard/inventory_detail_out/'.$master,'refresh');
			}
			 
		}

		
	 
	}
	
	public function testing(){
		$this->session->set_flashdata('pesan', 'Buku dengan id 1 berhasil disimpan');
		redirect('dashboard/master_katalog','refresh');

	}

	public function get_book_master(){
		 
		$this->datatables->select('id_katalog,no_register,no_panggil,judul,pengarang,penerbit,kategori_id,nama_kategori,bahasa,tahun,edisi,subyek,klasifikasi,deskripsi,isbn');
		$this->datatables->from('katalog');
		$this->datatables->join('katalog_kategori', 'kategori_id=id_kategori');
		$this->datatables->add_column('view', '<a href="javascript:void(0);" class="view_record btn btn-success btn-sm" data-id="$1" title="Lihat Detail Katalog"><i class="fas fa-eye"></i></a>
											   <a href="javascript:void(0);" class="edit_record btn btn-primary btn-sm" data-id="$1"  title="edit Katalog"><i class="far fa-edit"></i></a> 
											   <a href="javascript:void(0);" class="delete_record btn btn-danger btn-sm" data-id="$1" title="Hapus Katalog"><i class="fas fa-trash"></i></a>',
											   'id_katalog');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
	}
	public function get_book_stok_master(){
		 
		$this->datatables->select('idx,  data_masuk,  data_keluar,judul,pengarang,penerbit,kategori_id, stok,nama_kategori');
		$this->datatables->from('stok_inventory');				
		$this->datatables->join('katalog', 'idx=katalog.id_katalog'); 
		$this->datatables->join('katalog_kategori', 'kategori_id=id_kategori');
		
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
	}
	public function get_katalog_json(){
		 
		$id=$this->uri->segment(3);
		if($id==null)
			die();
		$katalog=$this->bookman_model->get_katalog(array('id_katalog'=>$id));
		 
		header('Content-Type: application/json');
		echo  json_encode($katalog[0]);
	}

	public function kategori_katalog(){
		$this->checklogin();
		$kategori=$this->bookman_model->get_kategory();
		$data['kategori']=$kategori;
		$this->admintemplate->load('admin_kategori_katalog',$data);
	}

	public function buku()
	{
		$this->checklogin();

		$this->admintemplate->load('admin_buku');
		// $this->load->view('dashboard');
	}
	public function get_book(){
		 
			$this->datatables->select('product_code,product_name,product_price,category_id,category_name');
			$this->datatables->from('product');
			$this->datatables->join('categories', 'product_category_id=category_id');
			$this->datatables->add_column('view', '<a href="javascript:void(0);" class="edit_record btn btn-info" data-code="$1" data-name="$2" data-price="$3" data-category="$4">Edit</a>  <a href="javascript:void(0);" class="delete_record btn btn-danger" data-code="$1">Delete</a>','product_code,product_name,product_price,category_id,category_name');
			$book= $this->datatables->generate();
			header('Content-Type: application/json');
			echo $book;
	}
	public function kunjungan()
	{
		$this->checklogin();

		$this->admintemplate->load('admin_riwayat_kunjungan');
		// $this->load->view('dashboard');
	}
	public function get_riwayat_kunjungan()
	{
			$this->datatables->select('id_kunjungan,waktu,member_id,nama');
			$this->datatables->from('kunjungan');
			$this->datatables->join('member', 'member_id=id_member');			
			$book= $this->datatables->generate();
			header('Content-Type: application/json');
			echo $book;
	}
	public function user_admin(){
		$this->checklogin();

		$this->admintemplate->load('admin_user_admin');
	}
	public function get_user_admin(){
		$this->checklogin();
		$this->datatables->select('id_admin,nama,username,email,password,akses,status');
		$this->datatables->from('admin');		
		$this->datatables->add_column('view',  '<a href="javascript:void(0);" class="edit_record btn btn-primary btn-sm" data-id="$1" data-nama="$2" data-username="$3" data-email="$4" data-akses="$5" data-status="$6"><i class="far fa-edit"></i></a> '.
											   '<a href="javascript:void(0);" class="delete_record btn btn-danger btn-sm" data-id="$1"><i class="fas fa-trash"></i></a>',
											   'id_admin,nama,username,email,akses,status');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
	}
	public function checkadminusername(){

		if( $this->input->post('username') !== NULL){
			$con=array();
			$con['username']=$this->input->post('username');
			$v=$this->bookman_model->get_user_admin($con);
			if(count($v)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
	}
	public function admin_user_add(){
		
		if( $this->input->post('submit') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['username']=$this->input->post('username');
			$v=$this->bookman_model->get_user_admin($con);
			if(count($v)>0){
				$this->session->set_flashdata('pesan', 'Username sudah dipakai');
			}else{

				$value=array();
				$value['nama']=$this->input->post('nama');
				$value['username']=$this->input->post('username');
				$value['email']=$this->input->post('email');
				$value['password']=md5($this->input->post('username'));
				$value['akses']=$this->input->post('akses');
				if($this->input->post('status')!==null){
					$value['status']=$this->input->post('status');
				}else{
					$value['status']=0;
				}		
						
				$r=$this->bookman_model->insert_user_admin($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');
			}
			redirect('dashboard/user_admin','refresh');
		}
	

		redirect('dashboard/user_admin/','refresh');
		 
	}
	public function admin_user_update(){
		
		if( $this->input->post('id') !='0'){
			
			/* CHECK IF EXIST */
		 
				$con=array();
				$value=array();
				$con['id_admin']=$this->input->post('id');
				$value['nama']=$this->input->post('nama');
				$value['username']=$this->input->post('username');
				$value['email']=$this->input->post('email');				
				$value['akses']=$this->input->post('akses');
				if($this->input->post('password')!='')
					$value['password']=md5($this->input->post('username'));
				if($this->input->post('status')!==null){
					$value['status']=$this->input->post('status');
				}else{
					$value['status']=0;
				}		
						
				$r=$this->bookman_model->update_user_admin($value,$con);			
				$this->session->set_flashdata('pesan', 'data diupdate');
			 
			redirect('dashboard/user_admin','refresh');
		}
	

		redirect('dashboard/user_admin/','refresh');
		 
	}
	public function admin_user_delete(){
		
		if( $this->input->post('id') !== NULL){
			 
			$con=array();
			$con['id_admin']=$this->input->post('id');
			 
			$r=$this->bookman_model->delete_user_admin($con);			
			if($r){
				$this->session->set_flashdata('pesan', 'user berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'user GAGAL dihapus');
			}
		}

			

		redirect('dashboard/user_admin/','refresh');
		 
	}
	public function user_member(){
		$this->checklogin();

		$data['provinsi']=$this->bookman_model->get_province();

		$this->admintemplate->load('admin_user_member',$data);
	}
	public function get_user_member(){
		$this->checklogin();
		$this->datatables->select('id_member,nama,identitas,alamat,kec,kota,provinsi,kodepos,telp,email,password,tgl_daftar,type,status');
		$this->datatables->from('member');		
		$this->datatables->add_column('view',  '<a href="javascript:void(0);" class="edit_record btn btn-primary btn-sm" data-id="$1" title="Ubah Data"><i class="far fa-edit"></i></a> '.
											   '<a href="javascript:void(0);" class="ganti_password btn btn-warning btn-sm" data-id="$1" title="Set Password"><i class="fas fa-key"></i></a> '.
											   '<a href="javascript:void(0);" class="delete_record btn btn-danger btn-sm" data-id="$1" title="Hapus Data"><i class="fas fa-trash"></i></a>',
											   'id_member');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
	}
	public function getkota(){
		$result=array();		
		if($this->input->post('province_id')!==null){
			
			$con=array();
			$con['province_id']=$this->input->post('province_id');
			$kota=$this->bookman_model->get_regency($con);
			$result=$kota;
		}
		echo json_encode($result);
		 
	}
	public function getkec(){
		$result=array();		
		if($this->input->post('regency_id')!==null){
			
			$con=array();
			$con['regency_id']=$this->input->post('regency_id');
			$kota=$this->bookman_model->get_district($con);
			$result=$kota;
		}
		echo json_encode($result);
		 
	}
	public function getdesa(){
		$result=array();		
		if($this->input->post('district_id')!==null){
			
			$con=array();
			$con['district_id']=$this->input->post('district_id');
			$kota=$this->bookman_model->get_village($con);
			$result=$kota;
		}
		echo json_encode($result);
		 
	}
	public function checkidentitas(){

		if( $this->input->post('identitas') !== NULL){
			$con=array();
			$con['identitas']=$this->input->post('identitas');
			$v=$this->bookman_model->get_member($con);
			if(count($v)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
	}
	public function checkemail(){

		if( $this->input->post('email') !== NULL){
			$con=array();
			$con['email']=$this->input->post('email');
			$v=$this->bookman_model->get_member($con);
			if(count($v)>0){
				echo '0';
			}else{
				echo '1';
			}
		}else{
			echo '0';
		}
	}
	public function member_user_add(){
		
		if( $this->input->post('submit') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['identitas']=$this->input->post('identitas');
			$v=$this->bookman_model->get_member($con);
			if(count($v)>0){
				$this->session->set_flashdata('pesan', 'Identitas sudah dipakai');
			}else{

				$value=array();
				$value['nama']=$this->input->post('nama');
				$value['identitas']=$this->input->post('identitas');
				$value['alamat']=$this->input->post('alamat');
				$value['kec']=$this->input->post('kec');
				$value['kota']=$this->input->post('kota');
				$value['provinsi']=$this->input->post('provinsi');
				$value['kodepos']=$this->input->post('kodepos');
				$value['telp']=$this->input->post('telp');
				$value['email']=$this->input->post('email');				
				$value['password']=md5($this->input->post('password'));				
				$value['type']=$this->input->post('tipe');
				
				if($this->input->post('status')!==null){
					$value['status']=$this->input->post('status');
				}else{
					$value['status']=0;
				}		
						
				$r=$this->bookman_model->insert_member($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');
			}
			redirect('dashboard/user_member','refresh');
		}
	

		redirect('dashboard/user_member/','refresh');
		 
	}
	public function get_member_json(){
		 
		$id=$this->uri->segment(3);
		if($id==null)
			die();
		$user=$this->bookman_model->get_member(array('id_member'=>$id));
		$result=array();
		if(count($user)>0){
			$result['status']=1;
			$result['result']=$user[0];
		}else{
			$result['status']=0;
			$result['result']=array();;
		}
		header('Content-Type: application/json');
		
		echo  json_encode($result);
	}
	public function member_user_update(){
		
		if( $this->input->post('id') !='0'){
			
			/* CHECK IF EXIST */
		 
				$con=array();
				$value=array();
				$con['id_member']=$this->input->post('id');
				$value['nama']=$this->input->post('nama');
				$value['identitas']=$this->input->post('identitas');
				$value['alamat']=$this->input->post('alamat');
				$value['kec']=$this->input->post('kec');
				$value['kota']=$this->input->post('kota');
				$value['provinsi']=$this->input->post('provinsi');
				$value['kodepos']=$this->input->post('kodepos');
				$value['telp']=$this->input->post('telp');
				$value['email']=$this->input->post('email');								
				$value['type']=$this->input->post('tipe');
				
				if($this->input->post('status')!==null){
					$value['status']=$this->input->post('status');
				}else{
					$value['status']=0;
				}		
						
				$r=$this->bookman_model->update_member($value,$con);			
				$this->session->set_flashdata('pesan', 'data diupdate');
			 
			redirect('dashboard/user_member','refresh');
		}
	

		redirect('dashboard/user_member/','refresh');
		 
	}
	public function member_user_delete(){
		
		if( $this->input->post('id') !== NULL){
			 
			$con=array();
			$con['id_member']=$this->input->post('id');
			 
			$r=$this->bookman_model->delete_member($con);			
			if($r){
				$this->session->set_flashdata('pesan', 'user berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'user GAGAL dihapus');
			}
		}

			

		redirect('dashboard/user_member/','refresh');
		 
	}

	public function pinjaman(){
		$this->checklogin();

		$this->admintemplate->load('admin_list_pinjaman');
	}
	public function pinjaman_detail(){
		$this->checklogin();		
		$id='';
		$master=array();
		$nama='';
		if($this->uri->segment(3)!=''){
			$id=$this->uri->segment(3);
				$master=$this->bookman_model->get_pinjaman(array('id_pinjam'=>$id));
			if(count($master)>0){
				$master=$master[0];
				$member=$this->bookman_model->get_member(array('id_member'=>$master->member_id));
				if(count($member)>0){
					$nama=$member[0]->nama;
				}
			}
		}else{
			redirect('dashboard/pinjaman','refresh');
		}
		
		$data['nama']=$nama;
		$data['id_master']=$id;
		$data['master']=$master;
		$this->admintemplate->load('admin_list_pinjaman_detail',$data);
	}
	public function get_pinjaman(){
		$this->checklogin();
		$this->datatables->select('id_pinjam,member_id,nama,telp,admin_id,tanggal,keterangan,status_pinjam,email');
		$this->datatables->from('peminjaman');	
		
		$this->datatables->join('member', 'member_id=id_member');
		$this->datatables->add_column('view',  '<a href="'.base_url().'dashboard/pinjaman_detail/$1" class="btn btn-transparent text-primary bg-light"  title="Proses Data"><i class="far fa-hand-point-right"></i></a> '.											   
											   '<a href="javascript:void(0);" class="delete_record btn btn-danger btn-sm" data-id="$1" title="Hapus Data"><i class="fas fa-trash"></i></a>',
											   'id_pinjam');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
	}
	public function pinjam_baru(){
		
		if( $this->input->post('submit') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['member_id']=$this->input->post('nomor');
			$v=$this->bookman_model->get_pinjaman_aktif($con);
			if(count($v)>0){
				$this->session->set_flashdata('pesan', 'Ada Tanggungan, Tidak Bisa membuat pinjaman baru');
			}else{

				$bytes = random_bytes(4);				
				$id=bin2hex($bytes);

				// generate ID if exist
				while(true){
					if($this->checkpinjamid($id)){
						$bytes = random_bytes(4);				
						$id=bin2hex($bytes);
					}else{
						break;
					}
				} 
				
				$value=array();
				$value['id_pinjam']=$id;
				$value['member_id']=$this->input->post('nomor');
				$value['status_pinjam']=0;				 
				$value['admin_id']=$this->session->userdata('UID');				 
						
				$r=$this->bookman_model->insert_pinjaman($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');
			}
			redirect('dashboard/pinjaman_detail/'.$id,'refresh');
		}
	

		redirect('dashboard/pinjaman_detail/'.$id,'refresh');
		 
	}


	public function checkpinjamid($id){

		$con=array('id_pinjam'=>$id);
		$r=$this->bookman_model->get_pinjaman($con);
		if(count($r)>0){
			return true;
		}else{
			return false;
		}
	}

	public function peminjaman_delete(){
		
		if( $this->input->post('id') !== NULL){

			/* check kondisi jika ada buku yang sedang aktif dipinjam */
			$con=array();
			$con['id_pinjam']=$this->input->post('id');
			$a=$this->bookman_model->get_pinjaman_aktif($con);
			if(count($a)>0){
				$this->session->set_flashdata('pesan', 'Ada Pinjaman Aktif, Pinjaman tidak dapat dihapus');
			}else{
			 
				$con=array();
				$con['id_pinjam']=$this->input->post('id');
				
				$r=$this->bookman_model->delete_pinjaman($con);			
				if($r){
					$this->session->set_flashdata('pesan', 'Pinjaman berhasil dihapus');
				}	else{
					$this->session->set_flashdata('pesan', 'Pinjaman GAGAL dihapus');
				}
			}
		}

			

		redirect('dashboard/pinjaman/','refresh');
		 
	}

	public function get_pinjaman_detail(){
		$this->checklogin();
		$this->datatables->select('id_detail,no_panggil,judul,pengarang,tahun,isbn,status');
		if($this->uri->segment(3)!=''){
			$wh=$this->uri->segment(3);			
			$this->datatables->where('pinjam_id',$wh);
		}	
		$this->datatables->from('peminjaman_detail');	
		
		$this->datatables->join('katalog', 'katalog_id=id_katalog');
		$this->datatables->add_column('view',  '<a href="'.base_url().'dashboard/pinjaman_detail/$1" class="btn btn-primary btn-sm"  title="Proses Data"><i class="fas fa-dot-circle"></i></a> '.											   
											   '<a href="javascript:void(0);" class="delete_record btn btn-danger btn-sm" data-id="$1" title="Hapus Data"><i class="fas fa-times"></i></a>',
											   'id_pinjam');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
	}

	public function pinjam_detail_add(){
		 
		
		$master=$this->input->post('id_pinjam');
		if( $this->input->post('id_katalog') !== NULL){
			
			/* CHECK IF EXIST */
			$con=array();
			$con['pinjam_id']=$this->input->post('id_pinjam');
			$con['katalog_id']=$this->input->post('id_katalog');
			$v=$this->bookman_model->get_pinjaman_detail($con);
			if(count($v)>0){
				$this->session->set_flashdata('pesan', 'data Sudah ada');
			}else{
				 
				$value=array();
				/* CHECK LOGIN */
				$value['pinjam_id']=$this->input->post('id_pinjam');
				$value['katalog_id']=$this->input->post('id_katalog');					
						
				$r=$this->bookman_model->insert_pinjaman_detail($value);			
				$this->session->set_flashdata('pesan', 'data ditambahkan');			
			}
			 

			redirect('dashboard/pinjaman_detail/'.$master,'refresh');
		}
	

		redirect('dashboard/pinjaman_detail/'.$master,'refresh');
	}
	public function pinjam_detail_ajax_add(){
		 
		 

		$master=$this->input->post('id_pinjam');
		if( $this->input->post('nomor') !== NULL){
			
			/* check if nomor is exist on DB */
			$searchby=$this->input->post('searchby');
			$nomor=$this->input->post('nomor');
			$con=array();
			switch($searchby){
				case 'isbn':
					$con['isbn']=$nomor; 
					break;
				case 'id':
					$con['id_katalog']=$nomor; 
					break;
				case 'register':
					$con['no_register']=$nomor; 
					break;
				case 'panggil':
					$con['no_panggil']=$nomor; 
					break;
				default :{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Search By Harus Dipilih';
					echo json_encode($r); 
					die();
				}
			}
			$katalog=$this->bookman_model->get_katalog($con);
			if(count($katalog)==0){
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Data Dengan Nomor '.$nomor.' TIDAK DITEMUKAN!';
					echo json_encode($r); 
					die();
			}

			/* CHECK IF EXIST */
			$con=array();
			$con['pinjam_id']=$this->input->post('id_pinjam');
			$con['katalog_id']=$katalog[0]->id_katalog;
			$v=$this->bookman_model->get_pinjaman_detail($con);
			if(count($v)>0){
				$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Nomor '.$nomor.' Sudah DITAMBAHKAN';
					echo json_encode($r); 
					die();
			}else{
				 
				$value=array();
				/* CHECK LOGIN */
				$value['pinjam_id']=$this->input->post('id_pinjam');
				$value['katalog_id']=$katalog[0]->id_katalog;					
						
				$rx=$this->bookman_model->insert_pinjaman_detail($value);			
				if($rx){
					$r=array();
					$r['status']=1;
					$r['msg']='Berhasil! Nomor '.$nomor.' DITAMBAHKAN';
					echo json_encode($r); 
					die();
				}else{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! ADA YANG SALAH';
					echo json_encode($r); 
					die();
				}	
			}
			 

			
		}else{
			$r=array();
			$r['status']=0;
			$r['msg']='GAGAL! ID Tidak Ditemukan';
			echo json_encode($r);
			die();
		}
	

		
	}

	public function pinjam_detail_delete(){
		$idpinjam=$this->input->post('id_pinjam');
		if( $this->input->post('id_detail') !== NULL){			 
			
			$con=array();			 
			$con['id_detail']=$this->input->post('id_detail');
			$r=$this->bookman_model->delete_pinjaman_detail($con);			
			 	
			if($r){
				$this->session->set_flashdata('pesan', 'berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'GAGAL dihapus');
			}
		}

		
		redirect('dashboard/pinjaman_detail/'.$idpinjam,'refresh');
	}
	public function pinjam_detail_save(){
		$idpinjam=$this->input->post('id_pinjam');
		
		if( $this->input->post('id_pinjam') !== NULL){			 
			
			/* GET list BUKU yang DIpinjam */
			$c=array();
			$c['pinjam_id']=$idpinjam;
			$book=$this->bookman_model->get_pinjaman_detail($c);
			if(count($book)==0){
				$this->session->set_flashdata('pesan', 'List Buku Pinjaman Tidak Boleh Kosong');
			 	redirect('dashboard/pinjaman_detail/'.$idpinjam,'refresh');
				die();
			}
		
			
			/* MASUKKAN BUKU kedalam transaksi */
			$this->inventory_start();
			foreach($book as $buku){

				/* CHECK STOK BUKU */
				$c=array();
				$c['katalog_id']=$buku->katalog_id;
				$r=$this->bookman_model->get_stok_inventory($c);
				if($r[0]->stok < 1){
					/* stok buku kosong */

					/* hapus buku dari list pinjaman */
					$x=array();
					$x['katalog_id']=$buku->katalog_id;
					$this->bookman_model->delete_pinjaman_detail($x);
				}else{
					/* insert transaksi buku */

						$keterangan='BOOK OUT '.$idpinjam;
						$c2=array();
						$c2['keterangan']=$keterangan;
						$c2['katalog_id']=$buku->katalog_id;
						// CHECK TO PREVENT DUPLICATE
						$iv=$this->bookman_model->get_inventory($c2);
						if(count($iv)>0){
							continue;
						}else{
						/* INSERT INTO INVENTORY */						 
							$val=array();
							$val['katalog_id']=$buku->katalog_id;
							$val['keluar']=1;
							$val['keterangan']='BOOK OUT '.$idpinjam;
							$id=array();
							$id=$this->bookman_model->insert_inventory($val);					 

							/* UPDATE PINJAMAN DETAIL */
							$con=array();
							$con['id_detail']=$buku->id_detail;
							$v=array();
							$v['inventory_keluar_id']=$id;
							$v['status']=1;
							$this->bookman_model->update_pinjaman_detail($v,$con);
						}

					
				}
				
			}
			$this->inventory_end();

			 
			/* UPDATE STATUS PINJAMAN */
			$cond=array();
			$cond['id_pinjam']=$idpinjam;
			$va=array();
			$va['status_pinjam']=1;
			$this->bookman_model->update_pinjaman($va,$cond);			 
		}

		
 	 redirect('dashboard/pinjaman_detail/'.$idpinjam,'refresh');
	}

	public function pinjam_detail_return(){
		$idpinjam=$this->input->post('id_pinjam');
		if( $this->input->post('id_detail') !== NULL){			 
			
			
			/* GET transaction detail */			
			$con=array();			 
			$con['id_detail']=$this->input->post('id_detail');
			$con['status']=1;
			$r=$this->bookman_model->get_pinjaman_detail($con);	
			if(count($r)>0){
				$this->inventory_start();

				$detail=$r[0];
				/* transaksi buku masuk */
				$val=array();
				$val['katalog_id']=$detail->katalog_id;
				$val['masuk']=1;
				$val['keterangan']='BOOK IN '.$idpinjam;
				$id=array();
				$id=$this->bookman_model->insert_inventory($val);	


				/* UPDATE PINJAMAN DETAIL */
				$con=array();
				$con['id_detail']=$detail->id_detail;
				$v=array();
				$v['inventory_masuk_id']=$id;
				$v['status']=2;
				$v['tanggal_kembali']=date("Y-m-d H:i:s");
				$v['admin_kembali_id']=$this->session->userdata('UID');	
				$this->bookman_model->update_pinjaman_detail($v,$con);
				$this->inventory_end();
			}		
				 

			/* CHECK PINJAMAN TERSISA */
			$c=array();
			$c['pinjam_id']=$idpinjam;
			$c['status!=']=2;
			$pinjaman=$this->bookman_model->get_pinjaman_detail($c);

			if(count($pinjaman)==0){
				/* UPDATE STATUS PINJAMAN */
				$cond=array();
				$cond['id_pinjam']=$idpinjam;
				$va=array();
				$va['status_pinjam']=2;
				$this->bookman_model->update_pinjaman($va,$cond);	
			}
			 
		}

		
		redirect('dashboard/pinjaman_detail/'.$idpinjam,'refresh');
	}

	public function daftar_buku_dipinjam(){
		$this->checklogin();

		$this->admintemplate->load('admin_pinjaman_aktif');
	}
	public function get_pinjaman_aktif(){
		$this->checklogin();
		$this->datatables->select('id_pinjam,katalog_id,judul,member_id,admin_id,tanggal_pinjam,no_register,pengarang,status');
		$this->datatables->from('peminjaman_aktif');			
		
		$this->datatables->join('katalog', 'katalog_id=id_katalog');		
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
	}




	public function pinjaman_online(){
		$this->checklogin();

		
		// set status canceled on expired booking
		$con['date(tanggal_pinjam)<']=date('Y-m-d');
		$con['status']='0';
		$val['status']=2; 				// STATUS 0=belum_diproses, 1=sudah_diproses, 2=BATAL
		$this->bookman_model->update_booking($val,$con);

		$this->admintemplate->load('admin_booking_list');
	}

	public function get_pinjaman_online(){
        if(!$this->session->userdata('UID')){
            echo array();
            die();
				
        }       
		$this->datatables->select('id_booking,member_id,tanggal_booking,tanggal_pinjam,status');
		$this->datatables->from('booking');	         
        $this->datatables->add_column('view',  '<a href="'.base_url().'dashboard/booking_detail/$1" class="btn btn-transparent  "  title="cek list pinjaman"><i class="far fa-hand-point-right"></i></a> '											   
											   ,'id_booking');
		$book= $this->datatables->generate();
		header('Content-Type: application/json');
		echo $book;
		 
	}
	public function get_pinjaman_online_detail(){
		$this->checklogin();
		$this->datatables->select('id_booking_detail,no_panggil,judul,pengarang,tahun,isbn,katalog_id');
		if($this->uri->segment(3)!=''){
			$wh=$this->uri->segment(3);			
			$this->datatables->where('booking_id',$wh);
		}	
		$this->datatables->from('booking_detail');	
		
		$this->datatables->join('katalog', 'katalog_id=id_katalog');		
		$this->datatables->add_column('view',  '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger " data-id="$1" title="Hapus Buku"><i class="fas fa-times"></i></a>',
											   'id_booking_detail');
		$book= $this->datatables->generate();
		$konten=json_decode($book);
		foreach($konten->data as $da){
			$da->stok=$this->get_stok_inventory($da->katalog_id);
		}
		$book=json_encode($konten);
		header('Content-Type: application/json');
		echo $book;
		 
	}
	 
	public function booking_detail(){
		$this->checklogin();		
		$id='';
		$master=array();
		$nama='';
		if($this->uri->segment(3)!=''){
			$id=$this->uri->segment(3);
				$master=$this->bookman_model->get_booking_online(array('id_booking'=>$id));
			if(count($master)>0){
				$master=$master[0];
				$member=$this->bookman_model->get_member(array('id_member'=>$master->member_id));
				if(count($member)>0){
					$nama=$member[0]->nama;
				}
			}
		}else{
			redirect('dashboard/pinjaman_online','refresh');
		}
		
		$data['nama']=$nama;
		$data['id_master']=$id;
		$data['master']=$master;
		$this->admintemplate->load('admin_booking_detail',$data);
	}

	public function pinjam_online_detail_delete(){
		$id_booking=$this->input->post('id_booking');
		if( $this->input->post('id_detail') !== NULL){			 
			
			$con=array();			 
			$con['id_booking_detail']=$this->input->post('id_detail');
			$r=$this->bookman_model->delete_booking_detail($con);			
			 	
			if($r){
				$this->session->set_flashdata('pesan', 'berhasil dihapus');
			}	else{
				$this->session->set_flashdata('pesan', 'GAGAL dihapus');
			}
		}

		
		redirect('dashboard/booking_detail/'.$id_booking,'refresh');
	}

	public function proses_booking(){
		if( $this->input->post('id_booking') !== NULL){		
			$id_booking=$this->input->post('id_booking');
			 

			/* GET BOOKING  */
			$con=array();
			$con['id_booking']=$id_booking;
			$booking=$this->bookman_model->get_booking($con);

			// get LIST BOOK
			$con=array();
			$con['booking_id']=$id_booking;
			$detail=$this->bookman_model->get_booking_detail($con);

			/* if booking avaiable with detail */
			if(count($booking)>0 && count($detail)>0){

				
				
				/* BUAT PINJAMAN */
				/* CHECK jika sudah ada pinjaman */
				$con=array();
				$con['member_id']=$booking[0]->member_id;
				$v=$this->bookman_model->get_pinjaman_aktif($con);
				if(count($v)>0){		
					 
					$this->session->set_flashdata('pesan', 'Ada Tanggungan, Tidak Bisa membuat pinjaman baru');
					redirect('dashboard/booking_detail/'.$id_booking,'refresh');
				}else{
					 
					$bytes = random_bytes(4);				
					$id=bin2hex($bytes);
					// generate ID if exist
					while(true){
						if($this->checkpinjamid($id)){
							$bytes = random_bytes(4);				
							$id=bin2hex($bytes);
						}else{
							break;
						}
					} 				
					$value=array();
					$value['id_pinjam']=$id;
					$value['member_id']=$booking[0]->member_id;
					$value['status_pinjam']=0;				 
					$value['admin_id']=$this->session->userdata('UID');
					$r=$this->bookman_model->insert_pinjaman($value);	
					
					



					/* ISI DATA PINJAMAN */
					foreach ($detail as $dd) {
						/* check stok detail */
						$stok=$this->get_stok_inventory($dd->katalog_id);
						if($stok==0)
							continue;

						/* CHECK IF EXIST */
						$con=array();
						$con['pinjam_id']=$id;
						$con['katalog_id']=$dd->katalog_id;
						$v=$this->bookman_model->get_pinjaman_detail($con);
						if(count($v)>0){
							continue;
						}else{
							
							$value=array();							
							$value['pinjam_id']=$id;
							$value['katalog_id']=$dd->katalog_id;									
							$r=$this->bookman_model->insert_pinjaman_detail($value);			
								
						}
					}

					/* UPDATE PINJAMAN ONLINE STATUS */	
					$con=array();
					$val=array();				
					$con['id_booking']=$id_booking;					 
					$val['status']=1; 				// STATUS 0=belum_diproses, 1=sudah_diproses, 2=BATAL
					$this->bookman_model->update_booking($val,$con);

					redirect('dashboard/pinjaman_detail/'.$id,'refresh');
					
				}



				
			}
			
		}
		redirect('dashboard/pinjaman_online/','refresh');
	}

	public function pengembalian(){
		$this->checklogin();		
		 

		$this->admintemplate->load('admin_pengembalian');
	}


	public function get_riwayat_pengembalian(){
		$this->checklogin();
		$this->datatables->select('tanggal_kembali,judul,pengarang,tahun,pinjam_id,katalog_id');
		 		
			$this->datatables->where('tanggal_kembali IS NOT NULL');			
		 
		$this->datatables->from('peminjaman_detail');			 
		$this->datatables->join('katalog', 'katalog_id=id_katalog');	
		 

		 
		$book= $this->datatables->generate();
		$konten=json_decode($book);
		foreach($konten->data as $da){
			$con=array();
			$con['id_pinjam']=$da->pinjam_id;
			$peminjaman=$this->bookman_model->get_pinjaman($con);
			if(count($peminjaman)>0){
				$con2=array();
				$con2['id_member']=$peminjaman[0]->member_id;
				$member=$this->bookman_model->get_member($con2);
				if(count($member)>0){
					$da->nama=$member[0]->nama;
				}else{
					$da->nama='Unknown';	
				 
				}

			}else{
				$da->nama='Unknown';
			 
			}
			
		}
		$book=json_encode($konten);		
		header('Content-Type: application/json');
		echo $book;
		 
	}

	public function pengembalian_ajax(){
		 
		if( $this->input->post('nomor') !== NULL){			 
			
			$searchby=$this->input->post('searchby');
			$nomor=$this->input->post('nomor');

			/* GET KATALOG ID */
			switch($searchby){
				case 'isbn':
					$con['isbn']=$nomor; 
					break;
				case 'id':
					$con['id_katalog']=$nomor; 
					break;
				case 'register':
					$con['no_register']=$nomor; 
					break;
				case 'panggil':
					$con['no_panggil']=$nomor; 
					break;
				default :{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Search By Harus Dipilih';
					echo json_encode($r); 
					die();
				}
			}
			$katalog=$this->bookman_model->get_katalog($con);
			if(count($katalog)==0){
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Data Dengan Nomor '.$nomor.' TIDAK DITEMUKAN!';
					echo json_encode($r); 
					die();
			}

			/* CHECK JIKA DITEMUKA BUKU YANG DIPINJAM */
			$con=array();
			$con['katalog_id']=$katalog[0]->id_katalog;			 
			$datakatalog=$this->bookman_model->get_pinjaman_aktif($con);

			$jumlah=count($datakatalog);
			if($jumlah==0){
				$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! Data Dengan Nomor '.$nomor.' TIDAK DITEMUKAN!';
					echo json_encode($r); 
					die();
			}else if($jumlah==1){

				/* GET transaction detail */			
				$con=array();			 
				$con['id_detail']=$datakatalog[0]->id_detail;
				$con['status']=1;
				$r=$this->bookman_model->get_pinjaman_detail($con);	
				if(count($r)>0){
					$this->inventory_start();

					$detail=$r[0];
					/* transaksi buku masuk */
					$val=array();
					$val['katalog_id']=$detail->katalog_id;
					$val['masuk']=1;
					$val['keterangan']='BOOK IN '.$datakatalog[0]->id_pinjam;
					$id=array();
					$id=$this->bookman_model->insert_inventory($val);	


					/* UPDATE PINJAMAN DETAIL */
					$con=array();
					$con['id_detail']=$detail->id_detail;
					$v=array();
					$v['inventory_masuk_id']=$id;
					$v['status']=2;
					$v['tanggal_kembali']=date("Y-m-d H:i:s");
					$v['admin_kembali_id']=$this->session->userdata('UID');	
					$this->bookman_model->update_pinjaman_detail($v,$con);
					$this->inventory_end();
				}		
					

				/* CHECK PINJAMAN TERSISA */
				$c=array();
				$c['pinjam_id']=$datakatalog[0]->id_pinjam;
				$c['status!=']=2;
				$pinjaman=$this->bookman_model->get_pinjaman_detail($c);

				if(count($pinjaman)==0){
					/* UPDATE STATUS PINJAMAN */
					$cond=array();
					$cond['id_pinjam']=$datakatalog[0]->id_pinjam;
					$va=array();
					$va['status_pinjam']=2;
					$this->bookman_model->update_pinjaman($va,$cond);	
				}



				if($id){
					$r=array();
					$r['status']=1;
					$r['msg']='Berhasil! Nomor '.$nomor.' DITAMBAHKAN';
					echo json_encode($r); 
					die();
				}else{
					$r=array();
					$r['status']=0;
					$r['msg']='GAGAL! ADA YANG SALAH';
					echo json_encode($r); 
					die();
				}
			}else{
				/* jika ditemukan lebih dari 1 pinjaman */
					$r=array();
					$r['status']=2;
					$r['msg']='Pilih Nomor Pinjaman';
					$r['pinjaman']=$datakatalog;
					echo json_encode($r); 
					die();
			}	
			 
		}else{
			$r=array();
			$r['status']=0;
			$r['msg']='GAGAL! ID Tidak Ditemukan';
			echo json_encode($r);
			die();
		}

		
		 
	}

	public function cekstatusmember(){
		 
		$id=$this->uri->segment(3);
		if($id==null)
            die();
            
        /* GET USER */        
		$pinjaman=$this->bookman_model->get_pinjaman_aktif(array('member_id'=>$id));
		$result=array();
		if(count($pinjaman)>0){
            /* INSERT KUNJUNGAN */

			$result['status']=1;
			$result['result']=$pinjaman[0];
			 
		}else{
			$result['status']=0;
			$result['result']=array();;
		}
		header('Content-Type: application/json');
		
		echo  json_encode($result);
	}

	public function konfigurasi(){
		$this->checklogin();				 

		$data=array();
		$this->admintemplate->load('admin_konfigurasi',$data);
	}
	

	public function  checklogin(){
		if(!$this->session->userdata('UID')){
			$this->session->set_flashdata('pesan', 'Anda Belum Login, silahkan login');
			 redirect('/dashboard/login', 'refresh');				
		}else{
			if($this->session->userdata('type')!='admin')
			 redirect('main','refresh');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		$this->session->set_flashdata('pesan', 'Anda telah Keluar');
			redirect('/dashboard/login', 'refresh');
	}
	public function searchkatalog(){

		$by=$this->uri->segment(3);
		$con=$this->input->get('term');
		if($by=='' || $con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		switch($by){
			case 'noregister': $like['no_register'] = $con;break;
			case 'nopanggil': $like['no_panggil'] = $con;break;
			case 'judul': $like['judul'] = $con;break;
			default : $like=array(); ;break;
		}

		$r=$this->bookman_model->get_katalog_like($like,10);

		$result=array();
		switch($by){
			case 'noregister': 
				foreach($r as $rr){
					$temp=array();
					$temp=array('value'=>$rr->id_katalog, 'label'=>$rr->no_register);
					$result[]=$temp;
				};break;
			case 'nopanggil': 
				foreach($r as $rr){
					$temp=array();
					$temp=array('value'=>$rr->id_katalog, 'label'=>$rr->no_panggil);
					$result[]=$temp;
				};break;
			case 'judul': 
				foreach($r as $rr){
					$temp=array();
					$temp=array('value'=>$rr->id_katalog, 'label'=>$rr->judul);
					$result[]=$temp;
				};break;
			default : $like=array(); ;break;
		}		
		
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}
	public function searchkatalogall(){

		 
		$con=$this->input->get('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		$like['no_register'] = $con;
		$like['no_panggil'] = $con;
		$like['judul'] = $con;

		$r=$this->bookman_model->get_katalog_like_or($like,10);

		$result=array();
		foreach($r as $rr){
			$temp=array();
			if($rr->no_register==null||$rr->no_register==''){
				$rr->no_register='N/A';
			}
			if($rr->no_panggil==null||$rr->no_panggil==''){
				$rr->no_panggil='N/A';
			}
			if($rr->pengarang==null||$rr->pengarang==''){
				$rr->pengarang='N/A';
			}
			$temp=array('value'=>$rr->id_katalog, 'label'=>$rr->no_register.'| '.$rr->no_panggil.'| '.$rr->judul.'| '.$rr->pengarang.'| '.$rr->tahun);
			$result[]=$temp;
		};
		
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}
	public function searchkataloginstock(){

		 
		$con=$this->input->get('term');
		if($con==''){
			echo json_encode(array());
			die();
		}
		$like=array();
		$like['no_register'] = $con;
		$like['no_panggil'] = $con;
		$like['judul'] = $con;

		$r=$this->bookman_model->get_katalog_instock_like_or($like,10);

		$result=array();
		foreach($r as $rr){
			$temp=array();
			if($rr->no_register==null||$rr->no_register==''){
				$rr->no_register='N/A';
			}
			if($rr->no_panggil==null||$rr->no_panggil==''){
				$rr->no_panggil='N/A';
			}
			if($rr->pengarang==null||$rr->pengarang==''){
				$rr->pengarang='N/A';
			}
			$temp=array('value'=>$rr->id_katalog, 'label'=>$rr->no_register.' | '.$rr->no_panggil.' | '.$rr->judul.' | '.$rr->pengarang.' | '.$rr->tahun.' | '.$rr->stok);
			$result[]=$temp;
		};
		
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function inventory_start(){
		$val['status']=1;
		$this->bookman_model->update_inventory_status($val);
	}
	public function inventory_end(){
		$val['status']=0;
		$this->bookman_model->update_inventory_status($val);
	}
	public function check_inventory_status(){
		$r=$this->bookman_model->get_inventory_status();
		
		if($r[0]->status=='0')
			return true;
		else
			return false;

	}


}
