<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Daftar Pinjaman</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Pinjaman</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Buku yang sedang Dipinjam </h3>
                     
              </div>
             
               <!-- /.card-header -->
              <div class="card-body">
                 
                <div id="tabel-master-katalog_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row">
                    <div class="col-sm-12 col-md-6">
                      <div class="dataTables_length" id="tabel-master-katalog_length"> <label>Show <select name="tabel-master-katalog_length" aria-controls="tabel-master-katalog" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <table id="tabel-master-katalog" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="tabel-master-katalog_info">
                        <thead>
                        <tr>
                        <th>NoPinjam</th>                        
                        <th>Tanggal_pinjam</th>
                        <th>Judul Buku</th>
                        <th>pengarang</th>
                        <th>tahun</th>                        
                        </tr>
                            </thead>
                          <tbody>
                          <?php 
                          foreach($pinjaman as $p){
                                echo '<tr><td>'.$p->id_pinjam.'</td>';
                                echo '<td>'.$p->tanggal_pinjam.'</td>';
                                echo '<td>'.$p->katalog->judul.'</td>';
                                echo '<td>'.$p->katalog->pengarang.'</td>';
                                echo '<td>'.$p->katalog->tahun.'</td>';
                                echo '</tr>';
                          }
                          ?>
                          </tbody>
                          <tfoot>                           
                          </tfoot>
                        </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 
    <script>
var table;

      

    $(document).ready(function(){

        
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

     
 
       table = $("#tabel-master-katalog").DataTable({
          order: [[ 0, "asc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
         

                
                    
            
    });
</script>
