<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        
          <h1>Online Booking</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Online Booking</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-9">
                    <form method="POST" action="http://localhost/bookman/dashboard/master_detail_add">

                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-4 col-form-label">Tanggal Pinjam</label>
                        <input type="text" id="datepicker">

                    </div>
                    <hr>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label ">Katalog: </label>                        
                        <div class="col-md-6">
                        <input type="hidden" name="id_katalog" class="id_katalog" value="0">
                        <input type="hidden" name="id_master" value="18">
                        <input type="text" name="judul" class="form-control mainsearch form-control-sm ui-autocomplete-input" placeholder="Ketik No register, No Panggil atau Judul Buku" required="" autocomplete="off">
                        <small id="emailHelp" class="form-text text-muted">*hanya untuk katalog yang sudah tersimpan di database.</small>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-outline-primary  btn-sm" title="Tambah Data Katalog"><i class="fas fa-database"></i></button> 
                        </div>
                    </div>
                    </form>
                         
                    </div>
                    <div class="col-3">
                    <div class="float-right">                        
                        <button class="btn btn-primary btn-sm btn-save" data-toggle="modal" data-target="#myModalsave" title="Proses Data Utama"> <i class="fas fa-cloud"> Booking</i></button>
                        
                        </div>
                    </div>
                </div>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                 
                <div id="tabel-master-katalog_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row">
                    <div class="col-sm-12 col-md-6">
                      <div class="dataTables_length" id="tabel-master-katalog_length"> <label>Show <select name="tabel-master-katalog_length" aria-controls="tabel-master-katalog" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <table id="tabel-master-katalog" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="tabel-master-katalog_info">
                        <thead>
                        <tr role="row">
                          <th class="sorting_asc" tabindex="0" aria-controls="tabel-master-katalog" rowspan="1" colspan="1" aria-sort="ascending" aria-label="No: activate to sort column descending">No</th>
                          <th class="sorting" tabindex="0" aria-controls="tabel-master-katalog" rowspan="1" colspan="1" aria-label="no_register: activate to sort column ascending">no_register</th>
                          <th class="sorting" tabindex="0" aria-controls="tabel-master-katalog" rowspan="1" colspan="1" aria-label="no_panggil: activate to sort column ascending">no_panggil</th>
                          <th class="sorting" tabindex="0" aria-controls="tabel-master-katalog" rowspan="1" colspan="1" aria-label="judul: activate to sort column ascending">judul</th>
                          <th class="sorting" tabindex="0" aria-controls="tabel-master-katalog" rowspan="1" colspan="1" aria-label="pengarang: activate to sort column ascending">pengarang</th>
                          <th class="sorting" tabindex="0" aria-controls="tabel-master-katalog" rowspan="1" colspan="1" aria-label="penerbit: activate to sort column ascending">penerbit</th>
                          <th class="sorting" tabindex="0" aria-controls="tabel-master-katalog" rowspan="1" colspan="1" aria-label="kategori: activate to sort column ascending">kategori</th>
                          
                          <th class="text-center" rowspan="1" colspan="1"><i class="fas fa-tools "></i></th></tr>
                            </thead>
                          <tbody>
                          
                          <tr class="odd"><td valign="top" colspan="9" class="dataTables_empty">No data available in table</td></tr></tbody>
                          <tfoot>
                          <tr>
                             
                          </tr>
                          </tfoot>
                        </table><div id="tabel-master-katalog_processing" class="dataTables_processing card" style="display: none;">loading...</div></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="tabel-master-katalog_info" role="status" aria-live="polite">Showing 0 to 0 of 0 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="tabel-master-katalog_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="tabel-master-katalog_previous"><a href="#" aria-controls="tabel-master-katalog" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item next disabled" id="tabel-master-katalog_next"><a href="#" aria-controls="tabel-master-katalog" data-dt-idx="1" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>