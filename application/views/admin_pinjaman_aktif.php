<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pinjaman Aktif</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Pinjaman Aktif</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pinjaman Aktif</h3>
                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>NoPinjam</th> 
                        <th>judul</th>
                        <th>Pengarang</th>
                        <th>NoRegister</th>
                        <th>Member</th>
                        <th>telp</th>                        
                        <th>Admin</th>
                        <th>TangggalPinjam</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 
 
  
 
<script>
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center'
            }, 
             
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_pinjaman_aktif'?>", "type": "POST"},
                    columns: [
                                                {"data": "id_pinjam"}  ,
                                                {"data": "judul"},
                                                {"data": "pengarang"},
                                                {"data": "no_register"},
                                                {"data": "member_id"},
                                                {"data": null,"defaultContent": ''},                                          
                                                {"data": "admin_id"},                                              
                                                {"data": "tanggal_pinjam"},
                                                {"data": "status"}                                        
                                                
                  ],
                order: [[1, 'asc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 0, "asc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
      table.on( 'draw.dt', function () {
        var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
        table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
            // end setup datatables


           


            


            
    });
</script>
