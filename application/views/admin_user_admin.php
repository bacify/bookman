<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Daftar Pengguna (admin)</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Pengguna (admin)</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pengguna (admin)</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>No</th> 
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Akses</th>
                        <th>status</th>
                        <th>Tindakan</th>
                         
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 

   <!-- Modal Add Product-->
   <form id="form-tambah-user" action="<?php echo site_url('dashboard/admin_user_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Tambah Pengguna Baru (admin)</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>
                             
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">Username</label>
                           <small class="status"> 
                                <div class="spinner-border spinner-border-sm  loadingicon" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikon-gagal" style="display:none"> username sudah terpakai</i>
                                <i class="fas fa-check-circle ikon-ok"style="display:none"></i>
                                 </small>
                            <input type="text" name="username" class="form-control " placeholder="Username" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>
                        <div class="form-group">
                            <label for="password" class="mb-0 pb-0">Password</label>
                           <input type="text" name="password" class="form-control" placeholder="password" required>
                       </div>

                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Akses</label>
                           <select name="akses" class="form-control" data-live-search="true" title="Akses" required>
                                                                                               
                                                            <option value="1">SuperAdmin</option>
                                                        
                                                 </select>
                       </div>
                        <div class="form-group row">
                                <label for="kategori" class="mb-0 py-auto col-lg-1 col-sm-3">Status</label>
                                <div class="col">
                                    <input type="checkbox" name="status" value="1"  data-toggle="toggle" data-on="Aktif" data-off="Tidak Aktif" data-size="normal" data-width="200" data-onstyle="success" data-offstyle="danger">
                                </div>
                       </div>                                  
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>



     <!-- Modal Add Product-->
   <form id="form-update-user" action="<?php echo site_url('dashboard/admin_user_update');?>" method="post">
         <div class="modal fade " id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Edit Data User(admin)</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>
                            <input type="hidden" name="id" value="0">
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">Username</label>
                           <small class="status"> 
                                <div class="spinner-border spinner-border-sm  loadingicon" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikon-gagal" style="display:none"> username sudah terpakai</i>
                                <i class="fas fa-check-circle ikon-ok"style="display:none"></i>
                                 </small>
                            <input type="text" name="username" class="form-control " placeholder="Username" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>
                        <div class="form-group">
                            <label for="password" class="mb-0 pb-0">Password</label>
                           <input type="text" name="password" class="form-control" placeholder="Kosongi jika tidak diubah" >
                       </div>

                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Akses</label>
                           <select name="akses" class="form-control" data-live-search="true" title="Akses" required>
                                                                                               
                                                            <option value="1">SuperAdmin</option>
                                                        
                                                 </select>
                       </div>
                        <div class="form-group row">
                                <label for="kategori" class="mb-0 py-auto col-lg-1 col-sm-3">Status</label>
                                <div class="col">
                                    <input type="checkbox" name="status" value="1"  data-toggle="toggle" data-on="Aktif" data-off="Tidak Aktif" data-size="normal" data-width="200" data-onstyle="success" data-offstyle="danger">
                                </div>
                       </div>                                  
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo site_url('dashboard/admin_user_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
<script>

        $('input[name=username]').change(function(){
           $('.status').removeClass('text-success').removeClass('text-danger');
            $('.loadingicon').show();
            $('.ikon-gagal').hide();
            $('.ikon-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url().'dashboard/checkadminusername';?>",
                data: {username: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.status').addClass('text-success');
                            $('.loadingicon').hide(); 
                            $('.ikon-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.status').addClass('text-danger');
                            $('.loadingicon').hide(); 
                            $('.ikon-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });


        // get Edit Records
        $('#tabel-master-katalog').on('click','.edit_record',function(){
            var id=$(this).data('id');
            var nama=$(this).data('nama');
            var username=$(this).data('username');
            var email=$(this).data('email');
            var akses=$(this).data('akses');
            var status=$(this).data('status');
            
                
                $('#myModalUpdate').modal('show');
                $('#form-update-user [name="id"]').val(id);
                $('#form-update-user [name="nama"]').val(nama);
                $('#form-update-user [name="username"]').val(username);
                $('#form-update-user [name="email"]').val(email);
                $('#form-update-user [name="akses"]').selectpicker('val', akses);
                
                if(status==1)
                    $('#form-update-user [name="status"]').bootstrapToggle('on')
                else 
                    $('#form-update-user [name="status"]').bootstrapToggle('off')
                
                
            
        });

        $('#tabel-master-katalog').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
         });
 
    
        

  

    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center'
            } ,
            {
                targets: 4,
                className: 'dt-body-nowrap text-center'
            } ,
            {
                targets: 5,
                className: 'dt-body-nowrap text-center'
            } ,
            { targets : 5,
             render : function (data, type, row) {
                
                
                switch(data) {
                case '0' : return '<a href="#" class=" btn-transparent btn-sm text-danger"> <i class="fas fa-ban" title="Tidak Aktif"></i></a>'; break;
                case '1' : return '<a href="#" class=" btn-transparent btn-sm text-success"><i class="far fa-check-circle" title="Aktif"></i></a>'; break;
                default  : return 'N/A';
                }
             }
            },
            { targets : 4,
               render : function (data, type, row) {
                
                switch(data) {
                case '1' : return 'SuperAdmin'; break;
                case '2' : return 'Admin'; break;
                default  : return 'N/A';
                }
               }
            },
            { targets : 6,
              orderable: false
            }
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_user_admin'?>", "type": "POST"},
                    columns: [
                                                {"data": "id_admin"}  ,
                                                {"data": "nama"},
                                                {"data": "username"},
                                                {"data": "email"},                                                
                                                {"data": "akses"},
                                                {"data": "status"},
                                                {"data": "view"},
                                                
                                                                                       
                                                
                  ],
                 
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;

              $('td:eq(0)', row).html();
              if(info.iTotal<=1){
                $('td:eq(6) .delete_record', row).remove();
              }
          },
          
          autoWidth: false,
          responsive: true,
 
      });
      table.on( 'draw.dt', function () {
        var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
        table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
            // end setup datatables

 

            
    });
</script>
