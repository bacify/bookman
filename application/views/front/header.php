<section class="bg-primary">
  <div class="container">

<!--  START NAVBAR   -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark   navbar-offcanvas header">
      
      
<!--     If you want to show the button online on mobile use this toggle   
      
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

-->
      
<!--      This toggle button is always visible  -->
     <button class="navbar-toggler d-block" type="button" id="navToggle">
      <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand mr-auto ml-2" href="#">LIMASYS - Library Management System </a>

      <div class="navbar-collapse offcanvas-collapse menu">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?php if(($this->uri->segment(1,0)==NULL || $this->uri->segment(1)=='main') && ($this->uri->segment(2,0) == NULL ||$this->uri->segment(2,0)=='home' ) ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url();?>">Pencarian Buku </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1)=='main' && $this->uri->segment(2,0)=='cek'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url();?>main/cek">Cek Pinjaman</a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1)=='main' && $this->uri->segment(2,0)=='koleksi_baru'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url();?>main/koleksi_baru">Koleksi Baru</a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1)=='main' && $this->uri->segment(2,0)=='login'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url();?>main/akun">Akun</a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1)=='main' && $this->uri->segment(2,0)=='about'  ) echo 'active';?>">
            <a class="nav-link" href="<?php echo base_url();?>main/about">Tentang Bookman</a>
          </li>
           
        </ul>
 
        
      </div>
    </nav>
<!--  END NAVBAR   -->

  </div>
  <!--/.container    -->

</section>