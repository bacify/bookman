<!doctype html>
<html lang="en" class="h-100">
  <head>
  <title>LIMASYS -  Library Management System</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
   <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">

    <!-- Bootstrap CSS -->    
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/css/cfront.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/fontawesome/css/all.min.css">
    
    
   

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->    
    <script src="<?php echo base_url();?>/assets/js/jquery-3.5.1.min.js"></script>
	<!-- <script src="<?php echo base_url();?>/assets/js/popper.min.js"></script> -->
	<script src="<?php echo base_url();?>/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/cfront.js"></script>

    
  <!-- jQuery UI-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jqueryui/jquery-ui.min.css">
<script src="<?php echo base_url(); ?>assets/plugins/jqueryui/jquery-ui.min.js"></script>
    
  </head>
  <body class="h-100 main-content">

    <dev> </
    <!-- HEADER -->
    <?php echo $header; ?>  
     <!-- Dark Overlay element -->
     <div class="overlay"></div>
   
    <!-- <aside class="sidebar">
      <nav class="nav">
        <ul>
          <li class="active"><a href="#">Welcome</a></li>
          <li><a href="#">Who We Are</a></li>
          <li><a href="#">What We Do</a></li>
          <li><a href="#">Get In Touch</a></li>
        </ul>
      </nav>
    </aside> -->
  
    <div class="container h-100">
      <!-- MAIN BODY -->
      <?php echo $content; ?>
      


      <div class="footer text-light"><center>
        <img src="http://localhost/bookman/assets/img/limasys_logo.png" width="90px">
        <br>
        <span>LIMASYS</span> <i class="far fa-copyright"> 2020 SERANGGALANGIT</i></center></div>
    </div>


    <!-- FOOTER -->
    <?php echo $footer; ?>
   
  </body>
  
</html>







    



    