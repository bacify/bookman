<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url();?>dashboard/pinjaman" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1>Pengembalian Buku </h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Pengembalian Buku </li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
      
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">

          <!-- FORM PENGEMBALIAN -->
            <div class="card">
            <div class="card-header">
                <h3 class="card-title">Form Pengembalian Buku</h3>
            </div>
                <div class="card-body">
                    <div class="col-8"   >
                    
                        <form method="POST" action="#">
                        <div class="form-group row my-0 py-0">
                            <label for="tanggal" class="col-md-2 col-form-label ">SearchBy</label>   
                            <div class="col-md-2">
                                
                                <select name="searchby" class="form-control form-control-sm searchby">
                                    <option value="isbn">ISBN</option>
                                    <option value="id">ID</option>
                                    <option value="register">NoRegister</option>                                
                                    <option value="panggil">NoPanggil</option>                                
                                </select>               
                                
                            </div>                      
                            <div class="col-md-7">
                            
                            <input type="hidden" name="id_katalog_barcode" class="id_katalog_barcode"  value="0">                         
                            <input type="text" name="kode_barcode" class="form-control barcodesearch form-control-sm" onmouseover="this.focus();" placeholder="Ketik NoBarCode" required >
                            
                            </div>                                              
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog"  ><i class="fas fa-database" ></i></button> 
                            </div>
                        </div>
                        <div class="form-group row my-0 py-0">
                            <div class="col-md-12">
                                <div class="alert alert-success alert-ok" role="alert" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <span class="text-ok"><strong>Sukses!</strong> Data Ditambahkan!</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-gagal" role="alert" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <span class="text-gagal"><strong>Gagal!</strong> Data gagal Ditambahkan!</span>
                                </div>
                            </div>
                        </div>
                        </form>
                        
                    </div>
                </div>
                 
            </div>
            <!-- /.FORM PENGEMBALIAN -->

            <!-- FORM Cek Member -->
            <div class="card">
           <!--  <div class="card-header">
                <h3 class="card-title">Form Pengembalian Buku</h3>
            </div> -->
                <div class="card-body">
                    <div class="col-8"   >
                    
                        <form method="POST" action="#">
                        <div class="form-group row my-0 py-0">
                            <label for="tanggal" class="col-md-2 col-form-label ">Cek Member</label>                                                
                            <div class="col-md-6">  
                            <input type="text" name="member" id="noidmember" class="form-control nomember form-control-sm" onmouseover="this.focus();" maxlength="6" placeholder="123456"  required >
                            
                            </div>                                              
                            <!-- <div class="col-md-1">
                                <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog"  ><i class="fas fa-check" ></i></button> 
                            </div> -->
                        </div>
                         
                        </form>
                        
                    </div>
                </div>
                 
            </div>
            <!-- /.FORM Cek Member PENGEMBALIAN -->


            <div class="card">
              <div class="card-header">                
              
              <h3 class="card-title">Riwayat Pengembalian Buku</h3>
                   
              </div>
             
             
              <!-- /.card-header -->
              <div class="card-body">
              
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                <tr>
                        <th>Tanggal</th>                        
                        <th>Judul</th>
                        <th>pengarang</th>                        
                        <th>NoPinjam</th>
                        <th>Member</th>                        
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 
 <!-- MODAL HASIL -->
 <div class="modal fade " id="modal-result" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog  ">
               <div class="modal-content ">
                   <div class="modal-header">
                   <h5 class="modal-title text-center" id="exampleModalLongTitle">Hasil</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body text-center">                        
                        <h2 class="text-result"><strong class="text-uppercase text-primary">Data DITEMUKAN</strong></h2>
                        <p class="text-muted text-desc">deskripsi</p> 
                   </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">OK</button>
                     
                  </div>
                    
                </div>    
          </div>
    </div>
  
 
<script>

// SET IDENTITAS NUMBER ONLY
setInputFilter(document.getElementById("noidmember"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }
        $( "#noidmember" ).keypress(function(e) {
            var value=$(this).val();
            console.log(value);
            if(e.which == 13){
                $('.text-result').html('');
                  $('.text-desc').html('');
                        $.ajax({
                                method: "GET",
                                dataType: "json",
                                url: "<?php echo base_url();?>dashboard/cekstatusmember/"+value,                        
                                })
                                .done(function( msg ) {

                                if(msg.status=='1'){
                                    
                                    $('#modal-result').modal('toggle');
                                    $('.text-result').html('<strong class="text-uppercase text-primary">OK!</strong>');
                                    $('.text-desc').html('<strong class="text-danger">*tidak ada tanggungan</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('toggle');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 3000);
                                } else{
                                    $('#modal-result').modal('toggle');
                                  $('.text-result').html('<strong class="text-uppercase text-danger">Masih Ada Tanggungan!</strong>');
                                  $('.text-desc').html('<strong class="text-danger">*member memiliki tanggungan buku</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('toggle');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 3000);
                                }


                                    return false;
                                });
                return false;    //<---- Add this line
                }
            });


var table;
 
    // DETECT BARCODE
    $('.barcodesearch').keypress(function (e) {
        if (e.which == 13) {
            $(this).prop( "disabled", true );
            var vv=$(this).val();
            console.log(vv);
            console.log('ditemukan');
            
            var search=$('.searchby :selected').val();
            $.ajax({
                            method: "POST",
                            url: "<?php echo base_url();?>dashboard/pengembalian_ajax",
                            data: { nomor: vv ,searchby:search}
                            })
                            .done(function( msg ) {
                            
                                var res=JSON.parse(msg);                             
                                if(res.status=='1'){
                                    
                                    table.ajax.reload();
                                    console.log('ok');
                                    $('.text-ok').html(res.msg);
                                    $(".alert-ok").fadeTo(2000, 1000).slideUp(1000, function() {
                                            $(".alert-ok").slideUp(1000);
                                            $('.barcodesearch').val('');
                                            $('.barcodesearch').prop( "disabled", false );
                                            $('.barcodesearch').focus();
                                    });
                                }                                
                                else if(res.status=='2'){
                                    console.log('Pinjaman Lebih dari 1');
                                    $('.text-ok').html(res.msg);                                
                                    $(".alert-ok").fadeTo(2000, 1000).slideUp(1000, function() {
                                            $(".alert-ok").slideUp(1000);
                                            $('.barcodesearch').val('');
                                            $('.barcodesearch').prop( "disabled", false );
                                            $('.barcodesearch').focus();
                                    });
                                }else
                                {
                                    console.log('gagal');
                                    $('.text-gagal').html(res.msg);
                                    $(".alert-gagal").fadeTo(2000, 1000).slideUp(1000, function() {
                                            $(".alert-gagal").slideUp(1000);
                                            $('.barcodesearch').val('');
                                            $('.barcodesearch').prop( "disabled", false );
                                            $('.barcodesearch').focus();
                                    });

                                }

                                
                                return false;
                            });

            
            return false;    //<---- Add this line
        }
    });

    $(document).ready(function(){

        
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

     
 
       table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center'
            },
            {
                targets: -2,
                className: 'dt-body-nowrap text-center'
            },
                      
            { targets : -1,
              orderable: false
            }, 
                
             
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_riwayat_pengembalian/';?>", "type": "POST"},
                    columns: [
                                                 {"data": "tanggal_kembali","defaultContent": ''}  ,                                                 
                                                 {"data": "judul"}  ,
                                                 {"data": "pengarang"}  ,
                                                 {"data": "pinjam_id"}  ,
                                                 {"data": "nama"}  ,
                                                
                                                
                                                
                  ],
                order: [[1, 'desc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 0, "desc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
        /*   table.on( 'draw.dt', function () {
            var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
            table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } ); */
            // end setup datatables


 
            // End Edit Records
            // get Save Records
            $('#tombol-save').click(function(){                 
                $('#myModalsave').modal('show');
                
            });
             
            $('#tabel-master-katalog').on('click','.return_record',function(){
                var id=$(this).data('id');
               
                $('#ModalReturn').modal('show');
                $('#returnform [name="id_detail"]').val(id);
            });
            // End delete Records
                
                    
            
    });
</script>
