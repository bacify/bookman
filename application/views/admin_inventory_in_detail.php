<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url();?>dashboard/inventory_masuk" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1>Detail Transaksi (<strong><?php echo sprintf('%06d', $this->uri->segment(3));?></strong>) </h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Detail Transaksi  (<?php echo sprintf('%06d', $this->uri->segment(3));?>)</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-9">
                    <form method="POST" action="<?php echo base_url();?>dashboard/master_detail_add">
                    <div class="form-group row my-0 py-0">
                        <label for="notransksaksi" class="col-md-2 col-form-label">NoTransaksi</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="notransksaksi" value="<?php echo sprintf('%06d', $master->id_master);?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="asal" class="col-md-2 col-form-label">Asal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="asal" value="<?php echo $master->asal;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="keterangan" class="col-md-2 col-form-label">Keterangan</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="keterangan" value="<?php echo $master->keterangan;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label">Tanggal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="tanggal" value="<?php echo $master->tanggal_transaksi;?>">
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label ">Dt katalog</label>                        
                        <div class="col-md-6">
                        <input type="hidden" name="id_katalog" class="id_katalog"  value="0">
                        <input type="hidden" name="id_master" value="<?php echo $master->id_master;?>">
                        <input type="text" name="judul" class="form-control mainsearch form-control-sm" placeholder="Ketik No register, No Panggil atau Judul Buku" required <?php if($master->status_transaksi!='0') echo 'disabled';?>>
                        <small id="emailHelp" class="form-text text-muted">*hanya untuk katalog yang sudah tersimpan di database.</small>
                        </div>
                        <div class="col-md-2">
                        <input type="number" name="jumlah" class="form-control form-control-sm"  placeholder="jumlah"  required <?php if($master->status_transaksi!='0') echo 'disabled';?>>                        
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog" <?php if($master->status_transaksi!='0') echo 'disabled';?>><i class="fas fa-database" ></i></button> 
                        </div>
                    </div>
                    </form>
                         
                    </div>
                    <div class="col-3">
                    <div class="float-right">                        
                        <button class="btn btn-primary btn-sm btn-save" data-toggle="modal" data-target="#myModalsave" title="Proses Data Utama" <?php if($master->status_transaksi!='0') echo 'disabled';?>> <i class="fas fa-cloud" > Simpan</i></button>
                        <button class="btn btn-success btn-sm " data-toggle="modal" data-target="#myModalAdd"  title="Tambah Buku Baru" <?php if($master->status_transaksi!='0') echo 'disabled';?>><i class="fas fa-plus" > Tambah</i></button> 
                        </div>
                    </div>
                </div>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                 
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                <tr>
                        <th>No</th>
                        <th>no_register</th>
                        <th>no_panggil</th>
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>penerbit</th>                        
                        <th>kategori</th>
                        <th>Jumlah  </th>
                        <th class="text-center"><i class="fas fa-tools "></i></th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 <!-- Modal Add Product-->
 <form id="form-tambah-buku" action="<?php echo site_url('dashboard/master_detail_new_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Tambah Buku Baru</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group mb-0">

                        <small><label for="no_register" class="mb-0 pb-0">Nomor Register</label></small>
                           <input type="hidden" name="id_master" value="<?php echo $master->id_master;?>">  
                           <input type="text" name="no_register" class="form-control" placeholder="Nomor Register">
                       </div>
                       <div class="form-group mb-0">
                       <small><label for="judul" class="mb-0 pb-0">Judul</label></small>
                           <input type="text" name="judul" class="form-control inputjudulbuku" placeholder="Judul Buku" required>
                       </div>
                       <div class="form-group mb-0">
                       <small><label for="no_panggil" class="mb-0 pb-0">No Panggil</label></small>
                           <input type="text" name="no_panggil" class="form-control" placeholder="Nomor Panggil (ex: '793.7 Woe k')" >
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="pengarang" class="mb-0 pb-0">pengarang</label></small>
                           <input type="text" name="pengarang" class="form-control" placeholder="Pengarang" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="penerbit" class="mb-0 pb-0">Penerbit</label></small>
                           <input type="text" name="penerbit" class="form-control" placeholder="penerbit" required>
                       </div>
                       <div class="form-group mb-0">
                       <small><label for="kategori" class="mb-0 pb-0">Kategori</label></small>
                           <select name="kategori" class="form-control" data-live-search="true" title="Jenis Katalog" required>
                                         
                                                      <?php foreach ($kategori as $row) :?>
                                                            <option value="<?php echo $row->id_kategori;?>"><?php echo $row->nama_kategori;?></option>
                                                        <?php endforeach;?>
                                                 </select>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="bahasa" class="mb-0 pb-0">Bahasa</label></small>
                           <input type="text" name="bahasa" class="form-control" placeholder="bahasa" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="tahun" class="mb-0 pb-0">Tahun</label></small>
                           <input type="text" name="tahun" class="form-control" placeholder="tahun" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="edisi" class="mb-0 pb-0">Edisi</label></small>
                           <input type="text" name="edisi" class="form-control" placeholder="Edisi" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="subyek" class="mb-0 pb-0">Subyek</label></small>
                           <input type="text" name="subyek" class="form-control" placeholder="Subyek" required>
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="klasifikasi" class="mb-0 pb-0">Klasifikasi</label></small>
                           <input type="text" name="klasifikasi" class="form-control" placeholder="klasifikasi (ex: 793.7)" >
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="deskripsi" class="mb-0 pb-0">Deskripsi</label></small>
                           <input type="text" name="deskripsi" class="form-control" placeholder="Deskripsi Fisik (ex: 'xii, 200 hal. : ill. ; 18 cm)'" >
                       </div>
                        <div class="form-group mb-0">
                        <small><label for="isbn" class="mb-0 pb-0">isbn</label></small>
                           <input type="text" name="isbn" class="form-control" placeholder="isbn" >
                       </div>
                       <div class="form-group mb-0">
                        <small><label for="isbn" class="mb-0 pb-0">Jumlah</label></small>
                           <input type="text" name="jumlah" class="form-control" placeholder="0" required>
                       </div>
                                         
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
 <!-- Modal save Product-->
 <form id="saveform" action="<?php echo site_url('dashboard/master_detail_save');?>" method="post" >
         <div class="modal fade" id="myModalsave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Simpan dan Proses Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <input type="hidden" name="id_master" value="<?php echo $master->id_master;?>" >
                        <p><strong class="text-warning bg-warning text-uppercase">Transaksi tidak akan dapat di ubah jika sudah di simpan</strong>, Apakah anda yakin akan menyimpan data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit"   value="submit" class="btn btn-primary buttonsave">Simpan</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 <!-- Modal delete Product-->
 <form id="deleteform" action="<?php echo site_url('dashboard/master_detail_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                                                <input type="hidden" name="id_master" value="<?php echo $master->id_master;?>" >
                                                <input type="hidden" name="id_detail" class="form-control" >
                                                 <p> Apakah anda yakin akan menghapus data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
 
<script>
   function updateJumlah(x) {
                
                var newval=$(x).val();
                var iddetail=$(x).closest('td').find('.inputiddetail').val();
                if(!isNaN(newval)){
                    // proces data
                    ;
                }else{
                    $(x).val(0);
                    alert('Masukkan jumlah yang benar');
                    return false;
                }
                
                        
    }
    // $(".buttonsave").click(function(){
        
    //     $(this).prop("disabled",true);
    // });

   

    $(document).ready(function(){


        
                
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

     
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap'
            },
            <?php if($master->status_transaksi=='0'){?>
            { targets : 8,
                render : function (data, type, row) {
             
                return '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'+row.id_detail+'" title="Hapus Transaksi"><i class="fas fa-trash"></i></a>';
                }
            },
            <?php } ?>
            {   targets : 7,
                render : function (data, type, row) {
                        return '<form><input type="text" name="jumlah" class="form-control form-control-sm inputjumlah" value="'+data+'" onchange="updateJumlah(this)" <?php if($master->status_transaksi!='0') echo 'disabled';?>><input type="hidden" name="id" class="inputiddetail" value="'+row.id_detail+'"></form> ';
                    
                }
            }
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_transaksi_master_detail/'.$master->id_master;?>", "type": "POST"},
                    columns: [
                                                 {"data": "id_detail","defaultContent": ''}  ,
                                                 {"data": "no_register"}  ,
                                                 {"data": "no_panggil"}  ,
                                                 {"data": "judul"}  ,
                                                 {"data": "pengarang"}  ,
                                                 {"data": "penerbit"}  ,
                                                 {"data": "nama_kategori"}  ,
                                                //  {"data": "bahasa"}  ,
                                                //  {"data": "tahun"}  ,
                                                //  {"data": "edisi"}  ,
                                                //  {"data": "Subyek"}  ,
                                                //  {"data": "klasifikasi"}  ,
                                                //  {"data": "deskripsi"}  ,
                                                //  {"data": "isbn"}  ,
                                                 {"data": "masuk"}  ,
                                                
                                                
                                                
                  ],
                order: [[1, 'desc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 0, "asc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
        table.on( 'draw.dt', function () {
            var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
            table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
            // end setup datatables


            $('#tabel-master-katalog').on('click','.view_record',function(){

                    var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                    success:function(data) { 
                        console.log(data);
                        $('#ModalView').modal('show');
                        $('#viewform [name="id"]').val(data.id_katalog);
                        $('#viewform [name="no_register"]').val(data.no_register);
                        $('#viewform [name="judul"]').val(data.judul);
                        $('#viewform [name="no_panggil"]').val(data.no_panggil);
                        $('#viewform [name="pengarang"]').val(data.pengarang);
                        $('#viewform [name="penerbit"]').val(data.penerbit);
                        $('#viewform [name="kategori"]').selectpicker('val', data.kategori_id);
                        $('#viewform [name="bahasa"]').val(data.bahasa);
                        $('#viewform [name="tahun"]').val(data.tahun);
                        $('#viewform [name="edisi"]').val(data.edisi);
                        $('#viewform [name="subyek"]').val(data.subyek);
                        $('#viewform [name="klasifikasi"]').val(data.klasifikasi);
                        $('#viewform [name="deskripsi"]').val(data.deskripsi);
                        $('#viewform [name="isbn"]').val(data.isbn);
                        
                    }
                 });

            });



            // get Edit Records
            $('#tabel-master-katalog').on('click','.edit_record',function(){

                    var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                    success:function(data) { 
                        console.log(data);
                        $('#ModalUpdate').modal('show');
                        $('#updateform [name="id"]').val(data.id_katalog);
                        $('#updateform [name="no_register"]').val(data.no_register);
                        $('#updateform [name="judul"]').val(data.judul);
                        $('#updateform [name="no_panggil"]').val(data.no_panggil);
                        $('#updateform [name="pengarang"]').val(data.pengarang);
                        $('#updateform [name="penerbit"]').val(data.penerbit);
                        $('#updateform [name="kategori"]').selectpicker('val', data.kategori_id);
                        $('#updateform [name="bahasa"]').val(data.bahasa);
                        $('#updateform [name="tahun"]').val(data.tahun);
                        $('#updateform [name="edisi"]').val(data.edisi);
                        $('#updateform [name="subyek"]').val(data.subyek);
                        $('#updateform [name="klasifikasi"]').val(data.klasifikasi);
                        $('#updateform [name="deskripsi"]').val(data.deskripsi);
                        $('#updateform [name="isbn"]').val(data.isbn);
                        
                    }
                    });         
            
                            
             });
            // End Edit Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.save_record',function(){
                var id=$(this).data('id');
                $('#ModalSave').modal('show');
                
            });
            // End delete Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
                var id=$(this).data('id');
               
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id_detail"]').val(id);
            });
            // End delete Records
            
                  

               
                    $('.mainsearch').autocomplete({                    
                    source: "<?php echo base_url();?>dashboard/searchkatalogall",
                    minLength: 2,
                    select: showResult,
                    focus : showResult, 
                    change: false
                    } );  
                function showResult( event, ui ) {
                            $('.id_katalog').val(ui.item.value);
                            $('.mainsearch').val(ui.item.label);
                            
                            return false;
                }  


                
                    
            
    });
</script>
