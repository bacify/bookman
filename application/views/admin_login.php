 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Administrator Area</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Administrator Area</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

     <!-- Main content -->
     <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6 offset-md-3">

                <div class="notification bg-warning"><center><p class="text-danger"><?php echo $this->session->flashdata('pesan');?></p></center></div>
                        <!-- general form elements -->
                        <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Silahkan Login</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="#" method="POST">
                            <div class="card-body">
                            <div class="form-group">
                                <label for="userid">Username</label>
                                <input type="text" class="form-control" name="userid" id="userid" placeholder="Masukan Username">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan Password">
                            </div>
                            
                            
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                            <center><button type="submit" class="btn btn-primary" name="submit" value="submit">Submit</button></center>
                            </div>
                        </form>
                        </div>
                        <!-- /.card -->
                    </div>
            </div>
        </div>
    </section>