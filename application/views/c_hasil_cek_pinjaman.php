<div class="row   justify-content-center align-items-center pt-3">
      <div class="col-12 col-md-12 col-lg-12 content-cari">
        <small><a href="<?php echo base_url();?>main/cek" class="text-decoration-none alert-link text-light"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
        <h2 class="text-light mb-3">Hasil Pencarian</h2>
        
     
    <div class="card bg-result">
    <div class="card-header"><h5 class="text-dark card-title">Tanggungan Buku Nomor Anggota : <span class="font-weight-bold strong"><strong><?php echo $katakunci;?></strong></span></h5></div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Judul Buku</th>
                    <th scope="col">Pengarang</th>
                    <th scope="col">Penerbit</th>                    
                    <th scope="col">Subyek</th>
                    <th scope="col" class="text-nowrap">NoPanggil</th>
                    <th scope="col">tahun</th>                    
                    <th scope="col">NoPinjam</th>                    
                    <th scope="col">TglPinjam</th>                    
                    </tr>
                </thead>
                <tbody>
                <?php
                 if(count($hasil)==0){
                    echo '<tr><td colspan="9" class="text-nowrap text-center">Tidak ada hasil yang ditemukan</td></tr>';
                }
                $x=1; 
                foreach($hasil as $hasilcari){
                    echo '<tr><td>'.$x++.'</td>'.
                              '<td>'.$hasilcari->judul.'</td>'.
                              '<td>'.$hasilcari->pengarang.'</td>'.
                              '<td>'.$hasilcari->penerbit.'</td>'.
                              '<td>'.$hasilcari->subyek.'</td>'.
                              '<td class="text-nowrap">'.$hasilcari->no_panggil.'</td>'.
                              '<td>'.$hasilcari->tahun.'</td>'.
                              '<td>'.$hasilcari->id_pinjam.'</td>'.
                              '<td class="text-nowrap">'.date('d, m Y', strtotime($hasilcari->tanggal_pinjam)).'</td>';
                              

                              echo '</tr>';
                }
                ?>
                   
                </tbody>
            </table>
        </div>
    </div>
    </div>
     </div>
    </div>
 
 
 
 