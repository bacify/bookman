<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Daftar Pengguna (Member)</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Pengguna (Member)</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pengguna (admin)</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>NoMember</th> 
                        <th>Nama</th>
                        <th>Identitas</th>
                        <th>Alamat</th>
                        <th>Kec</th>
                        <th>Kota</th>
                        <th>Provinsi</th>
                        <th>kodepos</th>
                        <th>hp</th>
                        <th>email</th>
                        <th>jenis</th>
                        <th>status</th>
                        <th>TglDaftar</th>
                        <th>tindakan</th>
                         
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 

   <!-- Modal Add Product-->
   <form id="form-tambah-user" action="<?php echo site_url('dashboard/member_user_add');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Buat Member baru</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>                             
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="identitas" class="mb-0 pb-0">nomor identitas</label>
                           <small class="statusid"> 
                                <div class="spinner-border spinner-border-sm  loadingiconid" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonid-gagal" style="display:none"> Identitas sudah terpakai</i>
                                <i class="fas fa-check-circle ikonid-ok"style="display:none"></i>
                                 </small>
                            <input type="text" id="identitas" name="identitas" class="form-control " placeholder="" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">alamat</label>
                           <input type="text" name="alamat" class="form-control" placeholder="Jl Diponegoro 01" >
                       </div>
                       <div class="form-group">
                            <label for="provinsi" class="mb-0 pb-0">Provinsi</label>
                            <select name="provinsi" class="form-control provinsi" data-live-search="true" title="Silahkan Pilih" required>
                                    <?php 
                                    foreach($provinsi as $de){
                                        echo '<option value="'.$de->id.'">'.$de->name.'</option>';
                                    }                                                                                            
                                    ?>                                    
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kota" class="mb-0 pb-0">Kota</label>
                            <select name="kota" class="form-control kota" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi provinsi</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kec" class="mb-0 pb-0">Kec</label>
                            <select name="kec" class="form-control kec" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="desa" class="mb-0 pb-0">Desa</label>
                            <select name="desa" class="form-control desa" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kodepos" class="mb-0 pb-0">Kodepos</label>
                           <input type="text" name="kodepos" class="form-control" placeholder="65141" >
                       </div>
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">telp</label>
                           <input type="tel" name="telp" class="form-control" placeholder="0812333111" >
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                            <small class="statusemail"> 
                                <div class="spinner-border spinner-border-sm  loadingiconemail" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonemail-gagal" style="display:none"> Email sudah terpakai</i>
                                <i class="fas fa-check-circle ikonemail-ok"style="display:none"></i>
                                 </small>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>
                        <div class="form-group">
                            <label for="password" class="mb-0 pb-0">Password</label>
                           <input type="text" name="password" class="form-control" placeholder="password" required>
                       </div>

                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">jenis</label>
                           <select name="tipe" class="form-control" data-live-search="true" title="silahkan pilih" required>                                                                                               
                                                <option value="1">Member</option>                                                        
                                                <option value="2">Siswa</option>                                                        
                                                <option value="3">Guru</option>                                                        
                                                 </select>
                       </div>
                        <div class="form-group row">
                                <label for="status" class="mb-0 py-auto col-lg-1 col-sm-3">Status</label>
                                <div class="col">
                                    <input type="checkbox" name="status" value="1"  data-toggle="toggle" data-on="Aktif" data-off="Tidak Aktif" data-size="normal" data-width="200" data-onstyle="success" data-offstyle="danger">
                                </div>
                       </div>                                  
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>




   <!-- Modal Edit Member-->
   <form id="form-update-user" action="<?php echo site_url('dashboard/member_user_update');?>" method="post">
         <div class="modal fade " id="myModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Edit Data Member</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="no_register" class="mb-0 pb-0">Nama</label>                             
                           <input type="hidden" name="id" value="0">
                           <input type="text" name="nama" class="form-control" placeholder="nama" required>
                       </div>
                       <div class="form-group">
                            <label for="identitas" class="mb-0 pb-0">nomor identitas</label>
                           <small class="statusid"> 
                                <div class="spinner-border spinner-border-sm  loadingiconid" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonid-gagal" style="display:none"> Identitas sudah terpakai</i>
                                <i class="fas fa-check-circle ikonid-ok"style="display:none"></i>
                                 </small>
                            <input type="text" name="identitas" class="form-control " placeholder="" required>
                                                    
                       </div>
                       <div class="form-group">
                            <label for="alamat" class="mb-0 pb-0">alamat</label>
                           <input type="text" name="alamat" class="form-control" placeholder="Jl Diponegoro 01" >
                       </div>
                       <div class="form-group">
                            <label for="provinsi" class="mb-0 pb-0">Provinsi</label>
                            <select name="provinsi" class="form-control provinsi" data-live-search="true" title="Silahkan Pilih" required>
                                    <?php 
                                    foreach($provinsi as $de){
                                        echo '<option value="'.$de->id.'">'.$de->name.'</option>';
                                    }                                                                                            
                                    ?>                                    
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kota" class="mb-0 pb-0">Kota</label>
                            <select name="kota" class="form-control kota" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi provinsi</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kec" class="mb-0 pb-0">Kec</label>
                            <select name="kec" class="form-control kec" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="desa" class="mb-0 pb-0">Desa</label>
                            <select name="desa" class="form-control desa" data-live-search="true" title="Silahkan Pilih" required>
                                <option disabled>silahkan isi Kota</option>                                                                  
                            </select>
                       </div>
                       <div class="form-group">
                            <label for="kodepos" class="mb-0 pb-0">Kodepos</label>
                           <input type="text" name="kodepos" class="form-control" placeholder="65141" >
                       </div>
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">telp</label>
                           <input type="tel" name="telp" class="form-control" placeholder="0812333111" >
                       </div>
                       <div class="form-group">
                            <label for="email" class="mb-0 pb-0">Email</label>
                            <small class="statusemail"> 
                                <div class="spinner-border spinner-border-sm  loadingiconemail" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikonemail-gagal" style="display:none"> Email sudah terpakai</i>
                                <i class="fas fa-check-circle ikonemail-ok"style="display:none"></i>
                                 </small>
                           <input type="email" name="email" class="form-control" placeholder="contoh@gmail.com" >
                       </div>                        

                       <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">jenis</label>
                           <select name="tipe" class="form-control" data-live-search="true" title="silahkan pilih" required>                                                                                               
                                                <option value="1">Member</option>                                                        
                                                <option value="2">Siswa</option>                                                        
                                                <option value="3">Guru</option>                                                        
                                                 </select>
                       </div>
                        <div class="form-group row">
                                <label for="status" class="mb-0 py-auto col-lg-1 col-sm-3">Status</label>
                                <div class="col">
                                    <input type="checkbox" name="status" value="1"  data-toggle="toggle" data-on="Aktif" data-off="Tidak Aktif" data-size="normal" data-width="200" data-onstyle="success" data-offstyle="danger">
                                </div>
                       </div>                                  
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo site_url('dashboard/member_user_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Katalog</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
<script>

        // SET IDENTITAS NUMBER ONLY
        setInputFilter(document.getElementById("identitas"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }



        // check ID
        $('input[name=identitas]').change(function(){
           $('.statusid').removeClass('text-success').removeClass('text-danger');
            $('.loadingiconid').show();
            $('.ikonid-gagal').hide();
            $('.ikonid-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url().'dashboard/checkidentitas';?>",
                data: {identitas: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.statusid').addClass('text-success');
                            $('.loadingiconid').hide(); 
                            $('.ikonid-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.statusid').addClass('text-danger');
                            $('.loadingiconid').hide(); 
                            $('.ikonid-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });
        // check email
        $('input[name=email]').change(function(){
           $('.statusemail').removeClass('text-success').removeClass('text-danger');
            $('.loadingiconemail').show();
            $('.ikonemail-gagal').hide();
            $('.ikonemail-ok').hide();
            var user= $(this).val();
            $('.tombolsubmit').hide();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url().'dashboard/checkemail';?>",
                data: {email: user}
                }).done(function(msg ) {
                        setTimeout(
                    function() 
                    {   
                        if(msg=='1'){
                            $('.statusemail').addClass('text-success');
                            $('.loadingiconemail').hide(); 
                            $('.ikonemail-ok').show();
                            $('.tombolsubmit').show();
                        }else{
                            $('.statusemail').addClass('text-danger');
                            $('.loadingiconemail').hide(); 
                            $('.ikonemail-gagal').show();
                            $('.tombolsubmit').hide();
                        }
                    }, 1000);
                
                });
            
        });


        // get Edit Records
        $('#tabel-master-katalog').on('click','.edit_record',function(){


            var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>dashboard/get_member_json/'+id,                
                    success:function(data) { 
                        console.log(data);
                        $('#myModalUpdate').modal('show');
                        $('#form-update-user [name="id"]').val(data.id_member);
                        $('#form-update-user [name="nama"]').val(data.nama);
                        $('#form-update-user [name="identitas"]').val(data.identitas);
                        $('#form-update-user [name="alamat"]').val(data.alamat);
                        $('#form-update-user [name="provinsi"]').selectpicker('val',data.provinsi);
                        $('#form-update-user [name="kota"]').selectpicker('val',data.kota);
                        $('#form-update-user [name="kec"]').selectpicker('val',data.kec);
                        $('#form-update-user [name="kodepos"]').val(data.kodepos);
                        $('#form-update-user [name="telp"]').val(data.telp);
                        $('#form-update-user [name="email"]').val(data.email);
                        $('#form-update-user [name="tipe"]').selectpicker('val',data.type);
                        if(data.status==1)
                            $('#form-update-user [name="status"]').bootstrapToggle('on')
                                else 
                            $('#form-update-user [name="status"]').bootstrapToggle('off')
                        
                    }
                    });       


           
                
            
        });

        $('#tabel-master-katalog').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
         });
 
    
        
        //  on provinsi select
        $('.provinsi').change(function(){
            
            var id=$(this).val();
            $.ajax({
                method: "POST",
                url: "<?php echo base_url().'dashboard/getkota';?>",
                data: {province_id: id}
                }).done(function(result ) {
                      
                    //  console.log($('select[name="kota"]').html());
                     $('select[name="kota"]').empty();
                    var kota=JSON.parse(result);     
                    // console.log(kota);               
                    for(var a=0;a<kota.length;a++){
                         
                        $('select[name="kota"]').append('<option value="'+kota[a].id+'">'+kota[a].name+'</option>');
                    }
                    $('select[name="kota"]').selectpicker('refresh');
                
                });
        })

        //on kota select
        $('.kota').change(function(){
            
            var id=$(this).val();
            // console.log($('select[name="kec"]').html());
            $.ajax({
                method: "POST",
                url: "<?php echo base_url().'dashboard/getkec';?>",
                data: {regency_id: id}
                }).done(function(result ) {
                      
                    //  console.log($('select[name="kec"]').html());
                     $('select[name="kec"]').empty();
                    var kota=JSON.parse(result);     
                    // console.log(kota);               
                    for(var a=0;a<kota.length;a++){
                         
                        $('select[name="kec"]').append('<option value="'+kota[a].id+'">'+kota[a].name+'</option>');
                    }
                    $('select[name="kec"]').selectpicker('refresh');
                
                });
        })

        //on Kec select
        $('.kec').change(function(){
            
            var id=$(this).val();
            // console.log($('select[name="desa"]').html());
            $.ajax({
                method: "POST",
                url: "<?php echo base_url().'dashboard/getdesa';?>",
                data: {district_id: id}
                }).done(function(result ) {
                      
                     console.log($('select[name="desa"]').html());
                     $('select[name="desa"]').empty();
                    var kota=JSON.parse(result);     
                    // console.log(kota);               
                    for(var a=0;a<kota.length;a++){
                         
                        $('select[name="desa"]').append('<option value="'+kota[a].id+'">'+kota[a].name+'</option>');
                    }
                    $('select[name="desa"]').selectpicker('refresh');
                
                });
        })
  

    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();
       
       
       
        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center'
            } ,
            {
                targets: 1,
                className: 'dt-body-nowrap text-center'
            } ,
            {
                targets: 0,
                className: 'dt-body-nowrap text-center'
            } ,
            {
                targets: 4,
                className: 'dt-body-nowrap text-center'
            } ,
            {
                targets: 5,
                className: 'dt-body-nowrap text-center'
            } ,
           
            { targets : -1,
              orderable: false
            }
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_user_member'?>", "type": "POST"},
                    columns: [
                                                {"data": "id_member"}  ,
                                                {"data": "nama"},
                                                {"data": "identitas"},
                                                {"data": "alamat"},                                                
                                                {"data": "kec"},
                                                {"data": "kota"},
                                                {"data": "provinsi"},
                                                {"data": "kodepos"},
                                                {"data": "telp"},
                                                {"data": "email"},                                                
                                                {"data": "type"},
                                                {"data": "status"},
                                                {"data": "tgl_daftar"},
                                                {"data": "view"},
                                                
                                                                                       
                                                
                  ],
                 
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;

              
          },
          
          autoWidth: false,
          responsive: true,
 
      });
      
            // end setup datatables

 

            
    });
</script>
