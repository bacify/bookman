
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url();?>dashboard/stok_katalog" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1>Cetak Barcode</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Cetak Barcode</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

<div class="container-fluid">
       
      
       
      <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">cetak Barcode</h3>
                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                  <div class="row">
                    <div id="kiri" class="col col-md">
                        <div class="printingpage float-left" ">
                            <div class="img-container float-left mt-1 ml-1 p-1">                                
                                <img src="<?php echo base_url().'dashboard/getqrcode/'.$value;?>" class="img-fluid qrcode d-block mx-auto" alt="Responsive image">
                                <center><strong class="m-0 p-0"><?php echo $value;?></strong> </center>                                
                            </div>
                            <div class="text-container mt-1 pr-1">
                                <strong class="text-left textjudul mt-2 mr-1 mb-0">judul:</strong>
                                <p class="text-left textjudul mt-0 mb-1" ><?php echo $judul;?></p>
                                <strong class="text-left textpengarang mt-1 mr-1 mb-0">pengarang:</strong>
                                <p class="text-left textpengarang mt-0 mb-1" ><?php echo $pengarang;?></p>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="col">
                        <h4> print setting </h4>
                        <form method="POST" action="#">
                        <div class="form-group row my-0 py-0">
                            <label for="panjang" class="col-md-3 col-form-label">Panjang</label>
                            <div  class="col-md-3">
                                <input type="number" name="panjang" class="form-control form-control-sm inputpanjang" placeholder="panjang">
                                <small id="numberhelp" class="form-text text-muted">*default 10 (cm).</small>
                            </div>
                        </div>
                        <div class="form-group row my-0 py-0">
                            <label for="panjang" class="col-md-3 col-form-label">Lebar</label>
                            <div class="col-md-3">
                                <input type="number" name="lebar" class="form-control form-control-sm inputlebar" placeholder="lebar" value="">
                                <small id="lebarhelp" class="form-text text-muted">*default 5 (cm).</small>
                            </div>
                        </div>
                        <div class="form-group row my-0 py-0">
                            <label for="judul" class="col-md-3 col-form-label">Judul</label>
                            <div class="col-md-3 form-check">
                                <input type="checkbox" class="form-control form-control-sm position-static checkjudul" name="judul">                                
                            </div>
                        </div>
                        <div class="form-group row my-0 py-0">
                            <label for="judul" class="col-md-3 col-form-label">pengarang</label>
                            <div class="col-md-3 form-check">
                                <input type="checkbox" class="form-control form-control-sm position-static checkpengarang" name="pengarang">                                
                            </div>
                        </div>
                        <div class="form-group row my-0 py-0">
                            
                            <div class="col-md-6">
                                <center>
                                <button type="button" class="btn btn-default btnprint" ><i class="fas fa-print "> cetak </i></button>
                                </center>
                            </div>
                        </div>
                        
                    </div>
                    </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->

</div>

<script>


$('.btnprint').click(function(){
     $(".printingpage").printThis();
});
$('.inputpanjang').change(function(){
    
    checkform();  
})
$('.inputlebar').change(function(){
    
    checkform();  
})
$('.checkjudul').change(function(){
    
    checkform();

    if(this.checked){
        $('.textjudul').show();
    }else{
        $('.textjudul').hide();
    }
})
$('.checkpengarang').change(function(){
    checkform();
    if(this.checked){                
        $('.textpengarang').show();
    }else{
        $('.textpengarang').hide();
    }
})

function checkform(){
    var c1= $('.checkjudul')[0];
    var c2= $('.checkpengarang')[0];
    var c3= $('.inputpanjang');
    var c4= $('.inputlebar');
    
     
     if(c3.val()!=''){
         console.log('panjang diubah');
        $('.printingpage').width(c3.val()+'cm');
     }else{
        if(c1.checked || c2.checked )
            $('.printingpage').width('10cm');
        else
            $('.printingpage').width('5cm');
     }
     if(c4.val()!=''){
        console.log('lebar diubah');
        $('.printingpage').height(c4.val()+'cm');
     }else{
        $('.printingpage').height('5cm');
     }
    if(c1.checked || c2.checked ){
           
        $('.img-container').width('50%');      

    }else{
               
        $('.img-container').width('100%');
        
    }
}

    </script>