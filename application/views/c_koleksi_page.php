<div class="row   justify-content-center align-items-center pt-3">
      <div class="col-12 col-md-12 col-lg-12 content-cari">
        
        
        
     
    <div class="card bg-result">
    <div class="card-header"><h5 class="text-dark card-title">Koleksi Katalog Terbaru (50)</h5></div>
    <div class="card-body">
    
            <table class="table table-striped  table-hover table-responsive">
                <thead class="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Judul Buku</th>
                    <th scope="col">Pengarang</th>
                    <th scope="col">Penerbit</th>                    
                    <th scope="col">Subyek</th>
                    <th scope="col" class="text-nowrap">NoPanggil</th>
                    <th scope="col">tahun</th>                    
                    
                    </tr>
                </thead>
                <tbody>
                <?php
                 if(count($hasil)==0){
                    echo '<tr><td colspan="7" class="text-nowrap text-center">Tidak ada hasil yang ditemukan</td></tr>';
                }
                $x=1; 
                foreach($hasil as $hasilcari){
                    echo '<tr><td>'.$x++.'</td>'.
                              '<td>'.$hasilcari->judul.'</td>'.
                              '<td>'.$hasilcari->pengarang.'</td>'.
                              '<td>'.$hasilcari->penerbit.'</td>'.
                              '<td>'.$hasilcari->subyek.'</td>'.
                              '<td class="text-nowrap">'.$hasilcari->no_panggil.'</td>'.
                              '<td>'.$hasilcari->tahun.'</td>';                          
                              

                              echo '</tr>';
                }
                ?>
                   
                </tbody>
            </table>
    </div>
    </div>
     </div>
    </div>
 
 
 
 