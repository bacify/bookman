<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Daftar Peminjaman</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Daftar Peminjaman</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Peminjaman</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Pinjaman Baru</button>                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>NoPinjam</th> 
                        <th>Tanggal</th>
                        <th>Member</th>
                        <th>handphone</th>
                        <th>email</th>
                        <th>status</th>                        
                        <th>Tindakan</th>                        
                         
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


     <!-- Modal Add Product-->
     <form id="form-pinjam-baru" action="<?php echo site_url('dashboard/pinjam_baru');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Pinjaman Baru </h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">                        
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">nomor Anggota</label>     
                            <small class="status"> 
                                <div class="spinner-border spinner-border-sm  loadingicon" role="status"  style="display:none">
                                    <span class="sr-only">Loading...</span>
                                </div>    
                                <i class="fas fa-times-circle ikon-gagal" style="display:none"> Nomor Tidak Ditemukan</i>
                                <i class="fas fa-check-circle ikon-ok"style="display:none"></i>
                                 </small>                       
                            <input type="text" name="nomor" id="noidmember" class="form-control" maxlength="6" placeholder="123456" required>
                                                    
                       </div>   
                       <div class="form-group">
                            <label for="no_register" class="mb-0 pb-0">Nama</label>                           
                           <input type="text" name="nama" class="form-control" placeholder="nama" >
                       </div>                                                                      
                       <div class="form-group">
                            <label for="telp" class="mb-0 pb-0">Telp</label>
                            <input type="hidden"  name="truephone"    value="0">                       
                            <input type="tel" name="phone" class="form-control" placeholder="0812345678">
                       </div>    
                         
                                                                                         
                                                                                        
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success tombolsubmit" disabled>Pinjaman Baru</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>

       <!-- Modal delete Product-->
    <form id="deleteform" action="<?php echo site_url('dashboard/peminjaman_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>

 
  
 
<script>
 // SET IDENTITAS NUMBER ONLY
 setInputFilter(document.getElementById("noidmember"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }

    $( "#noidmember" ).keyup(function() {
      var value=$(this).val();
      console.log(value);
      if(value.length == 6){
                $.ajax({
                        method: "GET",
                        dataType: "json",
                        url: "<?php echo base_url();?>dashboard/get_member_json/"+value,                        
                        })
                        .done(function( msg ) {

                          if(msg.status=='1'){
                            
                            $('#form-pinjam-baru input[name="nama"]').val(msg.result.nama);
                            $('#form-pinjam-baru input[name="phone"]').val(msg.result.telp);
                            $('.tombolsubmit').prop('disabled',false);
                          } else{
                            $('#form-pinjam-baru input[name="nama"]').val('');
                            $('#form-pinjam-baru input[name="phone"]').val('');
                            $('.tombolsubmit').prop('disabled',true);
                          }


                            return false;
                        });
        }
    });

    $('.mainsearch').autocomplete({                    
                    source: "<?php echo base_url();?>dashboard/searchkataloginstock",
                    minLength: 2,
                    select: showResult,
                    focus : showResult, 
                    change: false
                    } );  
                function showResult( event, ui ) {
                            $('.id_katalog').val(ui.item.value);
                            $('.mainsearch').val(ui.item.label);
                            var val=ui.item.label.split('|');
                            $('.istok').val(val[val.length - 1].trim());
                            
                            
                            return false;
                } 

    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center bd-callout'
            },
            {
                targets: 5,
                className: 'dt-body-nowrap text-center'
            },
            { targets : 5,
             render : function (data, type, row) {
             
              switch(data) {
                case '0' : return '<a href="#" class="btn btn-transparent   text-warning"> <i class="fas fa-sync" title="Belum Diproses"></i></a>'; break;
                case '1' : return '<a href="#" class="btn btn-transparent   text-light"><i class="fas fa-exclamation-circle" title="Tanggungan"></i></a>'; break;
                case '2' : return '<a href="#" class="btn btn-transparent   text-light"><i class="far fa-check-circle" title="Sudah Dikembalikan"></i></a>'; break;
                default  : return 'N/A';
              }
              }
            },
            { targets : 6,
             render : function (data, type, row) {
              
             if(row.status_pinjam=='0'){
               return data;
             } else{
             var a=data.split('</a> <a');               
             
               return a[0]+'</a>';
             }
              
            }
          }
         ],
        createdRow: function( row, data, dataIndex){
              
                if( data.status_pinjam ==  '1'){
                    $(row).addClass('bg-danger-low');
                }else if(data.status_pinjam ==  '2'){
                  $(row).addClass('bg-success-low');
                }
            },          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_pinjaman'?>", "type": "POST"},
                    columns: [
                                                {"data": "id_pinjam"}  ,
                                                {"data": "tanggal"},
                                                {"data": "nama"},
                                                {"data": "telp"},
                                                {"data": "email"},
                                                {"data": "status_pinjam"},
                                                {"data": "view"},
                                                
                                                                                       
                                                
                  ],
                order: [[1, 'desc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 1, "desc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
      
            // end setup datatables

 
            $('#tabel-master-katalog').on('click','.delete_record',function(){
              var id=$(this).data('id');
              $('#ModalDelete').modal('show');
              $('#deleteform [name="id"]').val(id);
            });
            
    });
</script>
