<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LIMASYS - Library Management System | Administrator Area</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url(); ?>favicon.ico" type="image/x-icon">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/dataTables.bootstrap4.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/responsive.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/adminlte.min.css">
 
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.min.css">

  <!-- JQUERY UI -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jqueryui/jquery-ui.min.css">
  
  <!-- Font Awesome -->  
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/plugins/fontawesome/css/all.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .dt-body-nowrap {
      white-space: nowrap;
    }
  </style>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/admincustom.css">


  <!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.min.js"></script>
  <!-- jQuery UI-->
<script src="<?php echo base_url(); ?>assets/plugins/jqueryui/jquery-ui.min.js"></script>
<!-- jQuery Date Picker-->
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>

<!-- PrintThis Library -->
<script src="<?php echo base_url(); ?>assets/js/printThis.js"></script>

<!-- Bootstrap toogle library -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-toggle.min.css">
<script src="<?php echo base_url(); ?>assets/js/bootstrap-toggle.min.js"></script>


<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url(); ?>assets/plugins/chart.js/Chart.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/responsive.bootstrap4.min.js"></script>

<!-- Bootstrap Select -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
  

 <!-- HEADER -->
 <?php echo $header;?>
 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    

    <!-- Main content -->
    <?php echo $content; ?>
    
   
  </div>
  <!-- /.content-wrapper -->


  <!-- FOOTER CONTENT -->
  <?php echo $footer;?>
  


</div>
<!-- ./wrapper -->





<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/admin/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo base_url(); ?>assets/admin/js/demo.js"></script> -->
 

    
</body>
</html>
