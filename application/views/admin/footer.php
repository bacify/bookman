<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>AdminLTE 3.0.5</b> 
    </div>
    <strong><span>LIMASYS</span> <i class="far fa-copyright"> 2020 SERANGGALANGIT </i> </strong>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->