<?php 
            $link1=$this->uri->segment(1,0);
            $link2=$this->uri->segment(2);

            ?>

<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbaratas">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button">
          <i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Library Management System</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url(); ?>assets/index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->
    <ul class="navbar-nav ml-auto">
      <?php if($this->session->userdata('UID')){
          
          ?>		
      <li class="nav-item">
        <a class="nav-link" href="<?php if($this->session->userdata('type')=='admin') echo base_url().'dashboard/logout'; else echo base_url().'member/logout';?>">          
          <i class="fas fa-sign-out-alt" title="Keluar"></i>
        </a>
      </li>

      <?php } ?>
    </ul>
    
  </nav>
  <!-- /.navbar -->

   <!-- Main Sidebar Container -->
  <aside class="main-sidebar  elevation-4 sidebar-utama">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?php echo base_url(); ?>assets/img/limasys_logo.png"
           alt="LIMASYS Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><?php if($this->session->userdata('type')=='admin') echo 'Admin Panel'; else echo 'Member Area';?></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->

        <!-- ADMIN MENU -->
        <?php if($this->session->userdata('type')=='admin'){ ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" <?php if(!$this->session->userdata('UID')){ echo 'style="display:none"';}?>>
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo base_url();?>dashboard/utama" class="nav-link <?php if( $link1=='dashboard' && $link2=='utama' ) echo 'active';?>">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Halaman Utama
                
              </p>
            </a>
            
          </li>
          <?php 
          $statustree=false;
          if($link2=='inventory' || $link2=='master_katalog'|| $link2=='stok_katalog' || $link2=='inventory_masuk' || $link2=='inventory_keluar' || $link2=='inventory_detail_in')
          {
            $statustree=true;
          }
          $statustree2=false;
          if($link2=='kategori_katalog' || $link2=='useradmin')
          {
            $statustree2=true;
          }

          ?>
          <li class="nav-item has-treeview <?php if($link1=='dashboard' && $statustree ) echo "menu-open";?>">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-book"></i>
              <p>
                  Inventory
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/master_katalog" class="nav-link <?php if( $link1=='dashboard' && $link2=='master_katalog' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Database 
                  </p>
                </a>
                </a>
              </li>
              
              <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/stok_katalog" class="nav-link <?php if($link1=='dashboard' && $link2=='stok_katalog'  ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Stok                
                  </p>
                </a>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/inventory" class="nav-link <?php if($link1=='dashboard' && $link2=='inventory' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                  Riwayat Transaksi             
                  </p>
                </a>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/inventory_masuk" class="nav-link <?php if( $link1=='dashboard' && $link2=='inventory_masuk' || $this->uri->segment(2)=='inventory_detail_in' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Transaksi Masuk 
                  </p>
                </a>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/inventory_keluar" class="nav-link <?php if( $link1=='dashboard' && $link2=='inventory_keluar' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Transaksi Keluar
                  </p>
                </a>
                </a>
              </li>
            </ul>
            
            
          </li>

          <li class="nav-item">

            <a href="<?php echo base_url();?>dashboard/kunjungan" class="nav-link <?php if( $this->uri->segment(1,0)=='dashboard' && $this->uri->segment(2)=='kunjungan' ) echo 'active';?>">

              <i class="nav-icon fas fa-eye"></i>
              <p>
                Kunjungan
                 
              </p>
            </a>
            </li>
            <?php 
            $statustree4=false;
            
              if($link2=='pinjaman' || $link2=='pengembalian' || $link2=='daftar_buku_dipinjam'||$link2=='pinjaman_detail'||$link2=='pembayaran'||$link2=='cek_tanggungan')
              {
                $statustree4=true;
              }

            ?>
            <li class="nav-item has-treeview <?php if($link1=='dashboard' && $statustree4 ) echo "menu-open";?>">

            <a href="#" class="nav-link <?php if( $this->uri->segment(1,0)=='dashboard' && $this->uri->segment(2)=='pinjaman_buku' ) echo 'active';?>">

              <i class="nav-icon fas fa-clipboard"></i>
              <p>
                Peminjaman
                <i class="fas fa-angle-left right"></i>      
              </p>
            </a>
            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/pinjaman" class="nav-link <?php if( $link1=='dashboard' && ($link2=='pinjaman' ||$link2=='pinjaman_detail') ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Daftar pinjaman
                  </p>
                </a>
                </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/pengembalian" class="nav-link <?php if( $link1=='dashboard' &&  $link2=='pengembalian') echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    pengembalian
                  </p>
                </a>
                </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/daftar_buku_dipinjam" class="nav-link <?php if( $link1=='dashboard' && $link2=='daftar_buku_dipinjam' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Daftar buku dipinjam
                  </p>
                </a>
                </a>
            </li>
            
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/pembayaran" class="nav-link <?php if( $link1=='dashboard' && $link2=='pembayaran' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Transaksi Pembayaran (nota)
                  </p>
                </a>
                </a>
            </li>
            </ul>
            </li>
            <li class="nav-item">
          
              <a href="<?php echo base_url();?>dashboard/pinjaman_online" class="nav-link">

              <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Pinjaman Online             
              </p>
            </a>
            
            </li>
            
            <?php 
            $statustree3=false;
            
              if($link2=='user_admin' || $link2=='user_member')
              {
                $statustree3=true;
              }

            ?>
            <li class="nav-item has-treeview <?php if($link1=='dashboard' && $statustree3 ) echo "menu-open";?>">

            <a href="#" class="nav-link">

              <i class="nav-icon fas fa-users"></i>
              <p>
                Daftar Pengguna     
                <i class="fas fa-angle-left right"></i>         
              </p>
            </a>
            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/user_admin" class="nav-link <?php if( $link1=='dashboard' && $link2=='user_admin' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Admin
                  </p>
                </a>
                </a>
            </li>

            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/user_member" class="nav-link <?php if( $link1=='dashboard' && $link2=='user_member' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Member
                  </p>
                </a>
                </a>
            </li>
            </ul>
              <?php

              $statustree5=false;
              if($link2=='kategori_katalog' || $link2=='konfigurasi'|| $link2=='ganti_password')
              {
                $statustree2=true;
              }
              ?>

            </li>
            <li class="nav-item has-treeview <?php if($link1=='dashboard' && $statustree5 ) echo "menu-open";?>">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>

            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/kategori_katalog" class="nav-link <?php if( $link1=='dashboard' && $link2=='kategori_katalog' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Kategori katalog 
                  </p>
                </a>
                </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/konfigurasi" class="nav-link <?php if( $link1=='dashboard' && $link2=='konfigurasi' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Pengaturan Aplikasi
                  </p>
                </a>
                </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url();?>dashboard/ganti_password" class="nav-link <?php if( $link1=='dashboard' && $link2=='ganti_password' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Ganti password
                  </p>
                </a>
                </a>
            </li>

            
            </ul>
            </li>          
           

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
        
          <!-- END OF ADMIN MENU -->
          <?php } else{?>
          <!-- MEMBER MENU -->
          <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false" <?php if(!$this->session->userdata('UID')){ echo 'style="display:none"';}?>>
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo base_url();?>member/home" class="nav-link <?php if( $link1=='member' && $link2=='utama' ) echo 'active';?>">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Halaman Utama
                
              </p>
            </a>
            
          </li>
          <?php 
          $statustree=false;
          if($link2=='password')
          {
            $statustree=true;
          }
          // $statustree2=false;
          // if($link2=='kategori_katalog' || $link2=='useradmin')
          // {
          //   $statustree2=true;
          // }

          ?>
          <li class="nav-item">
            <a href="<?php echo base_url();?>member/member_daftar_pinjaman" class="nav-link ">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Daftar Pinjaman
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url();?>member/member_riwayat" class="nav-link ">
               <i class="nav-icon fas fa-clipboard"></i>
              <p>
               Riwayat Pinjaman
                
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url();?>member/member_booking" class="nav-link ">
              <i class="nav-icon fas fa-chalkboard-teacher"></i>
              <p>
                Online Booking
                
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview <?php if($link1=='member' && $statustree ) echo "menu-open";?>">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-book"></i>
              <p>
                  Profil
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
           
            <li class="nav-item">
              <a href="<?php echo base_url();?>member/password" class="nav-link <?php if( $link1=='member' && $link2=='password' ) echo 'active';?>">
              <i class="far fa-circle nav-icon"></i>
                  <p>
                    Ubah password 
                  </p>
                </a>
                </a>
            </li>
          </ul>
        </nav>
          <?php }?>
          <!-- END OF MEMBER MENU -->

    </div>
    <!-- /.sidebar -->
  </aside> 