<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Riwayat Pinjam Online</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Riwayat Booking Online</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Riwayat Pinjam Online</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Pinjaman Baru</button>                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover table-sm">
                <thead>
                        <tr>
                        <th>NoBooking</th> 
                        <th>TanggalBooking</th>
                        <th>Tanggal Pinjam</th>
                        <th>Status</th>
                                           
                        <th>Tindakan</th>                        
                         
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


     <!-- Modal Add Product-->
     <form id="form-pinjam-baru" action="<?php echo site_url('member/booking_baru');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog  ">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Pinjaman Baru </h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">                        
                       <div class="form-group">
                            <label for="judul" class="mb-0 pb-0">tanggal Pinjam</label>     
                                               
                            <input type="text" name="tanggal" id="tanggalpinjam" class="form-control"  required>
                                                    
                       </div>   
                         
                                                                                         
                                                                                        
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success ">Pinjaman Baru</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>

     <!-- Modal delete Product-->
    <form id="deleteform" action="<?php echo site_url('dashboard/peminjaman_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>

 
  
 
<script>
 // SET IDENTITAS NUMBER ONLY
  

    $( "#tanggalpinjam").datepicker({ 
                                    dateFormat: "dd-mm-yy",
                                    minDate:0,
                                     
                                    beforeShowDay: function(date) {
                                        var day = date.getDay();
                                        return [(day != 0), ''];
                                    }
                                    });
 

    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").DataTable({          
        columnDefs: [
            {
                targets: 4,
                className: 'dt-body-nowrap text-center bd-callout'
            },
            {
                targets: 3,
                className: 'dt-body-nowrap text-center bd-callout'
            },
           
            {
                targets: '_all',
                className: 'align-middle'
            },
            { targets : 3,
             render : function (data, type, row) {
             
              switch(data) {
                case '0' : return '<a href="#" class="btn btn-transparent text-warning"> <i class="fas fa-sync" title="Belum Diproses"></i></a>'; break;
                case '1' : return '<a href="#" class="btn btn-transparent text-light"><i class="fas fa-check-circle" title="Sudah Diproses"></i></a>'; break;
                case '2' : return '<a href="#" class="btn btn-transparent text-danger"><i class="fas fa-times-circle" title="Dibatalkan"></i></a>'; break;
                default  : return 'N/A';
              }
              }
            },
            { targets : 4,
             render : function (data, type, row) {
              
             if(row.status=='1' || row.status=='2'){
                return '';
             } else{
                return data;
             }
              
            }
          }
             
         ],
        createdRow: function( row, data, dataIndex){
              
            if( data.status ==  '1'){
                    $(row).addClass('bg-success-low');
                }else if(data.status==  '2'){
                  $(row).addClass('bg-danger-low');
                }
            },          
        initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
        oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'member/get_pinjaman_online'?>", "type": "POST"},
                    columns: [
                                                {"data": "id_booking"}  ,
                                                {"data": "tanggal_booking"},
                                                {"data": "tanggal_pinjam"},
                                                {"data": "status"},                                                
                                                {"data": "view"}                    
                                                
                  ],
        order: [[1, 'desc']],
        rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
        order: [[ 1, "desc" ]],          
        autoWidth: false,
        responsive: true,
      });
      
            // end setup datatables

 
            $('#tabel-master-katalog').on('click','.delete_record',function(){
              var id=$(this).data('id');
              $('#ModalDelete').modal('show');
              $('#deleteform [name="id"]').val(id);
            });
            
    });
</script>
