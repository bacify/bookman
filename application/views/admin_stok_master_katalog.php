<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>DataTables</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Master Katalog</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Stok katalog</h3>
                 
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>No</th> 
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>penerbit</th>                        
                        <th>kategori</th>
                        <th>Stok</th>
                        <th>Cetak</th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



<!-- QR Code Form-->
<form id="barcode" action="<?php echo site_url('dashboard/barcode');?>" method="GET">
         <div class="modal fade" id="ModalBarcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Cetak <span class="texttitel">BARCODE</span></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="type" class="form-control" value="1">
                           <input type="hidden" name="id" class="form-control" >
                           <div class="form-group">
                        <label for="kategori" class="mb-0 pb-0">Field yang akan dicetak </label>
                           <select name="field" class="form-control" data-live-search="true" title="silahkan pilih" required>                                         
                                                <option value="id">Nomor katalog</option>
                                                <option value="register">Nomor Register</option>
                                                <option value="isbn">ISBN</option>
                                                      
                                                 </select>
                       </div>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
  
 
<script>
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center'
            }, 
            { targets : -1,
                render : function (data, type, row) {
             
                return  '<a href="javascript:void(0);" class="qrcodebtn btn btn-transparent btn-sm text-primary" data-id="'+row.katalog_id+'" title="Cetak QRCode"><i class="fas fa-qrcode fa-2x"></i></a>'+
                        '<a href="javascript:void(0);" class="barcodebtn btn btn-transparent btn-sm text-primary" data-id="'+row.katalog_id+'" title="Cetak Barcode"><i class="fas fa-barcode fa-2x"></i></a>';
                }
            }
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_book_stok_master'?>", "type": "POST"},
                    columns: [
                                                {"data": "idx"}  ,
                                                {"data": "judul"},
                                                {"data": "pengarang"},
                                                {"data": "penerbit"},                                          
                                                {"data": "nama_kategori"},                                              
                                                {"data": "stok"},
                                                {"data": null}                                        
                                                
                  ],
                order: [[1, 'asc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 0, "asc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
      table.on( 'draw.dt', function () {
        var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
        table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
            // end setup datatables


            $('#tabel-master-katalog').on('click','.view_record',function(){

                    var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                    success:function(data) { 
                        console.log(data);
                        $('#ModalView').modal('show');
                        $('#viewform [name="id"]').val(data.id_katalog);
                        $('#viewform [name="no_register"]').val(data.no_register);
                        $('#viewform [name="judul"]').val(data.judul);
                        $('#viewform [name="no_panggil"]').val(data.no_panggil);
                        $('#viewform [name="pengarang"]').val(data.pengarang);
                        $('#viewform [name="penerbit"]').val(data.penerbit);
                        $('#viewform [name="kategori"]').selectpicker('val', data.kategori_id);
                        $('#viewform [name="bahasa"]').val(data.bahasa);
                        $('#viewform [name="tahun"]').val(data.tahun);
                        $('#viewform [name="edisi"]').val(data.edisi);
                        $('#viewform [name="subyek"]').val(data.subyek);
                        $('#viewform [name="klasifikasi"]').val(data.klasifikasi);
                        $('#viewform [name="deskripsi"]').val(data.deskripsi);
                        $('#viewform [name="isbn"]').val(data.isbn);
                        
                    }
                    });

                            
                          
                            

                            
});


            // get Edit Records
            $('#tabel-master-katalog').on('click','.edit_record',function(){

                var id=$(this).data('id');
                $.ajax({
                dataType: "json",
                url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                success:function(data) { 
                    console.log(data);
                    $('#ModalUpdate').modal('show');
                    $('#updateform [name="id"]').val(data.id_katalog);
                    $('#updateform [name="no_register"]').val(data.no_register);
                    $('#updateform [name="judul"]').val(data.judul);
                    $('#updateform [name="no_panggil"]').val(data.no_panggil);
                    $('#updateform [name="pengarang"]').val(data.pengarang);
                    $('#updateform [name="penerbit"]').val(data.penerbit);
                    $('#updateform [name="kategori"]').selectpicker('val', data.kategori_id);
                    $('#updateform [name="bahasa"]').val(data.bahasa);
                    $('#updateform [name="tahun"]').val(data.tahun);
                    $('#updateform [name="edisi"]').val(data.edisi);
                    $('#updateform [name="subyek"]').val(data.subyek);
                    $('#updateform [name="klasifikasi"]').val(data.klasifikasi);
                    $('#updateform [name="deskripsi"]').val(data.deskripsi);
                    $('#updateform [name="isbn"]').val(data.isbn);
                    
                }
                });
                 
                    
                        
           
                        
      });
            // End Edit Records
            // get delete Records
       $('#tabel-master-katalog').on('click','.qrcodebtn',function(){
            var id=$(this).data('id');
            $('#ModalBarcode').modal('show');
            $('#barcode [name="type"]').val(1);
            $('#barcode [name="id"]').val(id);
            $('.texttitel').html('QrCode');
       });
       $('#tabel-master-katalog').on('click','.barcodebtn',function(){
            var id=$(this).data('id');
            $('#ModalBarcode').modal('show');
            $('#barcode [name="type"]').val(2);

            $('.texttitel').html('BarCode');
            $('#barcode [name="id"]').val(id);
       });
            // End delete Records


            
    });
</script>
