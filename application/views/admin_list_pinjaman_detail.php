<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url();?>dashboard/pinjaman" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1>Detail Pinjaman (<strong><?php echo  $this->uri->segment(3);?></strong>) </h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Detail Pinjaman  (<?php echo $this->uri->segment(3);?>)</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
      
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
              <button type="button" id="tombol-save" class="btn btn-outline-success float-right " title="simpan" <?php if($master->status_pinjam!='0') echo 'style="display:none;"';?>> <i class="fas fa-suitcase"></i> <i class="fas fa-check"></i> </button>
                <div class="row">
                    <div class="col-6">
                    <form method="POST" action="<?php echo base_url();?>dashboard/pinjam_detail_add">
                    <div class="form-group row my-0 py-0">
                        <label for="notransksaksi" class="col-md-2 col-form-label my-0 py-0">NoTransaksi</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext  my-0 py-0" id="notransksaksi" value="<?php echo $master->id_pinjam;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="asal" class="col-md-2 col-form-label  my-0 py-0">Member</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext  my-0 py-0" id="asal" value="<?php echo $nama;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="keterangan" class="col-md-2 col-form-label my-0 py-0">Keterangan</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="keterangan" value="<?php echo $master->keterangan;?>">
                        </div>
                    </div>
                    </div>
                    <div class="col">
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label my-0 py-0">Tanggal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext  my-0 py-0" id="tanggal" value="<?php echo $master->tanggal;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label my-0 py-0">STATUS</label>
                        <div class="col-md-10">
                        <p class="form-control-plaintext  my-0 py-0 font-weight-bold <?php if($master->status_pinjam=='0') echo 'text-warning'; else if($master->status_pinjam==1) echo 'text-danger';else echo 'text-success';?>"><?php if($master->status_pinjam=='0') echo '<i class="fas fa-ban" title="Belum Diproses"></i> BELUM DIPROSES'; else if($master->status_pinjam==1) echo '<i class="fas fa-exclamation-circle" ></i> DALAM PINJAMAN';else echo '<i class="fas fa-check-circle" title="Sudah Dikembalikan"></i> SUDAH DIKEMBALIKAN';?></p>
                        </div>
                    </div>
                    
                    </div>
                    <!-- END OF COL -->

                    <div class="col-8" <?php if($master->status_pinjam!='0') echo 'style="display:none;"';?> >
                    <hr/>
                    <div class="form-group row my-0 py-0" >
                        <label for="tanggal" class="col-md-2 col-form-label ">Cari Buku</label>                        
                        <div class="col-md-8">
                        <input type="hidden" name="id_katalog" class="id_katalog"  value="0">
                        <input type="hidden" name="id_pinjam" value="<?php echo $master->id_pinjam;?>">
                        <input type="text" name="judul" class="form-control mainsearch form-control-sm" placeholder="Ketik No register, No Panggil atau Judul Buku" required <?php if($master->status_pinjam!='0') echo 'disabled';?>>
                        <small id="emailHelp" class="form-text text-muted">*katalog yang memiliki stok 0 tidak akan ditampilkan.</small>
                        </div>
                        <div class="col-md-1">
                        <input type="number" name="stok" class="form-control form-control-sm istok"  value="0" placeholder="0"  disabled>                        
                        <small id="emailHelp" class="form-text text-muted">*stok</small>
                        </div>                        
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog" <?php if($master->status_pinjam!='0') echo 'disabled';?>><i class="fas fa-database" ></i></button> 
                        </div>
                    </div>
                    </form>
                    <hr/>
                    <form method="POST" action="#">
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label ">SearchBy</label>   
                        <div class="col-md-2">
                            
                            <select name="searchby" class="form-control form-control-sm searchby">
                                <option value="isbn">ISBN</option>
                                <option value="id">ID</option>
                                <option value="register">NoRegister</option>                                
                                <option value="panggil">NoPanggil</option>                                
                            </select>               
                            
                        </div>                      
                        <div class="col-md-7">
                        
                        <input type="hidden" name="id_katalog_barcode" class="id_katalog_barcode"  value="0">
                        <input type="hidden" name="id_pinjam_barcode" class="id_pinjam_barcode" value="<?php echo $master->id_pinjam;?>">
                        <input type="text" name="kode_barcode" class="form-control barcodesearch form-control-sm" onmouseover="this.focus();" placeholder="Ketik NoBarCode" required <?php if($master->status_pinjam!='0') echo 'disabled';?>>
                         
                        </div>
                                              
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog" <?php if($master->status_pinjam!='0') echo 'disabled';?>><i class="fas fa-database" ></i></button> 
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <div class="col-md-12">
                            <div class="alert alert-success alert-ok" role="alert" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <span class="text-ok"><strong>Sukses!</strong> Data Ditambahkan!</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-gagal" role="alert" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <span class="text-gagal"><strong>Gagal!</strong> Data gagal Ditambahkan!</span>
                            </div>
                        </div>
                    </div>
                    </form>
                     
                </div>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                 
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                <tr>
                        <th>No</th>                        
                        <th>no_panggil</th>
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>tahun</th>                        
                        <th>isbn</th>                                                
                        <th>status</th>                                                
                        <th class="text-center"><i class="fas fa-tools "></i></th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 
 
 <!-- Modal save Product-->
 <form id="saveform" action="<?php echo site_url('dashboard/pinjam_detail_save');?>" method="post">
         <div class="modal fade" id="myModalsave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Proses Pinjaman</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <input type="hidden" name="id_pinjam" value="<?php echo $master->id_pinjam;?>" >
                        <p><strong class="text-uppercase">Proses pinjaman buku?</strong></p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-primary buttonsave">Simpan</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 <!-- Modal delete Product-->
 <form id="deleteform" action="<?php echo site_url('dashboard/pinjam_detail_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                                                <input type="hidden" name="id_pinjam" value="<?php echo $master->id_pinjam;?>" >
                                                <input type="hidden" name="id_detail" class="form-control" >
                                                 <p> Apakah anda yakin akan menghapus data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
 <!-- Modal Return Product-->
 <form id="returnform" action="<?php echo site_url('dashboard/pinjam_detail_return');?>" method="post">
         <div class="modal fade" id="ModalReturn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Kembalikan Buku</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                                                <input type="hidden" name="id_pinjam" value="<?php echo $master->id_pinjam;?>" >
                                                <input type="hidden" name="id_detail" class="form-control" >
                                                 <p> Apakah anda yakin akan mengembalikan sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Kembalikan</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
 
<script>
var table;

    
   function updateJumlah(x) {
                
                var newval=$(x).val();
                var stok=$(x).closest('tr').find('.inputstok').val();                
                var iddetail=$(x).closest('td').find('.inputiddetail').val();
                
                if(!isNaN(newval)){
                    if(stok<newval){
                        $(this).val('0');
                        alert('Pastikan jumlah tidak melebihi stok yang ada')
                        return false;
                     }else{
                          // proces data
                        $.ajax({
                        method: "POST",
                        url: "<?php echo base_url();?>dashboard/master_detail_out_update",
                        data: { id_detail: iddetail, jumlah: newval }
                        })
                        .done(function( msg ) {
                            if(msg=='1')
                            alert( "OK");
                            else
                            alert( "Error");


                            return false
                        });
                     }
                   
                    
                }else{
                    $(x).val(0);
                    alert('Masukkan jumlah yang benar');
                    return false;
                }
                
                        
    }
     
    $(".ijumlah").keyup(function(){
        var value=$(this).val();
        var stok=$('.istok').val();

        if(stok<value){
            $(this).val('');
            alert('Pastikan jumlah tidak melebihi stok yang ada')
        }
    });
     
    // DETECT BARCODE
    $('.barcodesearch').keypress(function (e) {
    if (e.which == 13) {
        $(this).prop( "disabled", true );
        var vv=$(this).val();
        console.log(vv);
        console.log('ditemukan');
        var pinjamid=$('.id_pinjam_barcode').val();
        var search=$('.searchby :selected').val();
        $.ajax({
                        method: "POST",
                        url: "<?php echo base_url();?>dashboard/pinjam_detail_ajax_add",
                        data: { id_pinjam: pinjamid, nomor: vv ,searchby:search}
                        })
                        .done(function( msg ) {
                           
                            var res=JSON.parse(msg);                             
                            if(res.status=='1'){
                                
                                table.ajax.reload();
                                console.log('ok');
                                $('.text-ok').html(res.msg);
                                $(".alert-ok").fadeTo(2000, 1000).slideUp(1000, function() {
                                         $(".alert-ok").slideUp(1000);
                                         $('.barcodesearch').val('');
                                         $('.barcodesearch').prop( "disabled", false );
                                         $('.barcodesearch').focus();
                                });
                            }                                
                            else
                            {
                                console.log('gagal');
                                $('.text-gagal').html(res.msg);
                                $(".alert-gagal").fadeTo(2000, 1000).slideUp(1000, function() {
                                         $(".alert-gagal").slideUp(1000);
                                         $('.barcodesearch').val('');
                                         $('.barcodesearch').prop( "disabled", false );
                                         $('.barcodesearch').focus();
                                });

                            }

                            
                            return false;
                        });

        
        return false;    //<---- Add this line
        }
    });

    $(document).ready(function(){

        
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

     
 
       table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center'
            },
            {
                targets: -2,
                className: 'dt-body-nowrap text-center'
            },
            { targets : -1,
                render : function (data, type, row) { 
                    
                  switch(row.status){
                        case '0':  return '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger " data-id="'+row.id_detail+'" title="Hapus Buku"><i class="fas fa-times"></i></a>';
                                  break; 
                        case '1': return '<a href="javascript:void(0);" class="return_record btn btn-transparent  btn-sm  text-primary p-0 m-0" data-id="'+row.id_detail+'" title="pengembalian"><i class="far fa-hand-point-left fa-lg"></i></a>';
                            break; 
                        default :   return '<a href="#" class=" btn-transparent btn-sm text-success"> <i class="fas fa-check-circle" title="Sudah Dikembalikan"></i></a>';
                    }                     
                }
            },              
            { targets : -1,
              orderable: false
            }, 
            { targets : -2,
             render : function (data, type, row) {
                
                
                switch(data) {
                case '0' : return '<a href="#" class=" btn-transparent btn-sm text-warning"> <i class="fas fa-ban" title="Belum Diproses"></i></a>'; break;
                case '1' : return '<a href="#" class=" btn-transparent btn-sm text-danger"><i class="fas fa-exclamation-circle" title="Dalam Pinjaman"></i></a>'; break;
                case '2' : return '<a href="#" class=" btn-transparent btn-sm text-success"><i class="far fa-check-circle" title="Sudah Dikembalikan"></i></a>'; break;
                default  : return 'N/A';
                }
             }
            },      
             
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_pinjaman_detail/'.$master->id_pinjam;?>", "type": "POST"},
                    columns: [
                                                 {"data": "id_detail","defaultContent": ''}  ,
                                                 {"data": "no_panggil"}  ,
                                                 {"data": "judul"}  ,
                                                 {"data": "pengarang"}  ,
                                                 {"data": "tahun"}  ,
                                                 {"data": "isbn"}  ,                                                                                               
                                                 {"data": "status"}  ,                                                                                               
                                                 {"defaultContent": ''} ,
                                                
                                                
                                                
                  ],
                order: [[1, 'desc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 0, "asc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
        table.on( 'draw.dt', function () {
            var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
            table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
            // end setup datatables


 
            // End Edit Records
            // get Save Records
            $('#tombol-save').click(function(){                 
                $('#myModalsave').modal('show');
                
            });
            // End delete Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
                var id=$(this).data('id');
               
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id_detail"]').val(id);
            });
            $('#tabel-master-katalog').on('click','.return_record',function(){
                var id=$(this).data('id');
               
                $('#ModalReturn').modal('show');
                $('#returnform [name="id_detail"]').val(id);
            });
            // End delete Records
            
                  

               
                    $('.mainsearch').autocomplete({                    
                    source: "<?php echo base_url();?>dashboard/searchkataloginstock",
                    minLength: 2,
                    select: showResult,
                    focus : showResult, 
                    change: false
                    } );  
                function showResult( event, ui ) {
                            $('.id_katalog').val(ui.item.value);
                            $('.mainsearch').val(ui.item.label);
                            var val=ui.item.label.split('|');
                            $('.istok').val(val[val.length - 1].trim());
                            
                            
                            return false;
                }  


                
                    
            
    });
</script>
