<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Transaksi Katalog Keluar</h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Transaksi Katalog Keluar</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Transaksi</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>NoTransaksi</th> 
                        <th>Tanggal</th> 
                        <th>Tujuan</th>
                        <th>Keterangan</th>
                        <th>Admin</th> 
                        <th>Status</th> 
                        <th>Tindakan</th> 
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->


 <!-- Modal Add Product-->
 <form id="form-tambah-buku" action="<?php echo site_url('dashboard/master_transaksi_out_save');?>" method="post">
         <div class="modal fade " id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content  ">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Transaksi Masuk</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="Asal" class="mb-0 pb-0">Tujuan</label>
                             
                           <input type="hidden" name="jenis" value="2">
                           <input type="hidden" name="status_transaksi" value="0">
                           <input type="text" name="asal" class="form-control" placeholder="Tujuan Katalog">
                       </div>
                        
                       <div class="form-group">
                            <label for="keterangan" class="mb-0 pb-0">Keterangan</label>
                           <input type="text" name="keterangan" class="form-control" placeholder="keterangan" >
                       </div>                           
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 

 <!-- Modal Update Product-->
<form id="updateform" action="<?php echo site_url('dashboard/master_transaksi_out_update');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Transaksi <strong class="idmaster"></strong></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <div class="form-group">
                        <label for="Asal" class="mb-0 pb-0">Tujuan</label>
                          <input type="hidden" name="id_master" value="0">                           
                           <input type="text" name="asal" class="form-control" placeholder="Tujuan Katalog">
                       </div>
                        
                       <div class="form-group">
                            <label for="keterangan" class="mb-0 pb-0">Keterangan</label>
                           <input type="text" name="keterangan" class="form-control" placeholder="keterangan" >
                       </div>                           
                                       
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
  <!-- Modal delete Product-->
  <form id="deleteform" action="<?php echo site_url('dashboard/master_transaksi_out_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
<script>
    $(document).ready(function(){
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: 2,
                className: 'text-nowrap'
            }, {
                targets: 5,
                className: 'text-center'
            },
            { targets : 5,
             render : function (data, type, row) {
             
            switch(data) {
               case '0' : return '<a href="#" class=" btn-transparent btn-sm text-warning"> <i class="fas fa-sync" title="sedang diproses"></i></a>'; break;
               case '1' : return '<a href="#" class=" btn-transparent btn-sm text-success"><i class="far fa-check-circle" title="Sudah diproses"></i></a>'; break;
               default  : return 'N/A';
            }
            }
          },
          { targets : 0,
             render : function (data, type, row) {
             return pad(data,6);;
                          
            }
          },
          { targets : 6,
          render : function (data, type, row) {
            
            switch(row.status_transaksi) {
               case '0' : return '<a href="<?php echo base_url();?>dashboard/inventory_detail_out/'+row.id_master+'" class="view_record btn btn-transparent btn-sm text-success" title="Detail Transaksi"><i class="fas fa-eye"></i></a> '+
                                 '<a href="javascript:void(0);" class="edit_record btn btn-transparent btn-sm text-primary" data-id="'+row.id_master+'"  title="Edit Transaksi"><i class="far fa-edit"></i></a> '+
                                 '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'+row.id_master+'" title="Hapus Transaksi"><i class="fas fa-trash"></i></a>'; break;               
               case '1' : return '<a href="<?php echo base_url();?>dashboard/inventory_detail_out/'+row.id_master+'" class="view_record btn btn-transparent btn-sm text-success" title="Detail Transaksi"><i class="fas fa-eye"></i></a>'; break;
               default  : return 'N/A';
            }
            }
          }
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_transaksi_master/2'?>", "type": "POST"},
              columns: [
                                                {"data": "id_master"}  ,
                                                {"data": "tanggal"}  ,
                                                {"data": "asal"},
                                                {"data": "keterangan"},
                                                {"data": "nama"},                                          
                                                {"data": "status_transaksi"}     
                                                
                  ],
       
          autoWidth: false,
          responsive: true,
 
      });
 
            // end setup datatables


            $('#tabel-master-katalog').on('click','.view_record',function(){

                    var id=$(this).data('id');
                    $.ajax({
                    dataType: "json",
                    url: '<?php echo base_url();?>dashboard/get_katalog_json/'+id,                
                    success:function(data) { 
                        
                        $('#ModalView').modal('show');                       
                        
                    }
                    });

                            
                          
                            

                            
});


            // get Edit Records
            $('#tabel-master-katalog').on('click','.edit_record',function(){

                var id=$(this).data('id');
                $.ajax({
                dataType: "json",
                url: '<?php echo base_url();?>dashboard/get_transaksi_master_json/'+id,                
                success:function(data) { 
                    
                    $('#ModalUpdate').modal('show');
                    $('#updateform [name="id_master"]').val(data.id_master);
                    $('#updateform [name="asal"]').val(data.asal);
                    $('#updateform [name="keterangan"]').val(data.keterangan);
                    $('#updateform .idmaster').html(pad(id,6));
                    
                }
                });
                 
                    
                        
           
                        
      });
            // End Edit Records
            // get delete Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
            var id=$(this).data('id');
            $('#ModalDelete').modal('show');
            $('#deleteform [name="id"]').val(id);
      });
            // End delete Records


            
    });


  function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
  }
</script>
