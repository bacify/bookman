<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url();?>dashboard/pinjaman" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1>Detail Pinjaman Online (<strong><?php echo  $this->uri->segment(3);?></strong>) </h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Detail Pinjaman Online (<?php echo $this->uri->segment(3);?>)</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
      
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
              <button type="button" id="tombol-save" class="btn btn-outline-success float-right " title="Proses Pinjam" <?php if($master->status!='0') echo 'style="display:none;"';?>> <i class="fas fa-suitcase"></i> <i class="fas fa-check"></i> </button>
                <div class="row">
                    <div class="col-6">
                    <form method="POST" action="<?php echo base_url();?>dashboard/pinjam_detail_add">
                    <div class="form-group row my-0 py-0">
                        <label for="notransksaksi" class="col-md-2 col-form-label my-0 py-0">NoTransaksi</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext  my-0 py-0" id="notransksaksi" value="<?php echo $master->id_booking;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="asal" class="col-md-2 col-form-label  my-0 py-0">Member</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext  my-0 py-0" id="asal" value="<?php echo $nama;?>">
                        </div>
                    </div>
                    
                    </div>
                    <div class="col">
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label my-0 py-0">Tanggal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext  my-0 py-0" id="tanggal" value="<?php echo $master->tanggal_booking;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label my-0 py-0">STATUS</label>
                        <div class="col-md-10">
                        <p class="form-control-plaintext  my-0 py-0 font-weight-bold <?php if($master->status=='0') echo 'text-warning'; else if($master->status==1) echo 'text-danger';else echo 'text-success';?>"><?php if($master->status=='0') echo '<i class="fas fa-ban" title="Belum Diproses"></i> BELUM DIPROSES'; else if($master->status==1) echo '<i class="fas fa-exclamation-circle" ></i> DALAM PINJAMAN';else echo '<i class="fas fa-check-circle" title="Sudah Dikembalikan"></i> SUDAH DIKEMBALIKAN';?></p>
                        </div>
                    </div>
                    
                    </div>
                    <!-- END OF COL -->

                  
                   
                    </form>
                    
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                 
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                <tr>
                        <th>No</th>                        
                        <th>no_panggil</th>
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>tahun</th>                        
                        <th>Stok</th>
                        <th class="text-center"><i class="fas fa-tools "></i></th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                   
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 
  
 <!-- Modal save Product-->
 <form id="saveform" action="<?php echo site_url('dashboard/proses_booking');?>" method="post">
         <div class="modal fade" id="myModalsave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Proses Pinjaman</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <input type="hidden" name="id_booking" value="<?php echo $master->id_booking;?>" >
                        <p><strong class="text-uppercase">Proses pinjaman buku?</strong></p>
                        <small id="emailHelp" class="form-text text-muted">*proses pinjam hanya akan dilakukan untuk katalog yang tersedia.</small>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-primary buttonsave">Simpan</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 <!-- Modal delete Product-->
 <form id="deleteform" action="<?php echo site_url('dashboard/pinjam_online_detail_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                                                <input type="hidden" name="id_booking" value="<?php echo $master->id_booking;?>" >
                                                <input type="hidden" name="id_detail" class="form-control" >
                                                 <p> Apakah anda yakin akan menghapus data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
   
  
 
<script>
var table;

     
     
    

    $(document).ready(function(){

        
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

     
 
       table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap text-center'
            },
            {
                targets: -2,
                className: 'dt-body-nowrap text-center'
            },
                                  
            { targets : -1,
              orderable: false
            }, 
                                  
            { targets : -2,
              orderable: false
            }, 
            { targets : -2,
             render : function (data, type, row) {
                
                if(data>0)
                return '<a href="#" class=" btn-transparent btn-sm text-success"> <i class="fas fa-check-circle" title="Tersedia"></i></a>';
                else
                return '<a href="#" class=" btn-transparent btn-sm text-danger"> <i class="fas fa-exclamation-circle" title="Tidak Tersedia"></i></a>';

             }
            },      
             
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_pinjaman_online_detail/'.$master->id_booking;?>", "type": "POST"},
                    columns: [
                                                 {"data": "id_booking_detail","defaultContent": ''}  ,
                                                 {"data": "no_panggil"}  ,
                                                 {"data": "judul"}  ,
                                                 {"data": "pengarang"}  ,
                                                 {"data": "tahun"}  ,
                                                 {"data":"stok","defaultContent": ''} ,                                                                                            
                                                                                                                                          
                                                 {"data": "view"} ,
                                                
                                                
                                                
                  ],
                order: [[1, 'desc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 0, "asc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
        table.on( 'draw.dt', function () {
            var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
            table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
            // end setup datatables


 
            // End Edit Records
            // get Save Records
            $('#tombol-save').click(function(){                 
                $('#myModalsave').modal('show');
                
            });
            // End delete Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
                var id=$(this).data('id');
               
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id_detail"]').val(id);
            }); 
            // End delete Records
            
                   


                
                    
            
    });
</script>
