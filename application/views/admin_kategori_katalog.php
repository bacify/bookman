<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>DataTables</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Master Katalog</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Database katalog</h3>
                <button class="btn btn-success float-right" data-toggle="modal" data-target="#myModalAdd">Tambah Baru</button>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                        <tr>
                        <th>ID</th>
                        <th>no_register</th>
                        <th>no_panggil</th>
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>penerbit</th>
                        <th>kategori_id</th>
                        <th>nama_kategori</th>
                        <th>Tindakan</th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->



         <!-- Modal Add Product-->
         <form id="add-row-form" action="<?php echo site_url('dashboard/master_buku_save');?>" method="post">
         <div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Tambah katalog Baru</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                   
                   </div>
                   <div class="modal-body">
                       <div class="form-group">
                           <input type="text" name="product_code" class="form-control" placeholder="Product Code" required>
                       </div>
                                         <div class="form-group">
                           <input type="text" name="product_name" class="form-control" placeholder="Product Name" required>
                       </div>
                                         <div class="form-group">
                           <select name="category" class="form-control" placeholder="Category" required>
                                                      <?php foreach ($kategori as $row) :?>
                                                            <option value="<?php echo $row->id_kategori;?>"><?php echo $row->nama_kategori;?></option>
                                                        <?php endforeach;?>
                                                 </select>
                       </div>
                                         <div class="form-group">
                           <input type="text" name="price" class="form-control" placeholder="Price" required>
                       </div>
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
     
<!-- Modal Update Product-->
<form id="add-row-form" action="<?php echo site_url('dashboard/master_buku_update');?>" method="post">
         <div class="modal fade" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                       <div class="form-group">
                           <input type="text" name="product_code" class="form-control" placeholder="Product Code" readonly>
                       </div>
                                         <div class="form-group">
                           <input type="text" name="product_name" class="form-control" placeholder="Product Name" required>
                       </div>
                                         <div class="form-group">
                           <select name="category" class="form-control" required>
                                                        <?php foreach ($kategori as $row) :?>
                                                            <option value="<?php echo $row->id_kategori;?>"><?php echo $row->nama_kategori;?></option>
                                                        <?php endforeach;?>
                                                 </select>
                       </div>
                                         <div class="form-group">
                           <input type="text" name="price" class="form-control" placeholder="Price" required>
                       </div>
 
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Update</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 
     <!-- Modal delete Product-->
      <form id="add-row-form" action="<?php echo site_url('dashboard/master_buku_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       <h4 class="modal-title" id="myModalLabel">Hapus Katalog</h4>
                   </div>
                   <div class="modal-body">
                           <input type="hidden" name="id" class="form-control" required>
                                                 <strong>Apakah anda yakin akan menghapus data ini?</strong>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="submit" class="btn btn-success">Yes</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
 
<script>
    $(document).ready(function(){
        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#tabel-master-katalog").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_book_master'?>", "type": "POST"},
                    columns: [
                                                {"data": "id_katalog"},
                                                {"data": "no_register"},
                                                {"data": "no_panggil"},
                                                {"data": "judul"},
                                                {"data": "pengarang"},
                                                {"data": "penerbit"},
                                                {"data": "kategori_id"},
                                                {"data": "nama_kategori"},                                         
                                                {"data": "view"}
                  ],
                order: [[1, 'asc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          responsive: true,
 
      });
            // end setup datatables
            // get Edit Records
            $('#mytable').on('click','.edit_record',function(){
            var code=$(this).data('code');
                        var name=$(this).data('name');
                        var price=$(this).data('price');
                        var category=$(this).data('category');
            $('#ModalUpdate').modal('show');
            $('[name="product_code"]').val(code);
                        $('[name="product_name"]').val(name);
                        $('[name="price"]').val(price);
                        $('[name="category"]').val(category);
      });
            // End Edit Records
            // get delete Records
            $('#mytable').on('click','.delete_record',function(){
            var code=$(this).data('code');
            $('#ModalDelete').modal('show');
            $('[name="product_code"]').val(code);
      });
            // End delete Records
    });
</script>
