<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        <small><a href="<?php echo base_url();?>dashboard/inventory_keluar" class="text-decoration-none alert-link"><i class="fas fa-chevron-circle-left"></i> kembali</a></small>
          <h1>Detail Transaksi (<strong><?php echo sprintf('%06d', $this->uri->segment(3));?></strong>) </h1>
          
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Detail Transaksi  (<?php echo sprintf('%06d', $this->uri->segment(3));?>)</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->

  
<section class="content">

      <div class="container-fluid">
        <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong><?php echo $this->session->flashdata('pesan');?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif;?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-9">
                    <form method="POST" action="<?php echo base_url();?>dashboard/master_detail_out_add">
                    <div class="form-group row my-0 py-0">
                        <label for="notransksaksi" class="col-md-2 col-form-label">NoTransaksi</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="notransksaksi" value="<?php echo sprintf('%06d', $master->id_master);?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="asal" class="col-md-2 col-form-label">Asal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="asal" value="<?php echo $master->asal;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="keterangan" class="col-md-2 col-form-label">Keterangan</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="keterangan" value="<?php echo $master->keterangan;?>">
                        </div>
                    </div>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label">Tanggal</label>
                        <div class="col-md-10">
                        <input type="text" readonly class="form-control-plaintext" id="tanggal" value="<?php echo $master->tanggal_transaksi;?>">
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group row my-0 py-0">
                        <label for="tanggal" class="col-md-2 col-form-label ">Dt katalog</label>                        
                        <div class="col-md-6">
                        <input type="hidden" name="id_katalog" class="id_katalog"  value="0">
                        <input type="hidden" name="id_master" value="<?php echo $master->id_master;?>">
                        <input type="text" name="judul" class="form-control mainsearch form-control-sm" placeholder="Ketik No register, No Panggil atau Judul Buku" required <?php if($master->status_transaksi!='0') echo 'disabled';?>>
                        <small id="emailHelp" class="form-text text-muted">*katalog yang memiliki stok 0 tidak akan ditampilkan.</small>
                        </div>
                        <div class="col-md-1">
                        <input type="number" name="stok" class="form-control form-control-sm istok"  value="0" placeholder="0"  disabled>                        
                        <small id="emailHelp" class="form-text text-muted">*stok</small>
                        </div>
                        <div class="col-md-1">
                        <input type="number" name="jumlah" class="form-control form-control-sm ijumlah"  placeholder="jumlah"  required <?php if($master->status_transaksi!='0') echo 'disabled';?>>                        
                        <small id="emailHelp" class="form-text text-muted">*jumlah</small>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-outline-primary  btn-sm"   title="Tambah Data Katalog" <?php if($master->status_transaksi!='0') echo 'disabled';?>><i class="fas fa-database" ></i></button> 
                        </div>
                    </div>
                    </form>
                         
                    </div>
                    <div class="col-3">
                    <div class="float-right">                        
                        <button class="btn btn-primary btn-sm btn-save" data-toggle="modal" data-target="#myModalsave" title="Proses Data Utama" <?php if($master->status_transaksi!='0') echo 'disabled';?>> <i class="fas fa-cloud" > Simpan</i></button>
                        
                        </div>
                    </div>
                </div>
              </div>
             
              <!-- /.card-header -->
              <div class="card-body">
                 
                <table id="tabel-master-katalog" class="table table-bordered table-hover">
                <thead>
                <tr>
                        <th>No</th>
                        <th>no_register</th>
                        <th>no_panggil</th>
                        <th>judul</th>
                        <th>pengarang</th>
                        <th>penerbit</th>                        
                        <th>tahun</th>
                        <th class="text-center">stok  </th>
                        <th class="text-center">Jumlah  </th>
                        <th class="text-center"><i class="fas fa-tools "></i></th>
                        </tr>
                    </thead>
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

 
 
 <!-- Modal save Product-->
 <form id="saveform" action="<?php echo site_url('dashboard/master_detail_out_save');?>" method="post">
         <div class="modal fade" id="myModalsave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Simpan dan Proses Transaksi</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                        <input type="hidden" name="id_master" value="<?php echo $master->id_master;?>" >
                        <p><strong class="text-warning bg-warning text-uppercase">Transaksi tidak akan dapat di ubah jika sudah di simpan</strong>, Apakah anda yakin akan menyimpan data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-primary buttonsave">Simpan</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
 <!-- Modal delete Product-->
 <form id="deleteform" action="<?php echo site_url('dashboard/master_detail_out_delete');?>" method="post">
         <div class="modal fade" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                   <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Hapus Data</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body">
                                                <input type="hidden" name="id_master" value="<?php echo $master->id_master;?>" >
                                                <input type="hidden" name="id_detail" class="form-control" >
                                                 <p> Apakah anda yakin akan menghapus data ini sekarang?</p>
                   </div>
                   <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="submit" name="submit" value="submit" class="btn btn-success">Hapus</button>
                   </div>
                    </div>
            </div>
         </div>
     </form>
  
 
<script>
   function updateJumlah(x) {
                
                var newval=$(x).val();
                var stok=$(x).closest('tr').find('.inputstok').val();                
                var iddetail=$(x).closest('td').find('.inputiddetail').val();
                
                if(!isNaN(newval)){
                    if(stok<newval){
                        $(this).val('0');
                        alert('Pastikan jumlah tidak melebihi stok yang ada')
                        return false;
                     }else{
                          // proces data
                        $.ajax({
                        method: "POST",
                        url: "<?php echo base_url();?>dashboard/master_detail_out_update",
                        data: { id_detail: iddetail, jumlah: newval }
                        })
                        .done(function( msg ) {
                            if(msg=='1')
                            alert( "OK");
                            else
                            alert( "Error");


                            return false
                        });
                     }
                   
                    
                }else{
                    $(x).val(0);
                    alert('Masukkan jumlah yang benar');
                    return false;
                }
                
                        
    }
     
    $(".ijumlah").keyup(function(){
        var value=$(this).val();
        var stok=$('.istok').val();

        if(stok<value){
            $(this).val('');
            alert('Pastikan jumlah tidak melebihi stok yang ada')
        }
    });
    // $(".buttonsave").click(function(){
        
    //     $(this).prop("disabled",true);
    //     consol
    //     $("#saveform").submit();
    // });


    $(document).ready(function(){


            
                
        // selectpicker
        $('select').selectpicker();

        // Setup datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

     
 
      var table = $("#tabel-master-katalog").DataTable({
        
        columnDefs: [
            {
                targets: -1,
                className: 'dt-body-nowrap'
            },
            <?php if($master->status_transaksi=='0'){?>
            { targets : 9,
                render : function (data, type, row) {
             
                return '<a href="javascript:void(0);" class="delete_record btn btn-transparent btn-sm text-danger" data-id="'+row.id_detail+'" title="Hapus Transaksi"><i class="fas fa-trash"></i></a>';
                }
            },
            <?php } ?>
            {   targets : 8,
                render : function (data, type, row) {
                        return '<form><input type="text" name="jumlah" class="form-control form-control-sm inputjumlah" onchange="updateJumlah(this)" value="'+data+'"  <?php if($master->status_transaksi!='0') echo 'disabled';?>><input type="hidden" name="id" class="inputiddetail" value="'+row.id_detail+'"></form> ';
                    
                }
            },
            {   targets : 7,
                render : function (data, type, row) {
                        return '<form><input type="text" name="stok" class="form-control form-control-sm inputstok" value="'+data+'"  disabled></form> ';
                    
                }
            }
         ],          
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'dashboard/get_transaksi_master_detail_out/'.$master->id_master;?>", "type": "POST"},
                    columns: [
                                                 {"data": "id_detail","defaultContent": ''}  ,
                                                 {"data": "no_register"}  ,
                                                 {"data": "no_panggil"}  ,
                                                 {"data": "judul"}  ,
                                                 {"data": "pengarang"}  ,
                                                 {"data": "penerbit"}  ,
                                                 {"data": "tahun"}  ,
                                                //  {"data": "bahasa"}  ,
                                                //  {"data": "tahun"}  ,
                                                //  {"data": "edisi"}  ,
                                                //  {"data": "Subyek"}  ,
                                                //  {"data": "klasifikasi"}  ,
                                                //  {"data": "deskripsi"}  ,
                                                //  {"data": "isbn"}  ,
                                                 {"data": "stok"}  ,
                                                 {"data": "keluar"}  ,
                                                
                                                
                                                
                  ],
                order: [[1, 'desc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          },
          order: [[ 0, "asc" ]],          
          autoWidth: false,
          responsive: true,
 
      });
        table.on( 'draw.dt', function () {
            var PageInfo = $('#tabel-master-katalog').DataTable().page.info();
            table.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            } );
        } );
            // end setup datatables


 
            // End Edit Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.save_record',function(){
                var id=$(this).data('id');
                $('#ModalSave').modal('show');
                
            });
            // End delete Records
            // get Save Records
            $('#tabel-master-katalog').on('click','.delete_record',function(){
                var id=$(this).data('id');
               
                $('#ModalDelete').modal('show');
                $('#deleteform [name="id_detail"]').val(id);
            });
            // End delete Records
            
                  

               
                    $('.mainsearch').autocomplete({                    
                    source: "<?php echo base_url();?>dashboard/searchkataloginstock",
                    minLength: 2,
                    select: showResult,
                    focus : showResult, 
                    change: false
                    } );  
                function showResult( event, ui ) {
                            $('.id_katalog').val(ui.item.value);
                            $('.mainsearch').val(ui.item.label);
                            var val=ui.item.label.split('|');
                            $('.istok').val(val[val.length - 1].trim());
                            
                            
                            return false;
                }  


                
                    
            
    });
</script>
