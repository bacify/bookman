<div class="row h-50 justify-content-center align-items-center">
      <div class="col-8 col-md-8 col-lg-6 content-cari">


      <div class="card bg-result">
        <div class="card-header"><h5 class="text-dark card-title">Kunjungan Anggota</h5></div>
      <div class="card-body">
      
          <form class="form form-cari bg-secondary p-2 mx-1" action="#" method="POST">
            
              <input type="text" name="nomember" id="noidmember" class="form-control form-rounded nomember form-rounded-right" placeholder="123456" aria-label="Recipient's username"  onmouseover="this.focus();" maxlength="6">
            
          </form>  
      </div>
      <div class="card-footer text-center">
        
      </div>
    </div>
        
     </div>
    </div>
 
    <!-- MODAL HASIL -->
    <div class="modal fade " id="modal-result" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog  ">
               <div class="modal-content ">
                   <div class="modal-header">
                   <h5 class="modal-title text-center" id="exampleModalLongTitle">Hasil</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                       
                   </div>
                   <div class="modal-body text-center">                        
                        <h2 class="text-result"><strong class="text-uppercase text-primary">Data DITEMUKAN</strong></h2>
                        <p class="text-muted text-desc">deskripsi</p> 
                   </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">OK</button>
                     
                  </div>
                    
                </div>    
          </div>
    </div>
 
<script>
  $("#noidmember" ).focus();

  
// SET IDENTITAS NUMBER ONLY
setInputFilter(document.getElementById("noidmember"), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
        });
        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            });
        }
 $( "#noidmember" ).keypress(function(e) {
            var value=$(this).val();
            console.log(value);
            if(e.which == 13){
                  $('.text-result').html('');
                  $('.text-desc').html('');
                        $.ajax({
                                method: "GET",
                                dataType: "json",
                                url: "<?php echo base_url();?>kunjungan/insertkunjungan/"+value,                        
                                })
                                .done(function( msg ) {

                                if(msg.status=='1'){
                                    
                                  $('#modal-result').modal('toggle');
                                  $('.text-result').html('<strong class="text-uppercase text-primary">OK!</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('toggle');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 3000);
                                } else{
                                  $('#modal-result').modal('toggle');
                                  $('.text-result').html('<strong class="text-uppercase text-danger">GAGAL! Member Tidak Ditemukan</strong>');
                                  $('.text-desc').html('<strong class="text-danger">*silahkan hubungi admin!</strong>');
                                  setTimeout(function(){
                                      $('#modal-result').modal('toggle');
                                      $("#noidmember" ).val('');
                                      $("#noidmember" ).focus();
                                    }, 3000);
                                }


                                    return false;
                                });
                return false;    //<---- Add this line
                }
            });
</script>